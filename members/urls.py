from django.urls import path
from . import views

app_name = 'members'
urlpatterns = [
    # called by the join page
    path('create_inactive_member/', views.create_inactive_member, name='create_inactive_member'),
    
    # called by GDPR tool
    path('anonymise_member/', views.anonymise_member, name='anonymise_member'),
    
    # activation
    path('request_activation_email/', views.request_activation_email, name='request_activation_email'),
    path('confirm_activation_email/', views.confirm_activation_email, name='confirm_activation_email'),
    path('activate/<slug:activation_key>/', views.activate, name='activate'),
    
    # profile
    path('profile/', views.profile, name='profile'),
    path('change_login_email/', views.change_login_email, name='change_login_email'),
    path('login_email_verification_sent/', views.login_email_verification_sent, name='login_email_verification_sent'),
    path('verify_login_email/<slug:login_email_verification_key>/', views.verify_login_email, name='verify_login_email'),
    
    # update_subscriptions
    path('update_subscriptions/', views.update_subscriptions, name='update_subscriptions'),
    path('token_subs/<uuid:token>/', views.user_token_subs, name='user_token_subs'),

    # expose information to member
    path('memberinfo', views.ReturnMemberInfo.as_view()),

    # update details campaign
    path('update_details/<int:page>/', views.update_details, name='update_details'),
    
    # NationBuilder webhooks
    path('nation_builder_person_created/', views.nation_builder_person_created, name='nation_builder_person_created'),
    path('nation_builder_person_updated/', views.nation_builder_person_updated, name='nation_builder_person_updated'),
    path('nation_builder_person_deleted/', views.nation_builder_person_deleted, name='nation_builder_person_deleted'),
    path('nation_builder_person_merged/', views.nation_builder_person_merged, name='nation_builder_person_merged'),
]

