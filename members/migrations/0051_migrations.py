# Generated by Django 3.1.4 on 2021-01-25 14:38

import django.contrib.postgres.fields.citext
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0050_blankdate'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='member',
            index=models.Index(fields=['email'], name='members_mem_email_afed21_idx'),
        ),
        migrations.AddIndex(
            model_name='nationbuilderperson',
            index=models.Index(fields=['nation_builder_id'], name='members_nat_nation__0be1fe_idx'),
        ),
        migrations.AddIndex(
            model_name='nationbuilderperson',
            index=models.Index(fields=['email'], name='members_nat_email_f38d0b_idx'),
        ),
        migrations.AddIndex(
            model_name='nationbuilderperson',
            index=models.Index(fields=['email1'], name='members_nat_email1_24a4cd_idx'),
        ),
        migrations.AddIndex(
            model_name='nationbuilderperson',
            index=models.Index(fields=['email2'], name='members_nat_email2_68fa6d_idx'),
        ),
        migrations.AddIndex(
            model_name='nationbuilderperson',
            index=models.Index(fields=['email3'], name='members_nat_email3_7e724b_idx'),
        ),
        migrations.AddIndex(
            model_name='nationbuilderperson',
            index=models.Index(fields=['email4'], name='members_nat_email4_cb6480_idx'),
        ),
        migrations.AlterField(
            model_name='nationbuilderperson',
            name='email',
            field=django.contrib.postgres.fields.citext.CIEmailField(blank=True, max_length=255, null=True, unique=True),
        ),
    ]
