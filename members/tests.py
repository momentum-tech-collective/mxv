from datetime import date
from django.test import Client, TestCase, TransactionTestCase, override_settings
from groups.models import Group, GroupStatusType
from members.models import *
from members.views import ensure_my_momentum_record_for_person
from unittest.mock import MagicMock, patch
from mxv.nation_builder import NationBuilder
from mxv import test_common
from django.conf import settings
import json
import os
import logging

class TestNbDataToPostgres(TransactionTestCase):
    def tearDown(self):
        NationBuilderPerson.objects.all().delete()
        NationBuilderTag.objects.all().delete()

    def test_data(self):
        json_data = open(f'{os.path.join(os.path.dirname(__file__))}/test_nb.json')
        person = json.load(json_data)['person']
        n = NationBuilderPerson.objects.create(nation_builder_id=person['id'])
        nb_data_to_postgres(n, person)
        self.assertEqual(999, n.nation_builder_id)
        self.assertEqual('haha@gmail.com', n.email)
        self.assertEqual('VxoRJj4jmzShRlNQIJrO', n.unique_token)
        self.assertEqual('Tyrone', n.first_name)
        self.assertEqual('Nicholas', n.last_name)
        self.assertEqual('07777777777', n.mobile_number)
        self.assertEqual('01111111111', n.phone_number)
        self.assertEqual('haha@peoplesmomentum.com', n.email1)
        self.assertEqual('haha@gmail.com', n.email2)
        self.assertEqual('example@peoplesmomentum.com', n.email3)
        self.assertEqual('31 Willesden Cove', n.primary_address1)
        self.assertEqual('Croydon', n.primary_city)
        self.assertEqual('CR0 7DJ', n.primary_postcode)
        self.assertEqual('GB', n.primary_country)
        self.assertEqual('M222223', n.membership_number)
        self.assertEqual(date(2020, 11, 2), n.join_date.date())
        self.assertEqual(date(2020, 11, 25), n.membership_payment_lapsed_date.date())
        self.assertEqual(MembershipStatusType.Cancelled, n.membership_status)
        self.assertEqual(3, n.membership_payment_amount)
        self.assertEqual(MembershipPaymentCycleType.Monthly, n.membership_payment_cycle)
        self.assertEqual(PaymentProcessorType.GoCardless, n.membership_payment_processor)
        self.assertEqual('CU000DW2Z7QT99', n.gocardless_id)
        self.assertEqual('8458eec7dbe64a6083e27ff65edf35bd', n.messagebird_id)
        self.assertEqual(59, n.tags.count())
    
    def test_date_assign(self):
        # join date in seconds, lapsed date in ms, opposite of previous test
        json_data = '''
            {
                "id": 3, 
                "email": "test@test.com", 
                "momentum_joined_date": 1514808000, 
                "membership_payment_lapsed_date": 1514808000000 
            }'''
        person = json.loads(json_data)
        n = NationBuilderPerson.objects.create(nation_builder_id=person['id'])
        nb_data_to_postgres(n, person)
        n = NationBuilderPerson.objects.get(nation_builder_id=3)
        self.assertEqual(date(2018, 1, 1), n.join_date)
        self.assertEqual(date(2018, 1, 1), n.membership_payment_lapsed_date)

    def test_tag_updates(self):
        json_data = '''
            {
                "id": 3, 
                "email": "test@test.com", 
                "updated_at": "2020-12-10T16:16:58+00:00",
                "tags": ["one", "two", "three"]
            }'''
        person = json.loads(json_data)
        one = NationBuilderTag.objects.create(name='one', first_used=date(2015, 6, 1), last_used=date(2019, 11, 2))
        n = NationBuilderPerson.objects.create(nation_builder_id=person['id'])
        nb_data_to_postgres(n, person)
        n = NationBuilderPerson.objects.get(nation_builder_id=3)
        self.assertEqual(3, n.tags.count())
        t1 = Tagging.objects.get(person__nation_builder_id=3, tag__name='one')
        self.assertEqual(date(2020, 12, 10), t1.tag_date)
        self.assertEqual(t1.tag_date, t1.tag.last_used)
        self.assertEqual(date(2015, 6, 1), t1.tag.first_used)
        self.assertTrue(NationBuilderTag.objects.filter(name='two').exists())
        self.assertTrue(NationBuilderTag.objects.filter(name='three').exists())
        self.assertEqual(date(2020, 12, 10), NationBuilderTag.objects.get(name='two').first_used)
        self.assertEqual(date(2020, 12, 10), NationBuilderTag.objects.get(name='three').last_used)

class TestWebhooks(TestCase):
    def setUp(self):
        self.client = Client()

    def test_missing_token(self):
        response = self.client.post('/members/nation_builder_person_created/', {
            "nation_slug": "momentum",
            "payload": {
                "person": {"person": {"id": 1, "email": "ha@ha.com", "email2": "hi@ha.com", "my_momentum_unique_token": "xxx" }},
            },
        }, 'application/json')
        self.assertEqual(403, response.status_code)
        self.assertFalse(NationBuilderPerson.objects.filter(nation_builder_id=1).exists())

    def test_merge(self):
        NationBuilderPerson.objects.create(nation_builder_id=1, email='ha@ha.com')
        NationBuilderPerson.objects.create(nation_builder_id=2, email='hi@ha.com')
        response = self.client.post('/members/nation_builder_person_merged/', {
            "nation_slug": "momentum",
            "payload": {
                "merged": {"person": {"id": 1, "email": "ha@ha.com", "email2": "hi@ha.com", "my_momentum_unique_token": "xxx" }},
                "deleted": {"person": {"id": 2, "email": "hi@ha.com" }},
            },
            "token": settings.WEB_HOOK_SECRET,
        }, 'application/json')
        self.assertEqual(200, response.status_code)
        self.assertFalse(NationBuilderPerson.objects.filter(nation_builder_id=2).exists())
        self.assertTrue(NationBuilderPerson.objects.filter(nation_builder_id=1).exists())
        self.assertEqual("hi@ha.com", NationBuilderPerson.objects.get(nation_builder_id=1).email2)

    def test_delete(self):
        NationBuilderPerson.objects.create(nation_builder_id=44, email='smack@smack.com')
        response = self.client.post('/members/nation_builder_person_deleted/', {
            "nation_slug": "momentum",
            "payload": {
                "person": {"id": 44, "email": "ha@ha.com" },
            },
            "token": settings.WEB_HOOK_SECRET,
        }, 'application/json')
        self.assertEqual(200, response.status_code)
        self.assertFalse(NationBuilderPerson.objects.filter(nation_builder_id=44).exists())

    def test_delete_str(self):
        NationBuilderPerson.objects.create(nation_builder_id=44, email='smack@smack.com')
        response = self.client.post('/members/nation_builder_person_deleted/', {
            "nation_slug": "momentum",
            "payload": {
                "person": {"id": "44", "email": "ha@ha.com" },
            },
            "token": settings.WEB_HOOK_SECRET,
        }, 'application/json')
        self.assertEqual(200, response.status_code)
        self.assertFalse(NationBuilderPerson.objects.filter(nation_builder_id=44).exists())

class TestEnsureNationBuilderPerson(TransactionTestCase):
    def setUp(self):
        self.nb = MagicMock()
        json_data = open(f'{os.path.join(os.path.dirname(__file__))}/test_nb.json')
        person = json.load(json_data)['person']
        self.nb.GetFromEmail.return_value = person
    
    def tearDown(self):
        Member.objects.all().delete()
        NationBuilderPerson.objects.all().delete()
        NationBuilderTag.objects.all().delete()

    ## Naming conventions used in these tests:
    ## c - object created before the test
    ## e - object we're going to ensure
    ## m - object we'll retrieve from the DB and check

    def test_attached(self):
        c = NationBuilderPerson.objects.create(nation_builder_id=111, email='m@jill.com')
        Member.objects.create(email=c.email, nationbuilderperson=c, first_name='Correct', last_name='record')
        self.nb.PersonFieldsAndValues.return_value = [("email", "m@jill.com")]
        e = Member.objects.get(email='m@jill.com')
        ensure_nationbuilder_person(self.nb, e)
        m = Member.objects.get(email='m@jill.com')
        self.assertEqual(111, m.nationbuilderperson.nation_builder_id)
        self.assertEqual(m.nationbuilderperson.email, m.email)
        self.nb.GetFromEmail.assert_not_called()
        self.nb.PersonFieldsAndValues.assert_called()

    def test_attached_wrong_id(self):
        c = NationBuilderPerson.objects.create(email='nye@bevan.com', nation_builder_id=0, first_name='Bad', last_name='ID')
        Member.objects.create(email=c.email, nationbuilderperson=c)
        self.nb.PersonFieldsAndValues.return_value = None
        e = Member.objects.get(email='nye@bevan.com')
        ensure_nationbuilder_person(self.nb, e)
        n = NationBuilderPerson.objects.get(email='haha@gmail.com')
        m = Member.objects.get(nationbuilderperson__nation_builder_id=999)
        self.assertEqual(999, n.nation_builder_id)
        self.assertEqual('nye@bevan.com', m.email)
        self.assertEqual('VxoRJj4jmzShRlNQIJrO', n.unique_token)

    def test_attached_missing_id(self):
        c = NationBuilderPerson.objects.create(email='nye@bevan.com', first_name='Missing', last_name='ID')
        Member.objects.create(email=c.email)
        e = Member.objects.get(email='nye@bevan.com')
        ensure_nationbuilder_person(self.nb, e)
        m = NationBuilderPerson.objects.get(email='haha@gmail.com')
        self.assertEqual(999, m.nation_builder_id)
        self.assertEqual('VxoRJj4jmzShRlNQIJrO', m.unique_token)

    def test_detached(self):
        NationBuilderPerson.objects.create(nation_builder_id=222, email='notmember@gmail.com')
        e = Member.objects.create(email='notmember@gmail.com', first_name='Just', last_name='joined')
        ensure_nationbuilder_person(self.nb, e)
        m = Member.objects.get(email='notmember@gmail.com')
        self.assertEqual(222, m.nationbuilderperson.nation_builder_id)
        self.assertEqual(m.nationbuilderperson.email, m.email)
        self.nb.GetFromEmail.assert_not_called()

    def test_detached_missing_id(self):
        c = NationBuilderPerson.objects.create(email='notmembernoid@gmail.com')
        e = Member.objects.create(email='notmembernoid@gmail.com', first_name='Just', last_name='joined')
        ensure_nationbuilder_person(self.nb, e)
        self.assertTrue(Member.objects.filter(nationbuilderperson__id=c.id).exists())
        m = Member.objects.get(nationbuilderperson__id=c.id)
        c = NationBuilderPerson.objects.get(id=c.id)
        self.assertEqual(999, c.nation_builder_id)
        self.assertEqual(c.id, e.nationbuilderperson_id)
        self.assertEqual('notmembernoid@gmail.com', m.email)
        self.assertEqual('VxoRJj4jmzShRlNQIJrO', c.unique_token)

class TestProfile(TestCase):
    def _create_member(self, email, first_name, last_name, nb_id):
        member = test_common.create_test_member(email, first_name, last_name)
        member.nationbuilderperson = NationBuilderPerson.objects.create(email=email, nation_builder_id=nb_id)
        member.save()
        return member

    def setUp(self):
        self.client = Client()
        patcher_nb = patch('members.views.NationBuilder')
        self.mock_nb = patcher_nb.start().return_value
        self.addCleanup(patcher_nb.stop)
        Member.objects.all().delete()
        NationBuilderPerson.objects.all().delete()
        ProfileField.objects.all().delete()
        c = self._create_member('nyebevan@gmail.com', 'Aneurin', 'Bevan', 1)
        nb_record = {
            'email': 'nyebevan@gmail.com',
            'id': 1,
        }
        self.mock_nb.GetFromEmail.return_value = nb_record
        self.mock_nb.PersonFieldsAndValues.return_value = nb_record
        self.mock_nb.SetFieldPathValues.return_value = None
        self.mock_nb.LookupPerson.return_value = nb_record
        self.client.force_login(c)

    def test_profile(self):
        resp = self.client.get('/members/profile/')

        self.assertEqual(200, resp.status_code)
        self.assertIsNotNone(resp.context)
        self.assertTrue(resp.context['member_in_nation_builder'])
        self.assertIsNotNone(resp.context['form'])
        fields = resp.context['form'].fields
        self.assertEqual(6, len(fields))
        self.assertIn('name', fields)
        self.assertIn('person__full_name', fields)
        self.assertIn('person__first_name', fields)
        self.assertIn('person__last_name', fields)
        self.assertIn('email', fields)
        self.assertIn('person__email', fields)

    def test_profile_fields(self):
        ProfileField.objects.create( \
            field_path='person.student', field_type=ProfileFieldType.Boolean.name)
        ProfileField.objects.create( \
            field_path='person.home_address.zip')

        resp = self.client.get('/members/profile/')

        fields = resp.context['form'].fields
        self.assertEqual(8, len(fields))
        self.assertIn('person__student', fields)
        self.assertIn('person__home_address__zip', fields)

    def test_save_profile(self):
        self._create_member('jeremy@corbyn.com', 'Jeremy', 'Corbyn', 1)

        resp = self.client.post('/members/profile/', {
            'name': 'Jeremy Corbyn',
            'person__first_name': 'John',
            'person__last_name': 'McDonnell',
            'person__full_name': 'Jeremy Corbyn',
            'email': 'jeremy@corbyn.com',
            'person__email': 'jeremy_changed@corbyn.com',
        }, follow=False)

        self.assertEqual(302, resp.status_code)
        self.mock_nb.SetFieldPathValues.assert_called_with(1, {
            'name': 'Jeremy Corbyn',
            'person.first_name': 'John',
            'person.last_name': 'McDonnell',
            'email': 'jeremy@corbyn.com',
            'person.email': 'jeremy_changed@corbyn.com',
        })

@override_settings(NEW_MEMBER_EMAIL=True)
class TestMemberEmails(TestCase):
    @patch("members.models.send_mailgun_email")
    @patch("members.models.postcode_geo")
    def test_member_on_new(self, mock_geo, mock_mailgun):
        mock_geo.return_value = {
            'european_electoral_region': 'London',
            'admin_district': 'Croydon',
        }
        jack = Member.objects.create(first_name='Jack', last_name='Welsh', email='jack@welsh.com')
        croydon = Territory.objects.create(name='Croydon', lookup_field='admin_district')
        group = Group.objects.create(name='Croydon', dpa=jack, status=GroupStatusType.Verified)
        group.territories.add(croydon)
        person = NationBuilderPerson.objects.create(
            nation_builder_id=1,
            email='tommy@canada.com',
            first_name='Tommy',
            last_name='Douglas',
            phone_number='1',
            mobile_number='7',
            primary_postcode='CR0 7DJ',
            join_date=date(2019, 1, 1),
            membership_status=MembershipStatusType.Active)
        member_on_new(person)
        mock_mailgun.assert_called_once()
        args = mock_mailgun.call_args[0]
        self.assertEqual(args[0], 'jack@welsh.com')
        self.assertEqual(args[1], 'Jack')
        self.assertFalse(args[2])
        self.assertFalse(args[3])
        self.assertEqual(args[4], settings.DEFAULT_FROM_EMAIL)
        self.assertIsNone(args[5])
        self.assertEqual(args[6], "A new member has signed up")
        text = '''
Dear %recipient.salutation%,

A new member has joined Momentum in your group's territory!

Name: Tommy Douglas
Email: tommy@canada.com
Mobile: 7
Phone: 1
Postcode: CR0 7DJ
Join date: 2019-01-01

Please contact them soon and welcome them to Momentum!
'''
        self.assertEqual(args[7], text)
        self.assertEqual(args[8], htmlify(text))
