from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.db import models, transaction
from django.contrib.postgres.fields.citext import CIEmailField
from solo.models import SingletonModel
from django.utils.crypto import get_random_string
from django.db.models.aggregates import Max
from groups.models import Group, GroupStatusType
from territories.models import Territory
from datetime import datetime
from mxv.utils import postcode_geo, send_mailgun_email, htmlify
import dateutil.parser
import distutils.util

import logging
import pprint
pp = pprint.PrettyPrinter()

# creates members and super users
class MemberManager(BaseUserManager):
    
    # creates a member
    def create_member(self, email, first_name, last_name, password=None):

        if not email:
            raise ValueError('Members must have an email address')

        if not first_name and not last_name:
            raise ValueError('Members must have a name')

        member = self.model(
            email = self.normalize_email(email),
            first_name = first_name,
            last_name = last_name
        )
        member.set_password(password)
        
        # create the NationBuilder link (the new member might already be a supporter)
        query = models.Q(email=email) | \
            models.Q(email1=email) | \
            models.Q(email2=email) | \
            models.Q(email3=email) | \
            models.Q(email4=email)
        matches = NationBuilderPerson.objects.filter(query)
        if matches.exists():
            member.nationbuilderperson = matches.first()
        else:
            logging.error(f'Creating member - could not find for email {member.email}!')
            member.nationbuilderperson = NationBuilderPerson.objects.create(email = member.email)
        
        member.save(using=self._db)

        return member

    # creates a superuser
    def create_superuser(self, email, first_name, last_name, password):

        member = self.create_member(
            email,
            password = password,
            first_name = first_name,
            last_name = last_name
        )
        member.is_superuser = True
        member.is_active = True
        
        member.save(using=self._db)
        return member

# default to 20 digit activation keys
activation_key_length = 20
def activation_key_default():
    return Member.objects.make_random_password(length = activation_key_length)

# a member identified uniquely by their email address and publicly by their name 
class Member(AbstractBaseUser, PermissionsMixin):
    email = CIEmailField(verbose_name='email address', max_length=255, unique=True)
    nationbuilderperson = models.OneToOneField('NationBuilderPerson', related_name='newmember',blank=True,null=True,on_delete=models.CASCADE)
    first_name = models.CharField(max_length=255, null=True)
    last_name = models.CharField(max_length=255, null=True)
    is_active = models.BooleanField(default=False)
    activation_key = models.CharField(max_length=activation_key_length, default=activation_key_default)
    last_emailed = models.DateField(blank=True, null=True, default=None)
    is_ncg = models.BooleanField(default=False, verbose_name = 'NCG')
    is_members_council = models.BooleanField(default=False, verbose_name = "Members' council (can act on behalf of the member's council)")
    new_login_email = CIEmailField(max_length=255, blank=True, null=True, default=None)
    login_email_verification_key = models.CharField(max_length=activation_key_length, blank=True, null=True, default=None)
    is_anonymised = models.BooleanField(default=False, verbose_name = "Personal data has been anonymised")

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']
    
    objects = MemberManager()

    # identify the member publicly by their name
    @property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    @property
    def short_name(self):
        return f'{self.first_name[0]} {self.last_name}'

    def __str__(self):
        return f'{self.first_name} {self.last_name} ({self.email})'

    anonymise_string_length = 20
    def anonymise_user(self):
        self.first_name = get_random_string(self.anonymise_string_length)
        self.last_name = get_random_string(self.anonymise_string_length)
        self.email = get_random_string(self.anonymise_string_length) +'@void.com'
        self.is_active = False
        self.is_anonymised = True
        self.save()

    @property
    def is_staff(self):
        return self.is_superuser

    class Meta:
        indexes = [
            models.Index(fields=['email']),
        ]

# returns a random token that has not yet been used as a unique token
# still vulnerable to race conditions from other web requests but better than assuming no collisions
unique_token_length = 20
def unused_unique_token():
    token = get_random_string(unique_token_length)
    while NationBuilderPerson.objects.filter(unique_token = token).first():
        token = get_random_string(unique_token_length)
    return token

class NationBuilderTag(models.Model):
    name = models.TextField(max_length=255, unique=True)
    first_used = models.DateField(null=True, blank=True)
    last_used = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.name
        
    class Meta:
        indexes = [models.Index(fields=['name'])]

class MembershipStatusType(models.TextChoices):
    Active = "active"
    Affiliate = "affiliate"
    Cancelled = "cancelled"
    Suspended = "suspended"

def is_member(status):
    return status and (status == MembershipStatusType.Active or status == MembershipStatusType.Affiliate)

class MembershipPaymentCycleType(models.TextChoices):
    Monthly = "Monthly"
    Yearly = "Yearly"

class PaymentProcessorType(models.TextChoices):
    GoCardless = "gocardless"
    PayPal = "paypal"

# a member's link to their NationBuilder record
class NationBuilderPerson(models.Model):
    email = CIEmailField(max_length=255, null=True, blank=True) # duplicate of member.email so that supporters can be promoted to members when joining
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    unique_token = models.CharField(max_length = unique_token_length, default = unused_unique_token)
    # TODO: this should not be nullable, and should be unique, maybe even the primary key
    nation_builder_id = models.IntegerField(blank=True, null=True, default=None)
    phone_number = models.CharField(max_length=32, null=True, blank=True)
    mobile_number = models.CharField(max_length=32, null=True, blank=True)
    email1 = CIEmailField(max_length=255, null=True, blank=True)
    email2 = CIEmailField(max_length=255, null=True, blank=True)
    email3 = CIEmailField(max_length=255, null=True, blank=True)
    email4 = CIEmailField(max_length=255, null=True, blank=True)
    email_opt_in = models.BooleanField(null=True, blank=True)
    mobile_opt_in = models.BooleanField(null=True, blank=True)
    do_not_call = models.BooleanField(null=True, blank=True)
    do_not_contact = models.BooleanField(null=True, blank=True)
    primary_address1 = models.CharField(max_length=255, null=True, blank=True)
    primary_address2 = models.CharField(max_length=255, null=True, blank=True)
    primary_address3 = models.CharField(max_length=255, null=True, blank=True)
    primary_city = models.CharField(max_length=255, null=True, blank=True)
    primary_postcode = models.CharField(max_length=10, null=True, blank=True)
    primary_country = models.CharField(max_length=32, null=True, blank=True)
    membership_number = models.CharField(max_length=16, null=True, blank=True)
    join_date = models.DateField(null=True, blank=True, default=None)
    membership_payment_lapsed_date = models.DateField(null=True, blank=True, default=None)
    membership_status = models.CharField(max_length=32, choices=MembershipStatusType.choices, null=True, blank=True)
    membership_payment_processor = models.CharField(max_length=32, choices=PaymentProcessorType.choices, null=True, blank=True)
    membership_payment_amount = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    membership_payment_cycle = models.CharField(max_length=32, choices=MembershipPaymentCycleType.choices, null=True, blank=True)
    gocardless_id = models.CharField(max_length=32, null=True, blank=True)
    paypal_id = models.CharField(max_length=32, null=True, blank=True)
    messagebird_id = models.CharField(max_length=32, null=True, blank=True)
    tags = models.ManyToManyField(NationBuilderTag, through='Tagging')

    def __str__(self):
        return f'{self.email} ({self.nation_builder_id})'

    class Meta:
        indexes = [
            models.Index(fields=['nation_builder_id']),
            models.Index(fields=['email']),
            models.Index(fields=['email1']),
            models.Index(fields=['email2']),
            models.Index(fields=['email3']),
            models.Index(fields=['email4']),
        ]

class Tagging(models.Model):
    person = models.ForeignKey(NationBuilderPerson, on_delete=models.CASCADE)
    tag = models.ForeignKey(NationBuilderTag, on_delete=models.CASCADE)
    tag_date = models.DateField()

def populate_from_nb(nb, email, nb_person):
    nb_record = nb.GetFromEmail(email)
    if not nb_record:
        raise Exception("Unable to contact NationBuilder")
    nb_data_to_postgres(nb_person, nb_record)

def present(nb_record, nb_field):
    return bool(nb_field in nb_record and nb_record[nb_field])

def maybe_assign(nb_person, object_field, nb_record, nb_field=None):
    if not nb_record:
        return
    if not nb_field:
        nb_field = object_field
    if present(nb_record, nb_field):
        vars(nb_person)[object_field] = nb_record[nb_field]

def maybe_bool_assign(nb_person, field, nb_record):
    if present(nb_record, field):
        value = nb_record[field]
        if type(value) is bool:
            boolval = value
        else:
            boolval = bool(distutils.util.strtobool(value))
    else:
        boolval = False
    vars(nb_person)[field] = boolval

def maybe_date_assign(nb_person, object_field, nb_record, nb_field=None):
    if not nb_field:
        nb_field = object_field
    if not present(nb_record, nb_field):
        return
    intval = int(float(nb_record[nb_field]))
    if intval > 1000000000000:
        intval /= 1000
    dateval = datetime.fromtimestamp(intval)
    vars(nb_person)[object_field] = dateval

def assign_nontag_fields(nb_person, nb_record):
    maybe_assign(nb_person, 'nation_builder_id', nb_record, 'id')
    maybe_assign(nb_person, 'email', nb_record)
    if present(nb_record, 'my_momentum_unique_token') and len(nb_record['my_momentum_unique_token']) <= unique_token_length:
       nb_person.unique_token = nb_record['my_momentum_unique_token']
    maybe_assign(nb_person, 'first_name', nb_record)
    maybe_assign(nb_person, 'last_name', nb_record)
    maybe_assign(nb_person, 'phone_number', nb_record, 'phone')
    maybe_assign(nb_person, 'mobile_number', nb_record, 'mobile')
    maybe_assign(nb_person, 'email1', nb_record)
    maybe_assign(nb_person, 'email2', nb_record)
    maybe_assign(nb_person, 'email3', nb_record)
    maybe_assign(nb_person, 'email4', nb_record)
    maybe_bool_assign(nb_person, 'email_opt_in', nb_record)
    maybe_bool_assign(nb_person, 'mobile_opt_in', nb_record)
    maybe_bool_assign(nb_person, 'do_not_call', nb_record)
    maybe_bool_assign(nb_person, 'do_not_contact', nb_record)
    if 'primary_address' in nb_record:
        primary_address = nb_record['primary_address']
        maybe_assign(nb_person, 'primary_address1', primary_address, 'address1')
        maybe_assign(nb_person, 'primary_address2', primary_address, 'address2')
        maybe_assign(nb_person, 'primary_address3', primary_address, 'address3')
        maybe_assign(nb_person, 'primary_city', primary_address, 'city')
        maybe_assign(nb_person, 'primary_postcode', primary_address, 'zip')
        maybe_assign(nb_person, 'primary_country', primary_address, 'country_code')
    else:
        # this is only during the backfill import - remove later
        maybe_assign(nb_person, 'primary_address1', nb_record)
        maybe_assign(nb_person, 'primary_address2', nb_record)
        maybe_assign(nb_person, 'primary_address3', nb_record)
        maybe_assign(nb_person, 'primary_city', nb_record)
        maybe_assign(nb_person, 'primary_postcode', nb_record, 'primary_zip')
        maybe_assign(nb_person, 'primary_country', nb_record, 'primary_country_code')
    maybe_assign(nb_person, 'membership_number', nb_record, 'momentum_membership_number')
    maybe_date_assign(nb_person, 'join_date', nb_record, 'momentum_joined_date')
    maybe_date_assign(nb_person, 'membership_payment_lapsed_date', nb_record)
    maybe_assign(nb_person, 'membership_payment_amount', nb_record)
    if present(nb_record, 'membership_payment_cycle'):
        nb_person.membership_payment_cycle = MembershipPaymentCycleType(nb_record['membership_payment_cycle'])
    if present(nb_record, 'membership_payment_processor'):
        nb_person.membership_payment_processor = PaymentProcessorType(nb_record['membership_payment_processor'])
        if nb_record['membership_payment_processor'] == 'gocardless':
            maybe_assign(nb_person, 'gocardless_id', nb_record, 'membership_payment_id')
        elif nb_record['membership_payment_processor'] == 'paypal':
            maybe_assign(nb_person, 'paypal_id', nb_record, 'membership_payment_id')
    maybe_assign(nb_person, 'messagebird_id', nb_record)
    if present(nb_record, 'membership_payment_status'):
        oldstatus = nb_person.membership_status
        newstatus = MembershipStatusType(nb_record['membership_payment_status'])
        nb_person.membership_status = newstatus
        membership_status_changed(nb_person, oldstatus)

def nb_data_to_postgres(nb_person, nb_record):
    assign_nontag_fields(nb_person, nb_record)
    save_person(nb_person, nb_record)
    if update_tags(nb_person, nb_record):
        save_person(nb_person, nb_record)

def save_person(nb_person, nb_record):
    try:
        nb_person.save() 
    except Exception as e:
        logging.error(f'Unable to save NB record {nb_record["id"]}: {e}')

def update_tags(nb_person, nb_record):
    if not present(nb_record, 'tags'):
        return False
    tag_date = dateutil.parser.parse(nb_record['updated_at']).date()
    if isinstance(nb_record['tags'], list):
        tags = nb_record['tags'] # an NB webhook
    else:
        tags = [tag.strip() for tag in nb_record['tags'].split(',')] # backfill script
    tags.sort()
    create_names = set()
    updates = set()
    for tag in tags:
        tagfilter = NationBuilderTag.objects.filter(name=tag)
        if not tagfilter.exists():
            create_names.add(tag)
        else:
            dbTag = tagfilter.first()
            dbTag.last_used = tag_date
            updates.add(dbTag)
    taggings = create_tags(tag_date, create_names)
    taggings.extend(update_existing_tags(updates))
    create_taggings(nb_person, tag_date, taggings)
    return len(taggings) > 0    

def create_tags(tag_date, create_names):
    """Return NBTag model objects, with ids, with the names in the input list"""
    clist = list(create_names)
    creates = [ NationBuilderTag(name=name, first_used=tag_date, last_used=tag_date) for name in clist ]
    try:
        return NationBuilderTag.objects.bulk_create(creates)
    except Exception as e:
        # For some reason the bulk_create sometimes fails with apparently bogus duplicates.
        # In which case we try again, then fetch the newly created tags out of the DB
        logging.error(f'Bulk create tags failed: {e}. Retrying')
        taggings = NationBuilderTag.objects.bulk_create(creates, ignore_conflicts=True)
        logging.error(f'{len(taggings)} of the {len(creates)} creates happened')
        query = models.Q()
        for newname in create_names:
            query |= models.Q(name=newname)
        return list(NationBuilderTag.objects.filter(query).iterator())

def update_existing_tags(updates):
    update_list = list(updates)
    try:
        NationBuilderTag.objects.bulk_update(update_list, ['last_used'])
    except Exception as e:
        logging.error(f'Bulk update tags failed: {e}')
    return update_list

def create_taggings(nb_person, tag_date, taggings):
    extant_taggings = Tagging.objects.filter(person=nb_person)
    dbTaggings = []
    for tagging in extant_taggings:
        tagging.delete()
    for tag in taggings:
        new_tagging = Tagging(person=nb_person, tag=tag, tag_date=tag_date)
        dbTaggings.append(new_tagging)
    try:
        Tagging.objects.bulk_create(dbTaggings)
    except Exception as e:
        logging.error(f'Tagging creates failed: {e}')

def ensure_nationbuilder_person(nb, member):
    if hasattr(member, 'nationbuilderperson') and member.nationbuilderperson:
        if member.nationbuilderperson.nation_builder_id:
            if nb.PersonFieldsAndValues(member.nationbuilderperson.nation_builder_id):
                # Case 1: everything's ok
                return
            # Case 2: the nationbuilder id isn't actually in the real NB
        # Case 3: there's an attached NB record, but it's missing the NB ID
        populate_from_nb(nb, member.email, member.nationbuilderperson)
        return
    nb_filter = NationBuilderPerson.objects.filter(email=member.email)
    if nb_filter.count():
        nb_person = nb_filter[0]
        if not nb_person.nation_builder_id:
            # Case 4: detached record has no ID
            populate_from_nb(nb, member.email, nb_person)
        # Case 4, and 5: detached record has ID
        nb_person.member = member
        member.nationbuilderperson = nb_person
        nb_person.save()
        member.save()
        return
    # Case 6: there is no NB record
    nb_record = nb.GetFromEmail(member.email)
    member.nationbuilderperson = NationBuilderPerson.objects.create(
        member=member, email=nb_record['email'], nation_builder_id=nb_record['id'])
    member.save()

# TODO this ought not to live in the models file
def membership_status_changed(nb_person, old_status):
    if not is_member(old_status) and is_member(nb_person.membership_status):
        member_on_new(nb_person)
    elif is_member(old_status) and not is_member(nb_person.membership_status):
        member_on_cancel(nb_person)
    
def member_on_new(nb_person):
    if not settings.NEW_MEMBER_EMAIL or not nb_person.primary_postcode:
        return
    try:
        geo = postcode_geo(nb_person.primary_postcode)
    except Exception as e:
        # probably an invalid postcode
        logging.error(e)
        return
    query = models.Q(status=GroupStatusType.Verified) | \
            models.Q(status=GroupStatusType.ScotlandVerified) | \
            models.Q(status=GroupStatusType.WalesVerified)
    groups = Group.objects.filter(query).exclude(dpa__isnull=True)
    matches = set()
    for group in groups:
        for territory in group.territories.exclude(lookup_field='scottish_parliamentary_constituency'):
            if geo[territory.lookup_field] == territory.name:
                matches.add(group)
    if geo["european_electoral_region"] == "Scotland":
        sgeo = postcode_geo(nb_person.primary_postcode, scotland=True)
        for group in groups:
            for territory in group.territories.filter(lookup_field='scottish_parliamentary_constituency'):
                if sgeo[territory.lookup_field] == territory.name:
                    matches.add(group)
    # TODO put special handling if >1 match, not a problem as of Feb 2021
    if len(matches) != 1:
        return
    dpa = matches.pop().dpa
    target_email = dpa.email
    text = f'''
Dear %recipient.salutation%,

A new member has joined Momentum in your group's territory!

Name: {display(nb_person.first_name)} {display(nb_person.last_name)}
Email: {display(nb_person.email)}
Mobile: {display(nb_person.mobile_number)}
Phone: {display(nb_person.phone_number)}
Postcode: {display(nb_person.primary_postcode)}
Join date: {display(nb_person.join_date)}

Please contact them soon and welcome them to Momentum!
'''
    # Use MailGun instead of Postmark, as the email isn't going to the member themself
    send_mailgun_email(target_email, dpa.first_name, {}, {}, settings.DEFAULT_FROM_EMAIL, None, "A new member has signed up", text, htmlify(text))

def display(text):
    return text if text else ''

def member_on_cancel(nb_person):
    # TODO: send a cancellation/suspension email
    pass

# UI choices for profile fields
class ProfileFieldType(models.TextChoices):
    Char = "Single line text"
    Integer = "Integer"
    Decimal = "Decimal"
    Boolean = "Checkbox (true/false)"
    Email = "Email"
    Date = "Date"
    
# returns the next unused display order
def next_profile_field_display_order():
    highest = ProfileField.objects.aggregate(Max('display_order'))['display_order__max']
    return 1 if not highest else highest + 1

# fields in the members' NationBuilder records that are editable by the member on their profile page
class ProfileField(models.Model):
    field_path = models.CharField(max_length = 255)
    field_type = models.CharField(max_length = 8, 
        choices = [(choice.name, choice.value) for choice in ProfileFieldType], default = ProfileFieldType.Char.name)
    required = models.BooleanField(default = False)
    display_text = models.CharField(max_length = 255, default = '')
    display_order = models.IntegerField(default = next_profile_field_display_order)
    admin_only = models.BooleanField(default = False)
    is_phone_number = models.BooleanField(default = False)
    negate_value = models.BooleanField(default = False, help_text = 'Use with Checkbox fields only, returns True instead of False and vice versa')
    
    # defines instance-level non-database fields
    def __init__(self, *args, **kwargs):
        super(ProfileField, self).__init__(*args, **kwargs)
        self.value_string = ''
        self.is_member_field = False
    
    def __str__(self):
        return '%d - %s = %s' % (self.display_order, self.field_path, self.value_string)

# update details campaign
class UpdateDetailsCampaign(SingletonModel):
    fields_page_header = models.TextField()
    fields_page_footer = models.TextField()
    redirect_url = models.CharField(max_length = 255)
    
    # returns the URL parameters as a URL parameter string with values from the request GET overridden by the profile fields 
    def url_parameter_string(self, request, field_values = None):
        url_parameters_present = []
        
        # for each URL parameter...
        for url_parameter in self.url_parameters.all().order_by('name'):
            
            # if its name is in the GET...
            if url_parameter.name in request.GET:
                
                # use the value from the GET
                value = request.GET[url_parameter.name]
                
                # possibly override the value if field values were supplied
                if field_values:
                
                    # use the matching field value if present
                    field = self.fields.filter(matching_url_parameter_name = url_parameter.name).first()
                    if field:
                        value = field_values[field.field_path]
                        
                # set a default value if none was supplied in the request and one is set for the URL parameter
                if (not value or value == '') and url_parameter.default_value_if_no_nation_builder_value:
                    value = url_parameter.default_value_if_no_nation_builder_value

                url_parameters_present.append((url_parameter.name, value))
        
        # build the URL parameter string
        url_parameter_string = '&'.join('='.join(present) for present in url_parameters_present)
        return url_parameter_string

# returns the next unused display order
def next_campaign_tag_group_display_order():
    highest = CampaignTagGroup.objects.aggregate(Max('display_order'))['display_order__max']
    return 1 if not highest else highest + 1
  
# a group of campaign tags
class CampaignTagGroup(models.Model):
    campaign = models.ForeignKey(UpdateDetailsCampaign, related_name = 'tag_groups', on_delete=models.CASCADE)
    header = models.TextField()
    footer = models.TextField()
    display_order = models.IntegerField(default = next_campaign_tag_group_display_order)
      
    def __str__(self):
        return '%d' % self.display_order
  
# returns the next unused display order
def next_campaign_tag_display_order():
    highest = CampaignTag.objects.aggregate(Max('display_order'))['display_order__max']
    return 1 if not highest else highest + 1
  
# sets a tag in nation builder if checked by the member
class CampaignTag(models.Model):
    group = models.ForeignKey(CampaignTagGroup, related_name = 'tags', on_delete=models.CASCADE)
    display_text = models.CharField(max_length = 255)
    tag = models.CharField(max_length = 255)
    display_order = models.IntegerField(default = next_campaign_tag_display_order)
    
    # defines instance-level non-database fields
    def __init__(self, *args, **kwargs):
        super(CampaignTag, self).__init__(*args, **kwargs)
        self.value_string = ''
      
    def __str__(self):
        return '%d - %s / %s = %s' % (self.display_order, self.display_text, self.tag, self.value_string)

# returns the next unused display order
def next_campaign_field_display_order():
    highest = CampaignField.objects.aggregate(Max('display_order'))['display_order__max']
    return 1 if not highest else highest + 1

# fields in the members' NationBuilder records that are editable by the member on the update details campaign page
class CampaignField(models.Model):
    campaign = models.ForeignKey(UpdateDetailsCampaign, related_name = 'fields', on_delete=models.CASCADE)
    field_path = models.CharField(max_length = 255)
    field_type = models.CharField(max_length = 8, choices = [(choice.name, choice.value) for choice in ProfileFieldType], default = ProfileFieldType.Char)
    required = models.BooleanField(default = False)
    display_text = models.CharField(max_length = 255, default = '')
    matching_url_parameter_name = models.CharField(max_length = 100, blank=True, null=True, default=None, help_text = 'The name of the matching URL parameter to update when this field changes')
    display_order = models.IntegerField(default = next_campaign_field_display_order)
    is_phone_number = models.BooleanField(default = False)
    negate_value = models.BooleanField(default = False, help_text = 'Use with Checkbox fields only, returns True instead of False and vice versa')
    
    # defines instance-level non-database fields
    def __init__(self, *args, **kwargs):
        super(CampaignField, self).__init__(*args, **kwargs)
        self.value_string = ''
    
    def __str__(self):
        return '%d - %s = %s' % (self.display_order, self.field_path, self.value_string)
    
# the URL parameters to pass on when redirecting 
# populated manually since the campaign is a singleton: 
#   insert into members_urlparameter (consultation_id, name, nation_builder_value) select 1, name, nation_builder_value from mxv_defaulturlparameter;
class UrlParameter(models.Model):
    campaign = models.ForeignKey(UpdateDetailsCampaign, related_name='url_parameters', on_delete=models.CASCADE)
    name = models.CharField(max_length = 100, help_text = 'The name of the URL parameter to pass on when redirecting')
    nation_builder_value = models.CharField(max_length = 100, blank=True, null=True, default=None, help_text = 'The value for this parameter in the NationBuilder URL above')
    default_value_if_no_nation_builder_value = models.CharField(max_length = 100, blank=True, null=True, default=None, help_text = 'The value for this parameter if NationBuilder does not supply one')
    
    def __str__(self):
        return self.name

