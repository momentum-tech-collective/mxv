# Generated by Django 3.0.7 on 2020-10-22 11:03

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('questions', '0002_text_validations'),
    ]

    operations = [
        migrations.AlterField(
            model_name='candidate',
            name='member',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='june_2020_candidate', to=settings.AUTH_USER_MODEL),
        ),
    ]
