from django import test
from django.core import mail
from rest_framework import status
from rest_framework.test import APITestCase
from members.models import Member
from groups.models import *
from groups.views import *
import mxv.test_common
import os

@test.override_settings(POLICY_PRIMARY_SUBMISSIONS_VISIBLE=True)
class EmailTest(APITestCase):

    def setUp(self):
        self.dpa = mxv.test_common.create_test_member('keir@hardie.com', 'Keir', 'Hardie')
        self.nondpa = mxv.test_common.create_test_member('ramsay@macdonald.com', 'Ramsay' ,'MacDonald')
        self.group = Group.objects.create(name='Croydon', dpa=self.dpa)
        self.policy = {
            'motion_title': 'Free Broadband', 
            'motion_body': 'This group resolves that Labour should support free broadband'}
    
    def tearDown(self):
        Group.objects.all().delete()
        Member.objects.all().delete()

    def test_policy_motion(self):
        self.client.force_authenticate(user=self.dpa)

        response = self.client.post('/groups/policy/', self.policy, format='json')

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(1, len(mail.outbox))
        self.assertEqual(os.environ.get('CONFERENCE_MOTIONS_EMAIL'), mail.outbox[0].to[0])
        self.assertEqual('CROYDON MOTION SUBMISSION: "Free Broadband"', mail.outbox[0].subject)
        expected = '''
This is an automated submission from the MyMomentum conference policy motions form.

-- BEGINS --

SUBMITTED BY MEMBER: Keir Hardie (keir@hardie.com)

MEMBER'S LOCAL GROUP: Croydon

MOTION TITLE: Free Broadband

MOTION BODY: 
This group resolves that Labour should support free broadband

-- ENDS --
            '''
        self.assertEqual(expected, mail.outbox[0].body)

    def test_policy_motion_non_dpa(self):
        self.client.force_authenticate(user=self.nondpa)

        response = self.client.post('/groups/policy/', self.policy, format='json')

        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)
        self.assertEqual(0, len(mail.outbox))

    def test_rulechange_motion(self):
        self.client.force_authenticate(user=self.nondpa)
        rulechange = {
            'motion_title': 'Open Selections', 
            'clp_name': 'Croydon Central',
            'local_group_name': 'Croydon',
            'discussed_with_local': True,
            'agreement_with_local': False,
            'motion_argument': 'Because I said so',
            'motion_body': 'Labour should openly select MPs',
            'motion_permissible_by_lp': True,
            'motion_notes': "Because I'm always right",
        }

        response = self.client.post('/groups/rulechange/', rulechange, format='json')

        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_rulechange_motion_send(self):
        self.client.force_authenticate(user=self.nondpa)
        rulechange = {
            'motion_title': 'Open Selections', 
            'clp_name': 'Croydon Central',
            'local_group_name': 'Croydon',
            'discussed_with_local': True,
            'agreement_with_local': False,
            'motion_argument': 'Because I said so',
            'motion_body': 'Labour should openly select MPs',
            'motion_permissible_by_lp': True,
            'motion_notes': "Because I'm always right",
        }

        response = self.client.post('/groups/rulechange/', rulechange, format='json')
        
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(1, len(mail.outbox))
        self.assertEqual(os.environ.get('CONFERENCE_RULE_CHANGES_EMAIL'), mail.outbox[0].to[0])
        self.assertEqual('RULE CHANGE SUBMISSION: Open Selections', mail.outbox[0].subject)
        expected = '''
This is an automated submission from the MyMomentum conference rules change form.

-- BEGINS --

RULE CHANGE SUBMISSION: Open Selections

SUBMITTED BY MEMBER: Ramsay MacDonald (ramsay@macdonald.com)

MEMBER'S CLP: Croydon Central
MEMBER'S LOCAL GROUP: Croydon
MEMBER DISCUSSED WITH LOCAL GROUP: Yes
MEMBER REACHED AGREEMENT WITH LOCAL GROUP: No

MOTION TITLE: 
Open Selections

"Please give the argument behind your motion": 
Because I said so

"Please write out here your suggested rule change": 
Labour should openly select MPs

"Are you confident this rule change be permitted by the Labour Party?": Yes

"Are there any other political reasons why you think Momentum should support this rule change?":
Because I'm always right

-- ENDS --
            '''

        self.assertEqual(expected, mail.outbox[0].body)
