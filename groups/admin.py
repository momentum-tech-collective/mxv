from django.contrib import admin
from django.db.models import Q
from groups.models import *
from territories.models import *


class GroupInline(admin.TabularInline):
    model = Group.territories.through
    show_change_link = True
    can_delete = False
    extra = 0


def parameteriseString(readableStr):
    return readableStr.lower().replace(' ', '_')


def makeChoice(item):
    return (parameteriseString(item), item)


def BooleanFilterFactory(name, query):
    class MyFilter(admin.SimpleListFilter):
        title = name
        parameter_name = parameteriseString(name)

        def lookups(self, request, admin):
            return (
                ('Yes', 'Yes'),
                ('No', 'No'),
            )

        def queryset(self, request, qset):
            if self.value() == 'Yes':
                return qset.exclude(query)
            elif self.value() == 'No':
                return qset.filter(query)
    return MyFilter


VerifiedFilter = BooleanFilterFactory(
    'Verified',
    ~(Q(status=GroupStatusType.Verified.name) | Q(status=GroupStatusType.WalesVerified.name)
      | Q(status=GroupStatusType.ScotlandVerified.name))
)

HasDpaFilter = BooleanFilterFactory(
    'Has DPA',
    Q(dpa=None)
)

RegionFilter = ('territories__region', admin.RelatedOnlyFieldListFilter)


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    search_fields = ('name', 'territories__name', 'dpa__name', 'dpa__email', 'key_contacts__name',
                     'key_contacts__email', 'social_media_managers__name', 'social_media_managers__email', 'territories__region__name')
    autocomplete_fields = ('territories', 'exclude_territories',
                           'dpa', 'key_contacts', 'social_media_managers',)
    list_display = ('name', 'status', 'notes', 'last_updated', 'dpa', 'region')
    list_filter = [VerifiedFilter, HasDpaFilter, RegionFilter]
    ordering = ('name',)
