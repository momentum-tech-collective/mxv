from django.db import models
from django.contrib.postgres.fields.citext import CIEmailField
from mxv.utils import short_text_length, long_text_length
from territories.models import Territory
from django.conf import settings

class GroupStatusType(models.TextChoices):
    Verified = "Verified"
    WalesVerified = "Wales Verified"
    ScotlandVerified = "Scotland Verified"
    Registered = "Registered"
    Unregistered = "Unregistered"
    Pending = "Pending"
    DetailsNeeded = "On Hold - Details needed"
    Problems = "PROBLEMS - Group on Hold"
    Query = "Query"
    Legacy = "Legacy"
    Unauthorised = "Unauthorised"


class Group(models.Model):
    name = models.CharField(max_length=short_text_length)
    status = models.CharField(max_length=32, choices=GroupStatusType.choices)
    territories = models.ManyToManyField(Territory,blank=True, related_name='groups_including',
        help_text='Territories to include')
    exclude_territories = models.ManyToManyField(Territory,blank=True, related_name='groups_excluding',
        help_text='Territories to exclude')
    notes = models.TextField(null=True, blank=True, max_length=long_text_length)
    dpa = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL, related_name='+')
    key_contacts = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='+')
    social_media_managers = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='+')
    facebook_url = models.URLField(null=True, blank=True, max_length=short_text_length)
    group_email = CIEmailField(null=True, blank=True, max_length=short_text_length)
    twitter_handle = models.CharField(null=True, blank=True, max_length=short_text_length)
    website_url = models.URLField(null=True, blank=True, max_length=short_text_length)
    last_updated = models.DateField(null=True, blank=True)

    def get_region(self):
        territories = self.territories.values_list()
        for territory in territories:
            territory_region = territory[3]
            if territory_region != None:
                return Territory.objects.get(pk=territory_region).name
            else:
                return "Not Set"

    region = property(get_region)

    def __str__(self):
        return self.name
