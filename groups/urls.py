from django.conf.urls import url
from groups import views

urlpatterns = [
    url(r'^policy/', views.SubmitPolicyMotion.as_view()),
    url(r'^rulechange/', views.SubmitRuleChangeMotion.as_view()),
]
