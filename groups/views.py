from django.conf import settings
from django.core.mail import send_mail
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from groups.models import Group
from mxv.utils import success, fail

import logging

def boolean_to_yesno(form_boolean):
    return 'Yes' if form_boolean else 'No'

class SubmitPolicyMotion(APIView):
    def post(self, request, format=None):

        if settings.POLICY_PRIMARY_SUBMISSIONS_VISIBLE == False:
            return fail("Forbidden")

        member = request.user

        if not Group.objects.filter(dpa=member).exists():
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        group = Group.objects.get(dpa=member)

        values = request.data

        try:
            subject = f'{group.name.upper()} MOTION SUBMISSION: "{values["motion_title"]}"'

            text = f'''
This is an automated submission from the MyMomentum conference policy motions form.

-- BEGINS --

SUBMITTED BY MEMBER: {member}

MEMBER'S LOCAL GROUP: {group.name}

MOTION TITLE: {values['motion_title']}

MOTION BODY: 
{values['motion_body']}

-- ENDS --
            '''

            send_mail(subject, text, settings.DEFAULT_FROM_EMAIL, [settings.CONFERENCE_MOTIONS_EMAIL])
            logging.info("SubmitPolicyMotion: sending mail")
            return success()

        except Exception as e:
            logging.error(e)
            return fail(e)

class SubmitRuleChangeMotion(APIView):
    def post(self, request, format=None):
        
        if settings.POLICY_PRIMARY_SUBMISSIONS_VISIBLE == False:
            return fail("Forbidden")
 
        member = request.user
        
        subject = f'{member.full_name}: Labour Rule Change - {request.data["motion_title"]}'
        values = request.data
        
        try:
            subject = f'RULE CHANGE SUBMISSION: {values["motion_title"]}'

            text = f'''
This is an automated submission from the MyMomentum conference rules change form.

-- BEGINS --

RULE CHANGE SUBMISSION: {values['motion_title']}

SUBMITTED BY MEMBER: {member}

MEMBER'S CLP: {values['clp_name']}
MEMBER'S LOCAL GROUP: {values['local_group_name']}
MEMBER DISCUSSED WITH LOCAL GROUP: {boolean_to_yesno(values['discussed_with_local'])}
MEMBER REACHED AGREEMENT WITH LOCAL GROUP: {boolean_to_yesno(values['agreement_with_local'])}

MOTION TITLE: 
{values['motion_title']}

"Please give the argument behind your motion": 
{values['motion_argument']}

"Please write out here your suggested rule change": 
{values['motion_body']}

"Are you confident this rule change be permitted by the Labour Party?": {boolean_to_yesno(values['motion_permissible_by_lp'])}

"Are there any other political reasons why you think Momentum should support this rule change?":
{values['motion_notes']}

-- ENDS --
            '''

            send_mail(subject, text, settings.DEFAULT_FROM_EMAIL, [settings.CONFERENCE_RULE_CHANGES_EMAIL])
            logging.info("SubmitRuleChangeMotion: sending mail")
            return success()

        except Exception as e:
            logging.error(e)
            return fail(e)
