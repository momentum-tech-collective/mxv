# MxV - Momentum's digital democracy platform

## What's this?

In physics, the equation for *momentum* is `m x v` - mass times velocity.

This  digital platform was used initially to help in the generation of ideas for Momentum's submissions to the Labour Party Democracy Review. Since then, it has been used for consultations with members. We hope to add more features to make it a fully-fledged portal for Momentum members.

## Get involved!

Do you have development skills (see below for the technologies we've used)?  Then get involved!  This is your chance to put your skills to use in the political arena in order to further Momentum's and the Labour Party's aims.  This is *not* a theoretical exercise, your contributions *will* be used!

## Developer setup

### Prerequisites

- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [GitLab account](https://gitlab.com/users/sign_up)
- Python 3.7 ([Os X](http://docs.python-guide.org/en/latest/starting/install3/osx/), [Windows](http://docs.python-guide.org/en/latest/starting/install3/win/) or [Linux](http://docs.python-guide.org/en/latest/starting/install3/linux/))
- [Pipenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/)
- [Postgres](https://devcenter.heroku.com/articles/heroku-postgresql#local-setup)
- [Yarn v1](https://classic.yarnpkg.com/en/docs/install) (note: on Ubuntu and Debian it is called yarnpkg rather than yarn)
- Node 12 (you can easily install and use this with `nvm`)

### Setup

1. `git clone https://gitlab.com/momentum-tech-collective/mxv` to get the code.
2. `cd mxv`, `pipenv --python 3.9` and `pipenv install` to configure a virtual environment.
    -  If you have an error with -lssl, see https://stackoverflow.com/questions/11538249/python-pip-install-psycopg2-install-error
3. Copy the example environment files in `example.env` and `mxv/example.env` to `.env` and `mxv/.env` respectively, and fill them out. You may want to use `heroku config -a mxv-staging -s` to get the environment variables from heroku (if you have permission to do so)
4. `mkdir -p client/build && yarn python manage.py collectstatic` to complete local Django setup.
5. Set up postgresql on your machine if you haven't already. See e.g. https://wiki.archlinux.org/title/PostgreSQL
   If you get permission errors, make sure you are running postgres as your OS expects (e.g. on most linuxes, run it with `systemctl start postgres`).
6. Create a Postgres database called 'mxv' and a Postgres user called 'mxv' with a password of 'mxv' and grant all permissions to that user in the new database (these settings are only for the local database so don't worry about the obvious password!).
   Run these commands to set up the database and the extensions we need:

  ```
  $ sudo -iu postgres psql <<EOF
  \c template1
  -- We need the citext extension on the main mxv database and in every test
  -- and backup database.
  -- Creating it on template1 means it will be in every database we create and
  -- that means yarn dbpull and yarn test will work much better.
  create extension citext;
  create database mxv;
  create user mxv with password 'mxv';
  grant all privileges on database mxv to mxv;
  alter user mxv createdb;
  EOF
  ```

7. Install `nvm` and run `nvm install 12` and `nvm use 12`.
   You may want to use a tool like `direnv` to do this automatically for you on entering the `mxv` directory.
7. `yarn python manage.py migrate` to populate the database with tables by running the migrations.
8. `yarn python manage.py createsuperuser` to add yourself as an admin.
9. `yarn install`
10. `yarn develop` to run the application locally.
	- Site accessible at [http://localhost:8000](http://localhost:8000).
	- Admin site accessible at [http://localhost:8000/admin](http://localhost:8000/admin).
	- React code at [http://localhost:3000/app](http://localhost:3000/app) - see below
10. `yarn test` to run unit tests, of which there are only a few. You'll need to create extension citext as described above.

### To pull data from prod

`yarn dbpull` will create a new database in your local server with prod data.

For this to work you'll need to have added the `citext` extension to the template database first as instructed above.

If you don't do this first, you can manually create that extension on the `mxv_backup_BLAH` and `test_mxv_backup_BLAH` databases.

### React in the Django app

Entirely new development should be done in React.

The React code lives in the `client` directory. 

In development, it is served from [http://localhost:3000/app](http://localhost:3000/app). In production, it will live at [https://my.peoplesmomentum.com/app]([https://my.peoplesmomentum.com/app). Client-side routing within the `/app` subdirectory will work as standard in React. All other urls continue to be served from Django.

There is no tutorial on the web that has the setup we have here, which is also different from My Campaign Map.

We continue to use a standard `create-react-app` TypeScript skeleton. We did *not* eject it, so we don't have to deal with low-level webpack code. This is not how My Campaign Map and every web tutorial I could find did it.

The `Router` object has the `basename` attribute set to `/app`. So all client-side routing is based off of `/app`. The `proxy` variable is pointing to the Django server, so all server requests automatically go there.

Instead of using Django Forms, we use Django Serializers and Viewsets as found in the [Django Rest Framework](https://www.django-rest-framework.org/).

Django writes a `csrftoken` cookie (we force this server side). All POST requests must include this in the `X-CSRFToken` header.

The production set up is hairier:
- Running `yarn build` will build the React app inside the `client/build` directory. It will then copy that over to Django's usual location for static files, `mxv/staticfiles`.
- We use a little trick based on the fact that the client code is actually found in two places.
- In `settings.py`, `client` is added to the `TEMPLATE_DIRS` variable.
- In `mxv/urls.py`, all `/app` urls are mapped to the `react` method in `mxv/views.py`. This just renders `client/build/index.html`.
- However, in `client/package.json`, the `homepage` attribute is set to `client/static`. The word `static` has to be in there for Django to serve the client as static files.
- That means `client/build/index.html` points to CSS and JS in the `client/static` directory. In the `STATICFILES_DIRS` section of `settings.py`, this is mapped to `mxv/staticfiles/client`. 
- End result is that `/app/xxx` routing works as we want in both dev and prod
- This may sound awful but I'd way rather do this than have to `yarn eject` and then hand-edit webpack config code, incorporate `django-webpack-loader`, etc.

# More fun with react-snap

We would like to run react-snap to allow React pages to have meta tags. There is currently only one such: `/app/lobby`. We put this in the `include` variable in the react-snap config in `client/package.json`.

However, we can't run react-snap at build time. The extra buildpacks needed on Heroku put us over the Heroku slug size limit.

The solution is to run it at dev time, and *check in* the result. Use the `yarn snapshot` command every time you make changes to `/app/lobby/`. So `client/build/app/lobby/index.html`, which is output by react-snap, is checked into the repo.

# Debugging production issues

If you're getting 500s in prod, see if you can reproduce it on staging, which will show you a proper Django stack trace.

If you can't repro in staging, set MXV_DEBUG=True in prod, that will show you the stack traces.

# Command-line scripts

You can write command-line scripts that have the full power of the Django database models. They go in the `scripts` directory. They will touch whatever database is indicated in your `.env` file - including prod. To run a script, just do `yarn script <script-name>` minus the `.py` extension.
