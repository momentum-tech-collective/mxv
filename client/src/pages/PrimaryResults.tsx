import React from 'react';
import { Link, Redirect} from "react-router-dom";
import { Row, Col, Container } from 'reactstrap';
import CandidateCard from '../components/Primary/CandidateCard';
import PrimaryTemplate from '../components/Template/PrimaryTemplate';
import SimpleContainer from '../components/SimpleContainer/SimpleContainer';
import CardTableContainer from '../components/CardTableContainer/CardTableContainer';
import { PrimaryDetail } from '../components/Context';

// # TODO: specify this type
const PrimaryResultsPage: React.FC<{result_primary: PrimaryDetail}> = ({result_primary}) => {
  
  return (
  <>
  { result_primary ?
  <PrimaryTemplate breadcrumbs={[["", "Primaries"], [`/result/${result_primary.id}/#`, `Results`], [`/result/${result_primary.id}/#`, `${result_primary.name}`]]}>
        
    <SimpleContainer link={<Link to="/"><h4 className="backlink">Back</h4></Link>}>
      <h1>{result_primary.name}</h1>
      <p>{result_primary.description}</p>
    </SimpleContainer>

    <CardTableContainer>
      {result_primary.candidates.map((candidate: any) => <CandidateCard candidate={candidate} disabled />)}
    </CardTableContainer>

    <Container className="mt-0 py-4">
      <Row className="justify-content-center">
        <Col sm="12" md="6">
        <><br/></>
        </Col>
      </Row>
    </Container>

  </PrimaryTemplate>
  :
  <Redirect to="/" />
  }
  </>
  )}

export default PrimaryResultsPage
