import React from 'react';
import { Formik, Form } from 'formik';
import { useCookies } from 'react-cookie';
import { Button, Container, FormGroup, Row, Col } from 'reactstrap';
import {SingleLineField, TextAreaField, CheckBoxField, SubmitForm} from '../components/FormComponents/FormComponents'
import Template from '../components/Template/Template'
import MembershipCard from '../components/MembershipCard/MembershipCard'

import * as Yup from 'yup';

type Values = {
  motion_title: string;
  motion_body: string;
  agreement_with_local: boolean;
}

const MotionSubmission: React.FC<{}> = () => {

  const endpoint = "/groups/policy/"
  const hed = "Submit your group's motion"
  const blurb = ''
  
  const [cookies] = useCookies(['csrftoken']);
  
  const formSchema = Yup.object().shape({
    motion_title: Yup.string().trim().required("Please enter your motion's title").max(200, "Please suggest a shorter title for your motion"),
    motion_body: Yup.string().trim().required("Required"),
    agreement_with_local: Yup.boolean().isTrue("All motions submitted by DPAs must be agreed with the local group")
  })

  const handleSubmit = async (values: Values) => {
    return SubmitForm(cookies.csrftoken, endpoint, values);
  }

  return (
    <>
      <Formik
        initialValues={{
          motion_title: "",
          motion_body:"",
          agreement_with_local: false,
        }}
        validationSchema={formSchema}        
        onSubmit={handleSubmit}
      >{({ values, errors }) => (
        <Form>
          <Template breadcrumbs={[["/"]]}>
            <Container>

              <Row className="justify-content-center">

                <Col sm="12" md="6">
                  <h1>{hed}</h1>
                  <p>{blurb}</p>

                  <hr/> 
                  <h2>Contact info</h2>
                  <p>This is the following contact info we have for you:</p>
                  
                  <MembershipCard/>

                  <h2>Your group's motion</h2>
                  <p>Please fill out this form as fully as possible</p>



                  <SingleLineField 
                    property="motion_title" 
                    label="Title of your motion" 
                    errors={errors} 
                  />

                  <TextAreaField
                    property="motion_body"
                    label="Please write out the body of your motion here"
                    errors={errors}
                    word_limit={1000}
                  />

                  <CheckBoxField
                    property="agreement_with_local"
                    label="I am submitting this motion with the agreement of my local group"
                    errors={errors}
                    helptext="Your submitted motion should have been agreed by your local group at a democratic meeting."
                  />

                  <FormGroup>
                    {Object.keys(errors).length > 0 && (<div className="error">Please make sure your entries are correct.</div>)}
                    <Button 
                      disabled={Object.keys(errors).length > 0}
                      type="submit" 
                      id='sendLobbyEmail' 
                      block 
                      className="btn-outline-primary"
                    >
                      Submit motion
                    </Button>
                  </FormGroup>
                </Col>

              </Row>

            </Container>
          </Template>
        </Form>
      )}
      </Formik>
    </>
  )
}

export default MotionSubmission;
