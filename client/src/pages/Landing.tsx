import React, { useContext } from 'react';
import { Row, Container, Button } from 'reactstrap';
import { PrimaryContext, PrimaryDetail } from '../components/Context';
import PrimaryTemplate from '../components/Template/PrimaryTemplate'
import SimpleContainer from '../components/SimpleContainer/SimpleContainer';
import CardTableContainer from '../components/CardTableContainer/CardTableContainer';
import PreviewCard from '../components/PreviewCard/PreviewCard';
import DismissableAlert from '../components/DismissableAlert/DismissableAlert';
import isWithinInterval from 'date-fns/isWithinInterval'

type PrimarySectionProps = {
  active_primaries: PrimaryDetail[],
  inactive_primaries?: PrimaryDetail[],
  activeC2a: string,
  inactiveC2a?: string,
  sectionHrefPrefix: string,
}

const PrimarySection: React.FC<PrimarySectionProps> = (props) => {

  const { active_primaries, activeC2a, sectionHrefPrefix, inactive_primaries, inactiveC2a } = props

  let allPrimaries = [...active_primaries]
  if (inactive_primaries) {
    allPrimaries = [...active_primaries, ...inactive_primaries]
  }

  const classes: String[] = []
  if (allPrimaries.length === 1) {
    classes.push("card-centred")
  }

  return (
    <>
      <SimpleContainer padTop>
        {props.children}
      </SimpleContainer>

      <CardTableContainer>
        {active_primaries.map((primary: PrimaryDetail) => (
          <PreviewCard
            classes={classes.join(" ")}
            key={primary.id}
            title={primary.name}
            body={primary.description}
            buttonColor="primary"
            buttonText={activeC2a}
            buttonHref={sectionHrefPrefix + String(primary.id)}
          />
        ))}
        {inactive_primaries &&
          <>
          {inactive_primaries.map((primary: PrimaryDetail) => (
            <PreviewCard
              disabled
              key={primary.id}
              title={primary.name}
              body={primary.description}
              buttonColor="primary"
              buttonText={inactiveC2a}
              badges={["You voted"]}
              buttonHref={""}
            />
          ))}
        </>
        }
      </CardTableContainer>
    </>
  )
}

const isRegistrable = (primary: PrimaryDetail) => {
  const registrationInterval: Interval = {
    start: Date.parse(primary.registrations_start),
    end: Date.parse(primary.nominations_end)
  }
  return isWithinInterval(Date.now(), registrationInterval)
}

const isNominatable = (primary: PrimaryDetail) => {
  const nominationInterval: Interval = {
    start: Date.parse(primary.nominations_start),
    end: Date.parse(primary.nominations_end)
  }
  return isWithinInterval(Date.now(), nominationInterval)
}

const isVotable = (primary: PrimaryDetail) => {
  const votingInterval: Interval = {
    start: Date.parse(primary.voting_start),
    end: Date.parse(primary.voting_end)
  }
  return isWithinInterval(Date.now(), votingInterval)
}

const Landing: React.FC<{}> = (props) => {

  const { primaries: open_primaries, completed: completed_primaries, cast: cast_primaries } = useContext(PrimaryContext)

  const registrable_primaries = open_primaries.filter(primary => isRegistrable(primary))
  const nominatable_primaries = open_primaries.filter(primary => isNominatable(primary))
  const votable_primaries = open_primaries.filter(primary => isVotable(primary))

  const userHasRegistrablePrimaries = registrable_primaries.length > 0
  const userHasNominatablePrimaries = nominatable_primaries.length > 0
  const userHasVotablePrimaries = votable_primaries.length  > 0
  const userHasCastPrimaries = cast_primaries.length > 0
  const userHasCompletedPrimaries = completed_primaries.length > 0
  const userHasNoPrimaries = !userHasRegistrablePrimaries && !userHasNominatablePrimaries && !userHasVotablePrimaries && !userHasCompletedPrimaries && !userHasCastPrimaries

  const displayPopups = () => {
    const popups = []

    try {
      const params = String(window.location).split("?")[1].split("&")

      params.forEach(param => {
        if (param.startsWith("primary")) {
          const prId = Number(param.split("_")[1])
          const primaryMatches = cast_primaries.filter(pri => pri.id === prId)

          if (primaryMatches && primaryMatches.length === 1) {
            const name = primaryMatches[0].name
            popups.push(<p>You successfully voted in {name}</p>)
          }
        }
      })

      if (params.includes("vote")) {
        popups.push(<>
          <p className="m-0">You have successfully changed your profile.</p>
          <hr />
          <p className="m-0">Due to your changes, you are now eligible for the following primaries below.</p>
        </>)
      }

      return popups.map(item => <DismissableAlert>{item}</DismissableAlert>)
    
    } catch (e) {
    
      return
    
    }
  }

  return (
    <PrimaryTemplate breadcrumbs={[["/", "Primaries"]]}>

        <SimpleContainer link={<a href="/"><h4 className="backlink">Home</h4></a>}>
          
          {displayPopups()}

          <h1>Primaries</h1>
          <p className="mb-0">Momentum is changing. To strengthen our movement, we're putting members at the heart of our organisation and making sure key decisions are taken from the ground up.</p>

        </SimpleContainer>

        {userHasNoPrimaries && 
          <SimpleContainer>
            <h1>No primaries available</h1>
            <p>If you are eligible and have already submitted your ballots, don't worry, we've got your vote. If you think you should be entitled to vote and still haven't, then let us know at <a href="mailto:membership@peoplesmomentum.com">membership@peoplesmomentum.com</a></p>
          </SimpleContainer>
        }

        {userHasRegistrablePrimaries && 
          <PrimarySection active_primaries={registrable_primaries} activeC2a={"Register as a candidate"} sectionHrefPrefix={"register/"}>
            <h1>REGISTER TO STAND</h1>
            <p className="mb-0">You are eligible to put yourself forward as a candidate for these primaries</p>
          </PrimarySection>
        }

        {userHasNominatablePrimaries &&
          <PrimarySection active_primaries={nominatable_primaries} activeC2a={"Nominate a candidate"} sectionHrefPrefix={"nominate/"}>
            <h1>OPEN FOR NOMINATIONS</h1>
            <p className="mb-0">You are eligible to nominate candidates for these primaries</p>
          </PrimarySection>
        }

        {userHasVotablePrimaries &&
          <PrimarySection active_primaries={votable_primaries} inactive_primaries={cast_primaries} activeC2a={"Vote now"} inactiveC2a={"Vote cast"} sectionHrefPrefix={"vote/"}>
            <h1>OPEN PRIMARIES</h1>
            <p className="mb-0">Primaries that you are eligible to vote in</p>
          </PrimarySection>
        }

        {userHasCastPrimaries &&
          <PrimarySection active_primaries={votable_primaries} inactive_primaries={cast_primaries} activeC2a={"Vote now"} inactiveC2a={"Vote cast"} sectionHrefPrefix={"vote/"}>
            <h1>CAST PRIMARIES</h1>
            <p className="mb-0">Primaries that you have cast your vote in</p>
          </PrimarySection>
        }

        { userHasCompletedPrimaries && 
         <PrimarySection active_primaries={completed_primaries} activeC2a={"View results"} sectionHrefPrefix={"result/"}>
            <h1>RESULTS</h1>
            <p className="mb-0">The results of completed primaries </p>
          </PrimarySection>
        }

      <Container fluid>

        <Row>
          <p><br/></p>
        </Row>

      </Container>

    </PrimaryTemplate>
  );
}

export default Landing
