import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import { Form, Formik } from 'formik';
import { useCookies } from 'react-cookie';
import ReactMarkdown from 'react-markdown';
import { PrimaryDetail, Candidate } from '../components/Context'
import { Row, Col, Container, Button, Label, FormGroup } from 'reactstrap';
import CandidateCard from '../components/Primary/CandidateCard';
import PrimaryTemplate from '../components/Template/PrimaryTemplate';
import SimpleContainer from '../components/SimpleContainer/SimpleContainer';
import CardTableContainer from '../components/CardTableContainer/CardTableContainer';
import { Interval, isWithinInterval, format } from 'date-fns'
import { SubmitForm } from '../components/FormComponents/FormComponents'

type SubmissionValues = {
  candidates: Candidate[],
  ranked: boolean,
}

type SubmittableFPTPBallot = {
  ranked: boolean,
  candidates: string[]
}

type SubmittableRankedCandidate = {
  rank: number,
  id: number
}

type SubmittableRankedBallot = {
  ranked: boolean,
  candidates: SubmittableRankedCandidate[]
}

export const getSuffix = (n: number) => {
  const suffixes = ["th", "st", "nd", "rd"]
  const lastNumber = n % 100;
  return String(n) + (suffixes[(lastNumber - 20) % 10] || suffixes[lastNumber] || suffixes[0]);
}

const PrimaryPage: React.FC<{ result_primary: PrimaryDetail }> = ({ result_primary }) => {

  const [cookies] = useCookies(['csrftoken']);

  // TODO this will have to be adjusted twice a year
  // If it is GMT, the time string below will need to be: "T12:00:00.000+00:00"
  // If it is BST, the time string below will need to be: "T12:00:00.000+01:00"
  const dateMaker = (ISOdateString: string) => new Date(ISOdateString + "T23:59:59.000+01:00")
  const startDate: Date = result_primary ? dateMaker(result_primary.voting_start) : new Date()
  const endDate: Date = result_primary ? dateMaker(result_primary.voting_end) : new Date()
  const openInterval: Interval = { start: startDate, end: endDate }
  // TODO: this depends on the browser's date. Server double-checks, but should be tightened with a server time
  const ballotIsOpen: boolean = isWithinInterval(new Date(), openInterval)

  const [confirming, setConfirming] = useState(false);
  const [ballot, setBallot] = useState<Candidate[]>([])

  const toggleConfirming = () => {
    setConfirming(!confirming)
    window.scrollTo(0, 0);
  }

  const candidateFullName = (c: Candidate) => {
    const isPolicy = c.last_name == '-'
    return isPolicy ? c.first_name : `${c.first_name} ${c.last_name}`
  }
  const findOneCandidateByKey = (key: keyof Candidate, value: string | number, list: Candidate[]) => {
    const matches = list.filter(c => c[key] === value)
    if (matches.length === 1) {
      return matches[0]
    }
  }

  const findOneCandidateById = (id: number) => findOneCandidateByKey("id", id, result_primary.candidates)
  const findOneCandidateByRank = (rank: number) => findOneCandidateByKey("rank", rank, ballot)
  const findCandidateOnBallot = (c: Candidate) => findOneCandidateByKey("id", c.id, ballot) ? true : false
  
  const removeCandidateFromBallot = (ballot: Candidate[], candidateToRemove: Candidate) => {
    const filteredBallot = ballot.filter(candidate => candidate.id !== candidateToRemove.id)
    const sortedBallot = filteredBallot.sort((candidateA, candidateB) => candidateA.rank - candidateB.rank)
    const rerankedBallot = sortedBallot.map((candidate, index) => {
      candidate.rank = index
      return candidate
    })
    return rerankedBallot
  }

  const toggleCandidateInBallot = (candToToggle: Candidate) => {
    if (findCandidateOnBallot(candToToggle)) {
      candToToggle.rank = -1
      const newBallot = removeCandidateFromBallot(ballot, candToToggle)
      setBallot(newBallot)
    } else {
      candToToggle.rank = ballot.length
      const appendedCandidates = [...ballot, candToToggle]
      setBallot(appendedCandidates)
    }
  }
  const clearBallot = () => result_primary.candidates.forEach(c => c.rank = -1)

  const breadcrumbs = [
    ["/", "Primaries"],
    ["/", `${result_primary.name}`]
  ]
  
  const votingExplainer = result_primary.ranked_choice ? 
    <>
      <p>You have a maximum of 33 votes to cast in this Policy Primary, and you must cast a minimum of 1. You should rank the motions in order of preference, with your favourite as your ‘1st choice’, your next favourite as your ‘2nd choice’ and so on. Continue voting for policies in this way until you find that none of the remaining policies are acceptable to you. Don’t rank policies you don’t believe Momentum should be supporting.</p>
      <p>The order of your choices matters. You are ranking these policies from "most favourite” to "least favourite”. This ranking will affect how the votes are counted and which motions win. A later preference is considered only if an earlier preference has a surplus above the quota required for election, or is excluded because of insufficient support.</p>
    </>
  :
    <p>Read the candidate statements, then cast your vote below.</p>

  const voteString = (n: Number) => n > 1 ? "votes" : "vote"

  const maxVoteString = `${result_primary.max_votes} ${voteString(result_primary.max_votes)}`
  const maxVotesReached = ballot.length >= result_primary.max_votes
  const voteInstructions = maxVotesReached ?
    <p>Remove a vote to change your ballot</p>
    :
    votingExplainer

  const validateForm = () => {
    const errors: any = {};
    return errors;
  }

  const createSubmittableBallot = (values: SubmissionValues) => {
    if (!values.ranked) {
      const fptpCandidates: string[] = values.candidates.map(c => String(c.id))
      const fptpBallot: SubmittableFPTPBallot = {
        candidates: fptpCandidates,
        ranked: values.ranked
      }
      return fptpBallot
    } else {
      const rankedCandidates: SubmittableRankedCandidate[] = values.candidates.map((c) => {
        const rankedCandidate: SubmittableRankedCandidate = {
          rank: c.rank + 1,
          id: c.id
        }
        return rankedCandidate
      })
      const rankedBallot: SubmittableRankedBallot = {
        candidates: rankedCandidates,
        ranked: values.ranked
      }
      return rankedBallot
    }
  }

  const handleSubmit = async (raw_values: SubmissionValues) => {
    const values = createSubmittableBallot(raw_values)
    SubmitForm(cookies.csrftoken, "/primaries/vote/", values, `/app/primaries?primary${result_primary.id}`);
  }

  const CandidateCards = () => result_primary.candidates.map((candidate: Candidate) =>
    <CandidateCard
      active={findCandidateOnBallot(candidate)}
      candidate={candidate}
      disabled={maxVotesReached}
      displayPrimaryBtn={ballotIsOpen}
      displaySecondaryBtn
      displayPhoto={false}
      displayRank={result_primary.ranked_choice}
      handleClick={(e: any) => {
        e.preventDefault()
        toggleCandidateInBallot(candidate)
      }}
    />
  )

  const BallotCheckbox: React.FC<{ candidate: Candidate }> = (props) => {

    const { candidate } = props

    const fullName = candidateFullName(candidate)
    const isChecked = findCandidateOnBallot(candidate)
    const isDisabled = maxVotesReached && !isChecked

    const onClick = (e: React.MouseEvent) => {
      if (!isDisabled) {
        e.preventDefault()
        toggleCandidateInBallot(candidate)
      }
    }

    return (
      <div
        className="form-check"
        style={isDisabled ? { opacity: 0.65, transition: "opacity 0.3s" } : { transition: "opacity 0.3s" }}
      >

        <span onClick={onClick} >
          <input
            id={`votebox-${fullName}`}
            type="checkbox"
            className={"form-check-input votebox" + maxVotesReached && " disabled"}
            name="candidates"
            disabled={isDisabled}
            checked={isChecked}
            value={candidate.id}
          />
        </span>

        <label className="form-check-label">
          <span style={{ cursor: "pointer" }} onClick={onClick}>
            {fullName}
          </span>
        </label>

      </div>
    )
  }

  const AllBallotCheckboxes: React.FC<{}> = (props) => {

    return <div
      className="mb-3"
      style={{ display: "grid", gridTemplateColumns: "1fr 1fr" }}
    >
      {result_primary.candidates.map((candidate: Candidate) => <BallotCheckbox candidate={candidate} />)}
    </div>
  }

  const CandidateSelect: React.FC<{ position: number }> = (props) => {

    const { position } = props

    const isDisabled = position !== ballot.length

    const [currentCandidate, setCurrentCandidate] = useState<Candidate>()

    useEffect(() => {
      const correspondingBallotCandidate = findOneCandidateByRank(position)
      if (correspondingBallotCandidate) {
        setCurrentCandidate(correspondingBallotCandidate)
      }

    }, [ballot])

    const onChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
      const value = Number(e.target.value)
      const candidate = findOneCandidateById(value)
      if (candidate) {
        toggleCandidateInBallot(candidate)
      }
    }

    const deleteCandidate = () => currentCandidate && toggleCandidateInBallot(currentCandidate)

    const writtenPos = getSuffix(position + 1)
    const property = `${writtenPos}_choice`
    const label = `${writtenPos} choice`

    const remainingCandidates = result_primary.candidates.filter(c => !findCandidateOnBallot(c))
    const options = remainingCandidates.map(c => {
      return <option value={c.id}>{candidateFullName(c)}</option>
    })

    const deleteBtn = <span
      onClick={deleteCandidate}
      style={{ cursor: "pointer", color: "#FF0F80", textDecoration: "underline" }}
    > Delete</span>

    return (
      <FormGroup >
        <Label for={property}>{label}{currentCandidate && deleteBtn}</Label>

        <select name={property} onChange={onChange} disabled={isDisabled}>
          {currentCandidate ?
            <option value={currentCandidate.id}>{candidateFullName(currentCandidate)}</option>
            :
            <>
              <option value={-1}>Choose a policy</option>
              {options}
            </>
          }
        </select>
      </FormGroup>
    )
  }

  const AllBallotSelects: React.FC<{}> = (props) => {

    const result = []

    for (let i = 0; i < result_primary.max_votes; i++) {
      result.push(<CandidateSelect position={i} />)
    }

    return <>{result}</>
  }

  if (!ballotIsOpen) {
    return <PrimaryTemplate breadcrumbs={breadcrumbs}>
      <SimpleContainer>
        <h2>This ballot is not open yet</h2>
      </SimpleContainer>
    </PrimaryTemplate>
  }

  return (
    <Formik enableReinitialize initialValues={{ candidates: ballot, ranked: result_primary.ranked_choice }} onSubmit={handleSubmit} validate={validateForm}>
      {() => (
        <Form>
          { !confirming ?
            <PrimaryTemplate breadcrumbs={breadcrumbs}>
              <SimpleContainer link={<Link onClick={clearBallot} to="/"><h4 className="backlink">Back</h4></Link>}>
                <h1>{result_primary.name}</h1>
                <p><ReactMarkdown source={result_primary.description} /></p>
                {voteInstructions}
              </SimpleContainer>
              <CardTableContainer>
                {CandidateCards()}
              </CardTableContainer>
              <Container className="mt-0 py-4">

                <Row className="justify-content-center">

                  <Col sm="12" md="6">
                    <h2 id="your-votes">Your votes</h2>
                    <p>You have {maxVoteString} to cast in this primary.</p>
                    {voteInstructions}
                    {result_primary.ranked_choice ?
                      <AllBallotSelects />
                      :
                      <AllBallotCheckboxes />
                    }

                  </Col>

                </Row>

                <Row className="justify-content-center">

                  <Col sm="12" md="6">
                    <Button
                      block
                      color="primary"
                      outline
                      disabled={ballot.length < 1}
                      onClick={toggleConfirming}
                    >
                      {ballot.length < 1 ? "Choose a candidate" : "Cast your vote"}
                    </Button>
                  </Col>

                </Row>

              </Container>
            </PrimaryTemplate>
            :
            <PrimaryTemplate breadcrumbs={breadcrumbs}>

              <SimpleContainer
                link={<button className="btn-backlink" onClick={toggleConfirming}><h4>Back</h4></button>}>
                <h1>{result_primary.name}</h1>
                <p><ReactMarkdown source={result_primary.description} /></p>
              </SimpleContainer>

              <CardTableContainer>
                {ballot.map((c: Candidate) => <CandidateCard 
                    displayPhoto={false} 
                    displayRank={result_primary.ranked_choice}
                    candidate={c} 
                  />)
                }
              </CardTableContainer>

              <SimpleContainer>
                <h2 id="your-ballot">Your ballot</h2>
                <p>You are casting your {voteString(ballot.length)} {result_primary.ranked_choice ? "in the following order" : "for"}:</p>

                {result_primary.ranked_choice ?
                  <ol>
                    {ballot.map(c => <li>{candidateFullName(c)}</li>)}
                  </ol>
                  :
                  <ul>
                    {ballot.map(c => <li>{candidateFullName(c)}</li>)}
                  </ul>
                }
                <p>Is this correct?</p>
                <input type="submit" className="btn btn-outline-primary btn-block" value="Submit Ballot" />
                <Button block outline onClick={toggleConfirming}>Return to ballot</Button>
              </SimpleContainer>

            </PrimaryTemplate>
          }
        </Form>
      )}
    </Formik>
  )
}

export default PrimaryPage
