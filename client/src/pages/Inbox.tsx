import React, { useEffect, useState } from 'react';
import Template from '../components/Template/Template';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import { InboxContext, MemberMessage } from '../components/Context';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import InboxMessage from './InboxMessage';

const jsonOptions = {
  method: 'get',
  headers: {
    'Content-Type': 'application/json'
  }
}

const InboxEntry: React.FC<{ message: MemberMessage }> = (props) => {

  const message = props.message
  const link = `message/${message.id}`

  return (
    <div className="py-3">
      <Link to={link} style={{ color: "unset" }}>
        <h2>{message.title}</h2>
        <h4>Sent {message.published_at}</h4>
        <p><em>{message.standfirst}</em></p>
      </Link>
      <h4><Link to={link}>Read more</Link></h4>
    </div>
  )
}

const InboxMessages: React.FC<{ messages: MemberMessage[] }> = (props) => {

  const allMessages = props.messages.map((message: MemberMessage, i: number, arr: MemberMessage[]) => {
    return (
      <>
        <hr />
        <InboxEntry message={message} />
        {i === arr.length - 1 && <hr />}
      </>
    )
  })

  return <>{allMessages}</>
}

const InboxTemplate: React.FC<{}> = (props) => {

  const breadcrumbs = [["/", "Inbox"]]

  return (
    <Template breadcrumbs={breadcrumbs}>
      <>
        <Container>
          <Row>
            <Col lg={{ size: 6, offset: 3 }} md={{ size: 9, offset: 2 }}>
              <div className="py-4">
                <h4 ><a className="backlink" href="/">Back</a></h4>
              </div>
              <h1>Inbox</h1>
              <p className="py-4">Please find all your messages from Momentum</p>
              {props.children}
            </Col>
          </Row>
        </Container>
      </>
    </Template>
  )
}


const Inbox: React.FC<{}> = (props) => {
  const [loaded, setLoaded] = useState(false)
  const [messages, setMessages] = useState<MemberMessage[]>([])

  const doFetch = async () => {
    try {
      const memberMessageResults = await fetch('/publications/memberMessages/', jsonOptions)
      if (memberMessageResults.status === 401) {
        window.location.href = "/members/login"
      }
      const memberMessageData = await memberMessageResults.json()
      setMessages(memberMessageData)

      setLoaded(true)
    } catch (error) {
      console.error(error)
      setMessages([])
    }
  }

  useEffect(() => {
    if (!loaded) {
      doFetch()
    }
  }, [loaded])

  return (
    <InboxContext.Provider value={{
      messages: messages,
      loaded: loaded,
      invalidateContext: () => setLoaded(false)
    }}>
      <Router basename="/app/inbox">
        <Switch>
          <Route exact path="/">
            <InboxTemplate>{loaded ? <InboxMessages messages={messages} /> : <h2>Loading...</h2>}</InboxTemplate>
          </Route>
          <Route
            path="/message/:id"
            render={(props) => {
              return (
                <InboxMessage
                  message={messages.filter((message: MemberMessage) => message.id === Number(props.match.params.id))[0]}
                />
              )
            }}
          />
        </Switch>
      </Router>
    </InboxContext.Provider>
  );

}

export default Inbox;
