import React from 'react';
import Template from '../components/Template/Template';
import { Container, Row, Col } from 'reactstrap';
import { MemberMessage } from '../components/Context';
import { Link } from 'react-router-dom';
import ReactMarkdown from 'react-markdown';

const InboxMessage: React.FC<{ message: MemberMessage }> = (props) => {

  const breadcrumbs = [["/", "Inbox"], ["#", props.message.title]]

  return (
    <Template breadcrumbs={breadcrumbs}>
      <>
        <Container>
          <Row>
            <Col lg={{ size: 6, offset: 3 }}>
              <div className="py-4">
                <h4><Link className="backlink" to="">Return to inbox</Link></h4>
              </div>
              <h1>{props.message.title}</h1>
              <h4 className="py-4">Sent {props.message.published_at}</h4>
              <ReactMarkdown source={props.message.body_text} />
              <hr />
              <div className="py-4">
                <h4><Link className="backlink" to="">Return to inbox</Link></h4>
              </div>
            </Col>
          </Row>
        </Container>
      </>
    </Template>
  )
}

export default InboxMessage;
