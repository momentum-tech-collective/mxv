import React from 'react';
import { Formik, Form } from 'formik';
import { useCookies } from 'react-cookie';
import { Button, Container, FormGroup, Row, Col } from 'reactstrap';
import {SingleLineField, TextAreaField, CheckBoxField, OptionField, SubmitForm } from '../components/FormComponents/FormComponents'
import Template from '../components/Template/Template'
import MembershipCard from '../components/MembershipCard/MembershipCard'

import * as Yup from 'yup';

type Values = {
  clp_name: string;
  local_group_name: string;
  discussed_with_local: boolean;
  agreement_with_local: boolean;
  motion_title: string;
  motion_argument: string;
  motion_body: string;
  motion_permissible_by_lp: "Yes" | "No" | "Not sure";
  motion_notes: string;
}

const RulesChangeSubmission: React.FC<{}> = () => {

  const endpoint = "/groups/rulechange/"
  const hed = 'Propose a rule change for Labour Party Conference 2021'
  const threeYearLink = 'https://peoplesmomentum.com/wp-content/uploads/2021/01/3-year-rule-explainer-for-2021-Conference-1.pdf'
  const blurb = <p>Before beginning, members are strongly encourage to read the <a target='_blank' rel='noopener noreferrer' href={threeYearLink}>guidance on the 3 year rule</a>.</p>
  
  const [cookies] = useCookies(['csrftoken']);
  
  const formSchema = Yup.object().shape({
    clp_name: Yup.string().trim().required("Required").max(75, "Maximum 75 characters"),
    local_group_name: Yup.string().trim().required("Required").max(200),
    motion_title: Yup.string().trim().required("Please enter your motion's title").max(200, "Please suggest a shorter title for your motion"),
    motion_argument: Yup.string().trim().required("Required"),
    motion_body: Yup.string().trim().required("Required"),
  })

  const handleSubmit = async (values: Values) => {
    return SubmitForm(cookies.csrftoken, endpoint, values);
  }

  return (
    <>
      <Formik
        initialValues={{
          clp_name: "",
          local_group_name: "",
          discussed_with_local: false,
          agreement_with_local: false,
          motion_title: "",
          motion_argument: "",
          motion_body:"",
          motion_permissible_by_lp: "No",
          motion_notes: "",
        }}
        validationSchema={formSchema}        
        onSubmit={handleSubmit}
      >{({ values, errors }) => (
        <Form>
          <Template breadcrumbs={[["/"]]}>
            <Container>

              <Row className="justify-content-center">

                <Col sm="12" md="6">
                  <h1>{hed}</h1>
                  {blurb}

                  <hr/> 
                  <h2>Contact info</h2>
                  <p>This is the following contact info we have for you:</p>
                  
                  <MembershipCard/>

                  <h2>Your motion</h2>
                  <p>Please fill out this form as fully as possible</p>

                  <SingleLineField 
                    property="clp_name" 
                    label="Your CLP" 
                    errors={errors} 
                  />

                  <SingleLineField 
                    property="local_group_name" 
                    label="Name of your local Momentum group" 
                    errors={errors} 
                  />

                  <CheckBoxField 
                    property="discussed_with_local" 
                    label="I have discussed this motion with my local group" 
                    errors={errors}
                  />

                  <CheckBoxField 
                    property="agreement_with_local" 
                    label="I am submitting this motion with the agreement of my local group" 
                    errors={errors} 
                  />

                  <SingleLineField 
                    property="motion_title" 
                    label="Title of your motion" 
                    errors={errors} 
                  />

                  <TextAreaField 
                    property="motion_argument" 
                    label="Please give the argument behind your motion"
                    errors={errors} 
                    word_limit={200}
                  />

                  <TextAreaField
                    property="motion_body"
                    label="Please write out here your suggested rule change"
                    errors={errors}
                    word_limit={1000}
                  />

                  <OptionField
                    property="motion_permissible_by_lp"
                    label="Are you confident this rule change will be permitted by the Labour Party?"
                    options={["Yes", "No", "Not sure"]}
                    errors={errors}
                    helptext={<p style={{ fontSize: "0.8em" }}>Please check <a href={threeYearLink} target='_blank' rel='noopener noreferrer'>this link</a>.</p>}
                  /> 

                  <TextAreaField
                    property="motion_notes"
                    label="Are there any other political reasons why you think Momentum should support this rule change?"
                    errors={errors}
                    word_limit={400}
                  />

                  <FormGroup>
                    {Object.keys(errors).length > 0 && (<div className="error">Please make sure your entries are correct.</div>)}
                    <Button 
                      disabled={Object.keys(errors).length > 0}
                      type="submit" 
                      id='sendLobbyEmail' 
                      block 
                      className="btn-outline-primary"
                    >
                      Submit rule change
                    </Button>
                  </FormGroup>
                </Col>

              </Row>

            </Container>
          </Template>
        </Form>
      )}
      </Formik>
    </>
  )
}

export default RulesChangeSubmission;
