import React, { ReactNode } from 'react';
import { Field } from 'formik';
import { FormGroup, Label} from 'reactstrap';

type FieldProps = {
  property: string;
  label: string;
  field_validator?: any;
  errors?: any;
  helptext?: ReactNode
}

type StringFieldProps = FieldProps & {
  word_limit?: number;
}

type SelectFieldProps = FieldProps & {
  options: string[]
}

export const SingleLineField: React.FC<FieldProps> = (props) => {
  return (
    <FormGroup>
      <Label for={props.property}>{props.label}</Label>
      {props.helptext}
      {props.errors && props.errors[props.property] && (<div className="error">{props.errors[props.property]}</div>)}
      <Field className="form-control" id={props.property} name={props.property} placeholder={props.label} validation={props.field_validator && props.field_validator}/>
    </FormGroup>
  )
}

export const TextAreaField: React.FC<StringFieldProps> = (props) => {

  const wordLimitValidator = (value: string) => {
    if (props.word_limit) {
      const count = value.match(/\S+/g)?.length
      if (count && count > props.word_limit) {
        return `Please ensure your entry is less than ${props.word_limit} words. Currently it is ${count} words long.`
      }
    } else if (props.field_validator) {
      return props.field_validator(value)
    } 
  }

  return(
    <FormGroup>
      <Label for={props.property}>{props.label}</Label>
      {props.helptext}
      {props.word_limit && <p style={{ fontSize: "0.8em" }}>Please ensure your answer is less than {props.word_limit} words long.</p>}
      {props.errors && props.errors[props.property] && (<div className="error">{props.errors[props.property]}</div>)}
      <Field className="form-control" as="textarea" id={props.property} name={props.property} validate={wordLimitValidator && wordLimitValidator}/>
    </FormGroup>
  )

}

export const CheckBoxField: React.FC<FieldProps> = (props) => {

  return (
    <>
      {props.helptext}
      {props.errors && props.errors[props.property] && (<div className="error">{props.errors[props.property]}</div>)}
      <FormGroup className={"form-check"}>
        <Field
          name={props.property}
          component="input"
          type="checkbox"
          validation={props.field_validator && props.field_validator}
        />
        <label htmlFor={props.property} className="form-check-label">{props.label}</label>
      </FormGroup>
    </>
  )

}

export const OptionField: React.FC<SelectFieldProps> = (props) => {

  return (
    <FormGroup>
      <Label for={props.property}>{props.label}</Label>
      {props.helptext}
      {props.errors && props.errors[props.property] && (<div className="error">{props.errors[props.property]}</div>)}
      <Field name={props.property} as="select" children={props.options.map((optiontext: string) => <option>{optiontext}</option>)}/>
    </FormGroup>
  )
}

export async function SubmitForm<T>(csrftoken: string, endpoint: string, values: T, redirect?: string) {

  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-CSRFToken': csrftoken,
    },
    body: JSON.stringify(values)
  }

  const response = await fetch(endpoint, options)
  if (!response.ok) {
    console.log(JSON.stringify(response));
    alert("We had trouble sending your submission. Please check your submission and try again");
    return;
  }
  alert("Submission sent successfully. Thank you.");
  window.location.href = `${redirect ? redirect : "/"}`;
}
