import React, { FunctionComponent } from 'react';
import {Container, Row, Col } from 'reactstrap';

type SimpleContainerProps = {
  link?: any 
  padTop?: boolean
}

const SimpleContainer: FunctionComponent<SimpleContainerProps> = (props) => {

  return (
    <Container>

      { props.link ? 

        <Row>

          <Col sm={{size: 12}} md={{size: 6, offset: 3}}>
            <span>{props.link}</span>
          </Col>

        </Row>
        : 
        props.padTop ? <Row><Col><br /></Col></Row> : null 
      }


      <Row className="my-3">

        <Col sm={{ size: 12 }} md={{ size: 6, offset: 3 }}>
          {props.children}
        </Col>

      </Row>

    </Container>
  )
}

export default SimpleContainer