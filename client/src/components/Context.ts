import React from 'react'

// TODO at this point the server is only giving us eligible primaries the user hasn't voted in
// may eventually include voted primaries, marked as such

// TODO see if there's a way to auto-generate this file from the server's json

export type ContextList = {
    loaded: boolean
    invalidateContext: () => void
}

export type Candidate = {
    id: number,
    first_name: string,
    last_name: string,
    statement: string,
    photo: string,
    votes?: number
    bame?: boolean,
    woman?: boolean,
    rank: number,
}

export type PrimaryDetail = {
    candidates: Array<Candidate>
    description: string,
    id: number,
    max_votes: number,
    min_nominations: number,
    name: string,
    nominations_end: string,
    nominations_start: string,
    registrations_start: string,
    voting_start: string,
    voting_end: string,
    ranked_choice: boolean,
}

export type PrimaryList = ContextList & {
    primaries: Array<PrimaryDetail>
    completed: Array<PrimaryDetail>
    cast: Array<PrimaryDetail>
}

export type MemberMessage = {
    id: number,
    title: string,
    body_text: string,
    standfirst: string,
    published_at: string
}

export type InboxList = ContextList & {
    messages: MemberMessage[]
}

const initialPrimaryContext: PrimaryList = {
    primaries: [],
    completed: [],
    cast: [],
    loaded: false,
    invalidateContext: () => { }
}

const initialInboxContext: InboxList = {
    messages: [],
    loaded: false,
    invalidateContext: () => { }
}

export const PrimaryContext = React.createContext(initialPrimaryContext)
export const InboxContext = React.createContext(initialInboxContext)
