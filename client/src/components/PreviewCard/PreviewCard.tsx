import React from 'react';
import { Button, Card, CardBody, CardTitle } from 'reactstrap';
import { Link } from 'react-router-dom';

type PreviewCardProps = {
  title: string,
  body: string,
  active?: boolean,
  disabled?: boolean,
  buttonText?: string,
  buttonColor?: string,
  buttonHref?: string,
  badges?: Array<string>,
  style?: React.CSSProperties,
  classes?: String
}

const PreviewCard = (props: PreviewCardProps) => {

  if (props.buttonHref === undefined) {
    props.buttonHref = "/"
  }

  const isDisabled = (props.disabled === true && !props.active)

  const disabledStyle = (conditions: Boolean) => {
    if (conditions) {
      return { opacity: 0.65, transition: "opacity 0.2s" }
    }
    else {
      return { transition: "opacity 0.2s" }
    }
  }

  return <Card 
      className={`m-4 ${props.classes}`} 
      outline={props.active} 
      color={props.active ? "primary" : undefined}
      style={{borderWidth: 2, ...props.style}}
  >

    <CardTitle></CardTitle>

    <CardBody>
      <div className="card-row-title">
        <CardTitle style={disabledStyle(isDisabled)}>{props.title}</CardTitle>
      </div>
      <div className="card-row-body">
        <p className="body-sm-1" style={disabledStyle(isDisabled)}>{props.body}</p>
      </div>
    </CardBody>

    {props.buttonText ?
      <div className="card-button-wrapper">
        <Link style={{ textDecoration: 'none' }} to={props.buttonHref}>
          <Button
            active={props.active}
            block
            color={props.buttonColor}
            outline
            disabled={isDisabled}
          >
            {props.buttonText}
          </Button>
        </Link>


      </div>
      : null
    }

  </Card>
}

export default PreviewCard