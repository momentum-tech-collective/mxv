import React, { useState } from 'react';
import ReactMarkdown from 'react-markdown';
import { Button, Card, CardBody, CardTitle, Modal, ModalHeader, ModalBody, ModalFooter, Badge } from 'reactstrap';
import { Candidate } from '../Context';
import { getSuffix } from '../../pages/Primary'

type CandidateCardProps = {
  active?: boolean,
  disabled?: boolean,
  displayPrimaryBtn?: boolean,
  displaySecondaryBtn?: boolean,
  candidate: Candidate,
  handleClick?: any
  displayPhoto?: boolean
  displayRank?: boolean
}

const CandidateCard: React.FC<CandidateCardProps> = (props) => {

  const [modal, setModal] = useState(false)
  function toggleModal() {
    setModal(!modal)
  }

  const candidate = props.candidate
  const candidateCode = candidate.first_name + candidate.last_name
  // TODO decide on proper dimensions for this photo. The server can send a url that matches, with Cloudinary doing
  // the cropping/scaling
  
  const isDisabled = (props.disabled === true && !props.active)

  const disabledStyle = (conditions: Boolean) => {
    if (conditions) {
      return { opacity: 0.65, transition: "opacity 0.2s" }
    }
    else {
      return { transition: "opacity 0.2s" }
    }
  }

  const displayPhoto = props.displayPhoto !== undefined ? props.displayPhoto : true
  
  const providePhoto = (url: string) => {
    if (url !== "" && displayPhoto) {
      return <img className="card-img-top img-fluid" src={url} alt="" />
    }
  }

  const photoUrl = candidate.photo || ""


  const statementExcerpt = (wordcount: number) => {
    const statementArray = candidate.statement.split(" ")
    if (statementArray.length > wordcount) {
      return statementArray.slice(0, wordcount).join(" ") + "..."
    } else {
      return candidate.statement
    }
  }

  const pills = <div>
    {props.displayRank && props.candidate.rank + 1 > 0 ? 
      <Badge pill color="primary" >Ranked {getSuffix(candidate.rank + 1)}</Badge> 
      : 
      <Badge pill color="white" style={{color: 'white', opacity:0}}>{"0"}</Badge>
    }
    {props.candidate.bame && <Badge pill color="light" style={disabledStyle(isDisabled)} className="ml-2">IDs as BAME</Badge> }
    {props.candidate.woman && <Badge pill color="light" style={disabledStyle(isDisabled)} className="ml-2">IDs as a woman</Badge> }
  </div>

  const policyMode = candidate.last_name === "-"
  
  const voteBtnText = policyMode ? "this policy" : candidate.first_name
  const statementBtnText =  policyMode ? "Read the policy" : `${candidate.first_name}'s statement`
  const candidateFullName = policyMode ? candidate.first_name : `${candidate.first_name} ${candidate.last_name}`
  
  return (
    <Card 
      className="m-4 candidate-card" 
      id={`card-${candidateCode}`} 
      outline={props.active} 
      color={props.active ? "primary" : undefined}
      style={{borderWidth: 2}}
    >

      {providePhoto(photoUrl)}

      <CardTitle></CardTitle>

      <CardBody>
        <div className="card-row-title">
          <CardTitle style={disabledStyle(isDisabled)}>{candidateFullName}</CardTitle>
          {pills}
        </div>
        <div className="card-row-body">
          <p className="body-sm-1" style={disabledStyle(isDisabled)}>{statementExcerpt(40)}</p>
        </div>
      </CardBody>
      {(props.displayPrimaryBtn || props.displaySecondaryBtn) &&
        <div className="card-button-wrapper">
          { props.displayPrimaryBtn &&
            <Button 
              active={props.active}
              block
              color="primary"
              outline
              disabled={isDisabled} 
              href="#" 
              onClick={props.handleClick}
            >
              {isDisabled ? "Max. votes used" : props.active ? `Remove Vote` : `Vote for ${voteBtnText}`}
            </Button>
          }
          { props.displaySecondaryBtn &&
            <Button onClick={toggleModal} outline block>
              {statementBtnText}
            </Button>
          }
        </div>
        }

      <Modal centred isOpen={modal} ontoggle={toggleModal} backdrop={true} returnFocusAfterClose={false}>
        <ModalHeader ontoggle={toggleModal}>{candidateFullName}</ModalHeader>
        <ModalBody>
          <p style={{ lineHeight: "135%" }}><ReactMarkdown source={candidate.statement} /></p>
        </ModalBody>
        <ModalFooter>
          <Button outline block onClick={toggleModal}>Close</Button>
        </ModalFooter>
      </Modal>

    </Card>
  )
}

export default CandidateCard