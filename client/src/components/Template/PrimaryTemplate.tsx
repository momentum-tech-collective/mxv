import React, { FunctionComponent, useContext, useEffect } from 'react';
import { Container, Row, Col, Spinner } from 'reactstrap';
import { useLocation } from 'react-router-dom';
import { Header, Footer } from './Template';
import { PrimaryContext}  from '../Context';

type TemplateProps = {
  breadcrumbs: Array<Array<string>>
}

const PrimaryTemplate:FunctionComponent<TemplateProps> = (props) => {

  const { loaded } = useContext(PrimaryContext)

  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return(
    <>
      <Header breadcrumbs={props.breadcrumbs} />
      { loaded ? 
        props.children :
        <Container fluid>
          <Row className="justify-content-center p-0">
            <Col className="p-0" style={{minHeight: "70vh"}}>
              <Spinner style={{ width: '100px', height: '100px', marginLeft: "calc(50vw - 50px)", marginTop: "calc(35vh - 50px)" }} color="primary" />
              <p className="p-0 mt-4" style={{ textAlign: "center" }}>Loading...</p>
            </Col>
          </Row>
        </Container>
      }
      <Footer />
    </>
  )
}

export default PrimaryTemplate