import React, { FunctionComponent, useEffect, useState } from 'react';
import { Navbar, NavbarBrand, Container, Row, Col, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link, useLocation } from 'react-router-dom';

type TemplateProps = {
  breadcrumbs: Array<Array<string>>
}

const Template:FunctionComponent<TemplateProps> = (props) => {

  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return(
    <>
      <Header breadcrumbs={props.breadcrumbs} />
      {props.children}
      <Footer />
    </>
  )
}

export const Header:FunctionComponent<TemplateProps> = (props) => {

  const displayBreadcrumbs = (breadcrumbs : Array<Array<string>>) => {
    if (breadcrumbs) {
      return props.breadcrumbs.map((item: Array<string>, i: number) => {
        const [url, name] = item;
        return (
          <BreadcrumbItem
            key={i}
            active={i + 1 === breadcrumbs.length}
            className="m-0 p-0">
            <Link to={url}>{name}</Link>
          </BreadcrumbItem>
        )
      })
    } else {
      return null
    }
  }

  return (
  <>
    <Navbar className="navbar navbar-expand-lg navbar-light bg-light" style={{ borderBottom: "thin solid #E5E5E5" }}>
    <Container>
      <NavbarBrand>
        <img src="https://my.peoplesmomentum.com/static/mxv/images/logo.b4a8b19baa05.png" style={{ maxWidth: 150 }} alt="Logo" />
      </NavbarBrand>
      </Container>
    </Navbar>
    <Container fluid className="m-0 bg-light py-2 px-0" style={{ borderBottom: "thin solid #E5E5E5" }}>
      <div className="pseudocontainer">
        <Row>
          <Col sm="12" lg="6">
            <Breadcrumb className="p-0 m-0" listClassName="p-0 m-0">
              <BreadcrumbItem className="p-0 m-0"><a href="/">Home</a></BreadcrumbItem>
              {displayBreadcrumbs(props.breadcrumbs)}
            </Breadcrumb>
          </Col>
        </Row>
      </div>
    </Container>
  </>
  );
}  

export const Footer = () => {
  const [imprint, setImprint] = useState('...')

  useEffect(() => {
    const getImprint = async () => {
      try {
        const res = await fetch('/imprint')
        const text = await res.text()
        setImprint(text)
      } catch (err) {
        console.log('Error:', err)
      }
    }
    getImprint()
  }, [])

  return (
    <footer>
      <Container fluid className="grey-backing pt-4 pseudocontainer">
        <Row className="justify-content-center">
          <Col sm="12">
            <p className="body-sm-1">
              Data entered on this site will be processed in accordance with our <a href="https://www.peoplesmomentum.com/privacy-policy" target="_blank" rel="noopener noreferrer">Privacy policy</a>.
              { imprint }
            </p>
          </Col>
        </Row>
      </Container>
    </footer>
  )
}

export default Template
