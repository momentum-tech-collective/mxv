import React, { useState, FunctionComponent } from 'react';
import { Alert } from 'reactstrap';

type DismissableAlertProps = {
  color?: string
}

const DismissableAlert: FunctionComponent<DismissableAlertProps> = (props) => {
  const [visible, setVisible] = useState(true);

  const onDismiss = () => setVisible(false);

  return (
    <Alert color={props!.color} isOpen={visible} toggle={onDismiss}>
      {props.children}
    </Alert>
  );
}

export default DismissableAlert;