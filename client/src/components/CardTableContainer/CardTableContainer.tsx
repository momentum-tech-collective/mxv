import React, { FunctionComponent } from 'react';
import {Container} from 'reactstrap';

type CardTableContainerProps = {
}


const CardTableContainer: FunctionComponent<CardTableContainerProps> = (props) => {

  return (
    <Container fluid className="grey-backing mb-0" style={{ borderBottom: "thin solid #E5E5E5", borderTop: "thin solid #E5E5E5" }}>
      <div className="card-table">
        {props.children}
      </div>
    </Container>
  )
}

export default CardTableContainer