import React, {useState, useEffect} from 'react';

type MemberInfo = {
  member_name: string, 
  member_phone?: string,
  member_email: string,
  member_address?: {
    line_1?: string,
    line_2?: string,
    line_3?: string,
    city?: string,
    zip?: string
  },
  member_memno: string
}


const MembershipCard: React.FC<{}> = (props) => { 

  const jsonOptions = {
    method: 'get',
    headers: {
      'Content-Type': 'application/json'
    }
  }

  const [memberInfo, setMemberInfo] = useState<MemberInfo>({ member_email: "", member_name: "", member_phone: "", address: {line_1: "", line_2: "", line_3: "", city: "", zip: ""}})

  const getMemberInfo = async () => {
    try {
      const memberinfo_response = await fetch('/members/memberinfo/', jsonOptions)
      const memberinfo_data: MemberInfo = await memberinfo_response.json()
      setMemberInfo(memberinfo_data)
    } catch (e) {
      console.error(e)
    }
  }

  useEffect(() => {
    getMemberInfo()
  },[])


  return (
    <>
    <div className="card" id="membership-card">
          <h2 className="p-0" id="mc_fullname">{memberInfo.member_name}</h2>
          <p className=" m-0 p-0 mc_address"><br/>
            {memberInfo.member_address?.line_1 && <><span>{memberInfo.member_address.line_1}</span><br/></> }
            {memberInfo.member_address?.line_2 && <><span>{memberInfo.member_address.line_2}</span><br /></>}
            {memberInfo.member_address?.line_3 && <><span>{memberInfo.member_address.line_3}</span><br /></>}
            {memberInfo.member_address?.city && <><span>{memberInfo.member_address.city}</span><br /></>}
            {memberInfo.member_address?.zip && <><span>{memberInfo.member_address.zip}</span><br /></>}
          </p>
          <p className=" m-0 p-0" id="mc_contact">
              <span className="primary-lighter-30">Email:</span> {memberInfo.member_email}<br/>
              <span className="primary-lighter-30">Tel:</span> {memberInfo.member_phone}
          </p>

          <p className=" m-0 p-0" id="mc_memno">
            <span className="primary-lighter-30 m-0 p-0 label-1">Membership no.:</span><br/>{memberInfo.member_memno}
          </p>
      </div>
      <h4 className="my-2"><a href="/members/profile">Update details ↷</a></h4>
  </>
  )
}

export default MembershipCard