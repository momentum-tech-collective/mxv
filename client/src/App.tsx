import React from 'react';
import { BrowserRouter as Router, Switch,  Route, Redirect} from "react-router-dom";
import Primaries from './Primaries';
import './App.css';
import RuleChangeSubmission from './pages/RuleChangeSubmission';
import MotionSubmission from './pages/MotionSubmission'
import Inbox from './pages/Inbox'

const App = () => {
  return (
    <Router basename="/app">
      <Switch>
        <Route path="/primaries">
          <Primaries />
        </Route>
        <Route path="/rulechange">
          <RuleChangeSubmission />
        </Route>
        <Route path="/motionsubmission">
          <MotionSubmission/>
        </Route>
        <Route path="/inbox">
          <Inbox/>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
