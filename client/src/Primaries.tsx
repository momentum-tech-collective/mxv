import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Landing from './pages/Landing';
import PrimaryPage from './pages/Primary';
import PrimaryResultsPage from './pages/PrimaryResults';
import { PrimaryContext, PrimaryDetail } from './components/Context';

import './App.css';

const jsonOptions = {
  method: 'get',
  headers: {
    'Content-Type': 'application/json'
  }
}


function Primaries() {
  const [loaded, setLoaded] = useState(false)
  const [primaries, setPrimaries] = useState<PrimaryDetail[]>([])
  const [completed, setCompleted] = useState<PrimaryDetail[]>([])
  const [cast, setCast] = useState<PrimaryDetail[]>([])

  const doFetch = async () => {
    try {
      // TODO these in parallel
      const pres = await fetch('/primaries/primaries/', jsonOptions)
      if (pres.status === 401) {
        window.location.href = "/members/login"
      }
      const pdata = await pres.json()
      setPrimaries(pdata)

      const completedres = await fetch('/primaries/completed/', jsonOptions)
      const completeddata = await completedres.json()
      setCompleted(completeddata)

      const castres = await fetch('/primaries/cast', jsonOptions)
      const castdata = await castres.json()
      setCast(castdata)

      setLoaded(true)
    } catch (error) {
      console.error(error)
      setPrimaries([])
    }
  }

  useEffect(() => {
    if (!loaded) {
      doFetch()
    }
  }, [loaded])

  return (
    <PrimaryContext.Provider value={{
      primaries: primaries,
      completed: completed,
      cast: cast,
      loaded: loaded,
      invalidateContext: () => setLoaded(false)
    }}>
      <Router basename="/app/primaries">
        <Switch>
          <Route exact path="/">
            <Landing />
          </Route>
          <Route 
             path="/vote/:id"
             render={(props) => {
              return(
                <PrimaryPage 
                  result_primary={primaries.filter((primary: PrimaryDetail) => primary.id === Number(props.match.params.id))[0]}
                />
              )
            }}
          />

          <Route path="/result/:id"
            render={(props) => { 
              return (
                <PrimaryResultsPage 
                  result_primary={completed.filter((primary: PrimaryDetail) => primary.id === Number(props.match.params.id))[0]}
                />
              )
            }}
          />
        </Switch>
      </Router>
    </PrimaryContext.Provider>
  );
}

export default Primaries;
