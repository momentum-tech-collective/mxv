#!/bin/bash

APP=mxv-staging

export PGPORT=5432
export PGHOST=localhost
export PGUSER=mxv
export PGPASSWORD=mxv

HEROKU=heroku 
TODAY=`date "+%m%d"`
DB="mxv_backup_${TODAY}"

$HEROKU pg:backups:capture --app $APP
$HEROKU pg:reset DATABASE_URL --app $APP --confirm $APP
$HEROKU pg:push $DB DATABASE_URL --app $APP
