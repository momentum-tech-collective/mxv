from members.models import *
from datetime import date
import logging
import os
import csv

downloads = f'{os.environ.get("HOME")}/Downloads'

def run(*args):
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%I:%M:%S')
    creations = []
    updates = []
    with open(f'{downloads}/tag-history.csv') as t:
        reader = csv.DictReader(t)
        for row in reader:
            try:
                first_used = date.fromisoformat(row["first_used"])
            except ValueError as e:
                logging.error(f'{row["name"]} first_used "{row["first_used"]}" invalid')
            try:
                last_used = date.fromisoformat(row["last_used"])
            except ValueError as e:
                logging.error(f'{row["name"]} last_used "{row["last_used"]}" invalid')
            if not NationBuilderTag.objects.filter(name=row["name"]).exists():
                tag = NationBuilderTag(name=row["name"])
                if first_used:
                    tag.first_used = first_used
                if last_used:
                    tag.last_used = last_used
                creations.append(tag)
            else:
                tag = NationBuilderTag.objects.get(name=row["name"])
                changed = False
                if first_used and first_used < tag.first_used:
                    tag.first_used = first_used
                    changed = True
                if last_used and last_used > tag.last_used:
                    tag.last_used = last_used
                    changed = True
                if changed:
                    updates.append(tag)
    NationBuilderTag.objects.bulk_create(creations, batch_size=100)
    NationBuilderTag.objects.bulk_update(updates, ['first_used', 'last_used'], batch_size=100)
    logging.error(f'{len(creations)} created, {len(updates)} updated')
