
from django.db import models, connection
from django.db.models import Q
from django.contrib.postgres.fields.citext import CIEmailField
from groups.models import *
from members.models import *
from territories.models import *
from datetime import datetime
from dateutil import parser
import os
import csv

downloads = f'{os.environ.get("HOME")}/Downloads'

def printgroup(group, message):
    print(f'{group.name} ({group.status}): {message}')

class NationBuilderCpUser(models.Model):
    nationbuilder_id = models.IntegerField(primary_key=True)
    first_name = models.CharField(max_length=127, null=True, blank=True)
    last_name = models.CharField(max_length=127, null=True, blank=True)
    email1 = CIEmailField(max_length=255, null=True, blank=True)
    email2 = CIEmailField(max_length=255, null=True, blank=True)
    email3 = CIEmailField(max_length=255, null=True, blank=True)
    email4 = CIEmailField(max_length=255, null=True, blank=True)
    class Meta:
        app_label = 'groups_populate'

class Dpa(models.Model):
    name = models.CharField(max_length=255)
    email = CIEmailField(max_length=255)
    class Meta:
        app_label = 'groups_populate'

def setup_temp_table(cursor):
    cursor.execute('DROP TABLE IF EXISTS groups_populate_nationbuildercpuser')
    cursor.execute('''
        CREATE TABLE groups_populate_nationbuildercpuser (
        nationbuilder_id INTEGER PRIMARY KEY NOT NULL,
        first_name VARCHAR(127) NULL,
        last_name VARCHAR(127) NULL,
        email1 CITEXT,
        email2 CITEXT,
        email3 CITEXT,
        email4 CITEXT
    );
    ''')
    with open(f'{downloads}/control-panel-users.csv') as cp:
        reader = csv.DictReader(cp)
        for row in reader:
            NationBuilderCpUser.objects.create(nationbuilder_id=row['nationbuilder_id'], first_name=row['first_name'], last_name=row['last_name'])

def setup_dpa_table(cursor):
    cursor.execute('DROP TABLE IF EXISTS groups_populate_dpa')
    cursor.execute('''
        CREATE TABLE groups_populate_dpa (
        id SERIAL PRIMARY KEY,
        name VARCHAR(255) NOT NULL,
        email CITEXT NOT NULL
    );
    ''')
    with open(f'{downloads}/DPAs-Tyrone.csv') as dpaf:
        reader = csv.DictReader(dpaf)
        for row in reader:
            Dpa.objects.create(name=row['Name'], email=row['email adress'])

def run(*args):
    Group.objects.all().delete()
    Territory.objects.filter(lookup_field='admin_district').all().delete()
    Territory.objects.filter(lookup_field='admin_ward').all().delete()
    Territory.objects.filter(lookup_field='parliamentary_constituency').all().delete()
    Territory.objects.filter(lookup_field='scottish_parliamentary_constituency').all().delete()
    Territory.objects.filter(lookup_field='parish').all().delete()
    wards1 = produce_uk_set('Ward names and codes UK as at 12_19.csv','WD19NM')
    wards2 = produce_uk_set('LAU2 names and codes UK as at 12_18.csv', 'LAU218NM')
    wards = wards1.union(wards2)
    constituencies = produce_uk_set('Westminster Parliamentary Constituency names and codes UK as at 12_14.csv', 'PCON14NM')
    councils = produce_uk_set('LA_UA names and codes UK as at 04_20.csv', 'LAD20NM')
    counties = produce_uk_set('County names and codes UK as at 04_20.csv', 'CTY20NM')
    parishes = produce_uk_set('Parish_NCP names and codes EW as at 12_19.csv', 'PARNCP19NM')
    scotland = produce_set('Scottish_Parliamentary_Constituencies_December_2019_Names_and_Codes_in_Scotland.csv', 'name')
    with connection.cursor() as cursor:
        setup_temp_table(cursor)
        setup_dpa_table(cursor)
        with open(f'{downloads}/MomentumGroupsAirtable.csv') as airtable:
            reader = csv.DictReader(airtable)
            for row in reader:
                group = Group(
                    name=row['Closest Momentum Group'],
                    status=GroupStatusType(row['STATUS']).name,
                    notes=row['CAUTION Groups Team Notes'],
                    group_email=row['Group Email'],
                    facebook_url=row['Facebook URL'],
                    twitter_handle=row['Twitter Handle'],
                    website_url=row['Website URL'],
                )
                last_updated=row['Details Last Updated'].strip()
                if last_updated:
                    group.last_updated = parser.parse(last_updated)
                populate_dpa(row, group)
                group.save()
                populate_key_contacts(row, group)
                populate_social_media_managers(row, group)
                populate_territories(row, group, wards, constituencies, councils, counties, parishes, scotland)
        cursor.execute('DROP TABLE groups_populate_nationbuildercpuser')
        cursor.execute('DROP TABLE groups_populate_dpa')


def produce_uk_set(filename, column):
    return produce_set(f'ONSPD_AUG_2020_UK/Documents/{filename}', column)

def produce_set(filename, column):
    data = set()
    with open(f'{downloads}/{filename}') as f:
        reader = csv.DictReader(f)
        for row in reader:
            data.add(row[column])
    return data

def populate_dpa(row, group):
    dpa = row['DPA (Data Manager)'].strip()
    if not dpa:
        return
    possible_dpas = Member.objects.filter(name__iexact=dpa)
    if possible_dpas.count() == 1:
        group.dpa = Member.objects.get(name__iexact=dpa)
    elif possible_dpas.count() > 1:
        for cand_dpa in possible_dpas:
            cp_matches = NationBuilderCpUser.objects.filter( \
                Q(first_name__iexact=cand_dpa.first_name, last_name__iexact=cand_dpa.last_name) |\
                Q(email1__iexact=cand_dpa.email) |\
                Q(email2__iexact=cand_dpa.email) |\
                Q(email3__iexact=cand_dpa.email) |\
                Q(email4__iexact=cand_dpa.email))
            if cp_matches.count() == 1:
                group.dpa = cand_dpa
                printgroup(group, f'multiple DPAs; {cand_dpa.full_name} was the only CP user')
                return
            if Dpa.objects.filter(email=cand_dpa.email).exists():
                group.dpa = cand_dpa
                return
            printgroup(group, f'{cand_dpa.first_name} {cand_dpa.last_name} not found in CP')
        printgroup(group, f'multiple DPAs; none had 1 match in CP')
    else:
        if ',' not in dpa:
            if maybe_get_dpa_from_file(group, dpa):
                return
            printgroup(group, f'DPA {dpa} had 0 matches')
        else:
            dpas = dpa.split(',')
            for cand_dpa in dpas:
                cand = cand_dpa.strip()
                if Member.objects.filter(name__iexact=cand).count() == 1:
                    group.dpa = Member.objects.get(name__iexact=cand)
                    printgroup(group, f'multiple DPAs; picking {cand}')
                    return
                if maybe_get_dpa_from_file(group, cand):
                    return
            printgroup(group, f'none of DPAs {dpas} found')


def maybe_get_dpa_from_file(group, name):
    if Dpa.objects.filter(name__iexact=name).count() == 1:
        email = Dpa.objects.get(name__iexact=name)
        if Member.objects.filter(email=email).exists():
            group.dpa = Member.objects.get(email=email)
            return True
    return False

def populate_key_contacts(row, group):
    key_contacts = multi_member_field(row, 'Key Contacts', group)
    for kc in key_contacts:
        group.key_contacts.add(kc)

def populate_social_media_managers(row, group):
    smm = multi_member_field(row, 'Social Media Managers', group)
    for mm in smm:
        group.social_media_managers.add(mm)

def multi_member_field(row, field_name, group):
    cell_str = row[field_name].strip()
    members = []
    if not cell_str:
        return members
    output_text = field_name.lower()[:-1]
    vals = cell_str.split(',')
    for val in vals:
        name = val.strip()
        vfilter = Member.objects.filter(name__iexact=name)
        if vfilter.count() == 1:
            members.append(Member.objects.get(name__iexact=name))
        else:
            printgroup(group, f'{output_text} {name} had {vfilter.count()} matches')
    return members

def populate_territories(row, group, wards, constituencies, councils, counties, parishes, scotland):
    territory_str = row['Data Area Assigned']
    if not territory_str:
        return
    if 'AND' in territory_str or 'NOT' in territory_str or 'Exclude' in territory_str:
        printgroup(group, f'manual handling needed for territory {territory_str}')
        return
    if ';' not in territory_str and ',' not in territory_str:
        add_territory(group, territory_str, wards, constituencies, councils, counties, parishes, scotland)
        return
    if ';' in territory_str:
        territories = territory_str.split(';')
    else:
        territories = territory_str.split(',')
    for territory in territories:
        add_territory(group, territory, wards, constituencies, councils, counties, parishes, scotland)
    if group.territories.count() == 0:
        printgroup(group, f'manual handling needed for territory {territory_str}')


def add_territory(group, tname, wards, constituencies, councils, counties, parishes, scotland):
    if tname == '' or tname.isspace():
        return
    name = tname.strip().replace('&', 'and')
    if 'CLP' in name:
        name = name.replace('CLP', '').strip()
        if 'Scotland' in group.name or group.status == GroupStatusType.ScotlandVerified.name:
            lookup_field = 'scottish_parliamentary_constituency'
            if not name in scotland:
                printgroup(group, f'Unrecognized Scottish constituency "{name}"')
                return
        else:
            lookup_field = 'parliamentary_constituency'
            if not name in constituencies:
                printgroup(group, f'Unrecognized constituency "{name}"')
                return

    elif 'County' in name:
        lookup_field = 'admin_county'
        name = name.replace('Council', '').replace('County', '').strip()
        if not name in counties:
            printgroup(group, f'Unrecognized county "{name}"')
            return
    elif 'Ward' in name:
        lookup_field = 'admin_ward'
        name = name.replace('Ward', '').strip()
        if not name in wards:
            printgroup(group, f'Unrecognized ward "{name}"')
            return
    elif 'Town' in name or 'Parish' in name:
        lookup_field = 'parish'
        name = name.replace('Council', '').replace('Town', '').replace('Parish', '').strip()
        if not name in parishes:
            printgroup(group, f'Unrecognized parish "{name}"')
    else:
        name = name.replace('Council', '').replace('City', '').replace('Borough', '').replace('District', '').strip()
        lookup_field = 'admin_district'
        if not name in councils:
            if name in parishes:
                lookup_field = 'parish'
            else:
                printgroup(group, f'Unrecognized council "{name}"')
                return
    territory, created = Territory.objects.get_or_create(name=name, lookup_field=lookup_field)
    if not created:
        printgroup(group, f'{territory} already existed')
    group.territories.add(territory)
