# Scripts

## Counting STV primaries

`python manage.py runscript scripts/rankedvote_count --script-args 17 8` ― This script counts a ranked vote primary. It was written for the 2021 Policy Primary, which was a ranked choice vote to decide 8 policies that Momentum would endeavour to take to Labour Party conference. It produces a [.blt file](https://yingtongli.me/blog/2020/12/24/pyrcv2.html#blt-file-format), a relatively standard format in the digital counting of ranked votes. The resulting file was then uploaded to [PyRCV](https://yingtongli.me/rcv/) and counted there.