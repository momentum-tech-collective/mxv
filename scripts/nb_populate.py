"""
Given a CSV of NationBuilder people (presumably exported from the NB UI, but
dunno for sure) add a bunch of records to our NationBuilderPerson table in mxv.

## History and speculation

Around January 26th 2021 Tyrone and Ed were trying to mirror NB data into MxV.
They used two approaches, this script for adding people in bulk from CSVs and
the webhook listeners in members/views.py.

Later Ed and Tyrone found that NB deprecated the webhooks, tho we do still get messages on them (you can see them in Sentry).

Anyway. I don't know why we even want to import from NB to MxV. Probably
something to find out eventually.

We've been importing data from join-page since the beginning of this project, so not sure why we didn't just use the NB IDs in that? Maybe suspicion about duplicate IDs, etc.

And the NB data is never sync'd back to join-page, so it won't know about merges or whatever that might make the IDs stale.
"""

from django.core.exceptions import ValidationError
from django.db.models import Q
from datetime import datetime
from members.models import Member, NationBuilderPerson, NationBuilderTag, present, dateutil, create_tags, create_taggings, assign_nontag_fields
import os
import csv
import cProfile
import pstats

from_id = 0
from_email = 0
from_other = 0
not_found = 0
deleted = 0
bulk = 0
not_bulk = 0
tag_cache = {}
tags_to_update = {}
nb_cache = {}
fields = ['email', 'unique_token', 'phone_number', 'mobile_number',
'email1', 'email2', 'email3', 'email4', 'email_opt_in', 'mobile_opt_in',
'do_not_call', 'do_not_contact', 'primary_address1', 'primary_address2',
'primary_address3', 'primary_city', 'primary_postcode', 'primary_country',
'membership_number', 'join_date', 'membership_payment_lapsed_date',
'membership_status', 'membership_payment_amount',
'membership_payment_cycle', 'membership_payment_processor', 'gocardless_id',
'paypal_id', 'messagebird_id']

# For this script to work, the fields in the input file have to be renamed as follows:
# nationbuilder_id -> id
# mobile_number -> mobile
# phone_number -> phone
# tag_list -> tags

def log(text):
    print(f'{datetime.now().strftime("%H:%M:%S")} {text}')

def run(*args):
    if len(args) < 1:
        print('Must specify filename with --script-args')
        return
    profile = cProfile.Profile()
    profile.runcall(main, *args)
    stats = pstats.Stats(profile)
    stats.strip_dirs()
    stats.sort_stats('cumtime')
    stats.print_stats('.*(nb_populate|models).*')

def main(*args):
    affiliate = 0
    affiliate_error = 0
    count = 0
    log('Building caches')
    build_tag_cache()
    build_nb_cache()
    updates = []
    with open(args[0]) as f:
        reader = csv.DictReader(f)
        log('Starting records')
        for row in reader:
            nb_person = treat_row(row)
            if nb_person:
                try:
                    nb_person.full_clean()
                    if nb_person.membership_status == 'affiliate':
                        affiliate += 1
                        nb_person = add_affiliate_member(nb_person)
                    updates.append(nb_person)
                except ValidationError as e:
                    log(f'{nb_person.nation_builder_id} had error {e}')
                    if nb_person.membership_status == 'affiliate':
                        affiliate_error += 1
            count += 1
            if count % 1000 == 0:
                log(f'{count} rows completed')
            if count % 2000 == 0:
                log(f"UPDATING: {len(updates)} records updating")
                bulk_nb_updates(updates)
                updates = []
    bulk_nb_updates(updates)
    bulk_tag_updates()
    log(f'{count} total: {from_id} from id, {from_email} from email, {from_other} from other, {not_found} created, {deleted} deleted')
    log(f'{bulk} bulked, {not_bulk} not bulk')
    log(f'{affiliate} affiliates found, and {affiliate_error} were faulty')

def build_tag_cache():
    global tag_cache
    for tag in NationBuilderTag.objects.all():
        tag_cache[tag.name] = tag

def build_nb_cache():
    global nb_cache
    nbs = NationBuilderPerson.objects.all()
    for nb in nbs:
        write_cache(nb, nb.nation_builder_id)
        write_cache(nb, nb.email1)
        write_cache(nb, nb.email2)
        write_cache(nb, nb.email3)
        write_cache(nb, nb.email4)


def write_cache(nb, field_val):
    global nb_cache
    if field_val:
        if field_val in nb_cache:
            log(f'{field_val} in db twice! {nb.id} and {nb_cache[field_val].id}')
        else:
            nb_cache[field_val] = nb

def bulk_nb_updates(updates):
    global fields
    NationBuilderPerson.objects.bulk_update(updates, fields)

def bulk_tag_updates():
    global tags_to_update
    NationBuilderTag.objects.bulk_update(tags_to_update.values(), ["last_used"])

def treat_row(row):
    global not_found, bulk, not_bulk
    nb_person = find_nb_person(row)
    if not nb_person:
        nb_person = NationBuilderPerson.objects.create(nation_builder_id=int(row["id"]), email=row["email"])
        not_found = not_found + 1
    assign_nontag_fields(nb_person, row)
    if cached_update_tags(nb_person, row):
        nb_person.save()
        not_bulk += 1
        return None
    bulk += 1
    return nb_person

def find_nb_person(row):
    global from_id, from_email, from_other
    if present(row, "id"):
        nb_id = int(row["id"])
        if nb_id in nb_cache:
            nb_person = nb_cache[nb_id]
            from_id += 1
            delete_others_with_same_email(row, nb_person)
            return nb_person
    if present(row, "email"):
        if row["email"] in nb_cache:
            from_email += 1
    return None

def add_affiliate_member(nb_person: NationBuilderPerson):
    if Member.objects.filter(email=nb_person.email).exists() == False:
        email = nb_person.email
        name = f'{nb_person.first_name} {nb_person.last_name}'
        member = Member.objects.create_member(email=email, name=name)
        member.save()
        nb_person.member = member
    return nb_person

def delete_others_with_same_email(row, nb_person):
    global nb_cache, deleted
    if not present(row, "email"):
        return
    if not present(nb_cache, row["email"]):
        return
    other = nb_cache[row["email"]]
    if other.nation_builder_id == int(row["id"]):
        return
    if other.member:
        member = other.member
        other.member = None
        nb_person.member = member
        nb_person.member.save()
    other.delete()
    deleted += 1


def cached_update_tags(nb_person, nb_record):
    global tag_cache, tags_to_update
    """Same as members.models.update_tags, but using an in-memory cache instead of the DB"""
    if not present(nb_record, 'tags'):
        return False
    tag_date = dateutil.parser.parse(nb_record['updated_at']).date()
    if isinstance(nb_record['tags'], list):
        tags = nb_record['tags'] # an NB webhook
    else:
        tags = [tag.strip() for tag in nb_record['tags'].split(',')] # backfill script
    tags.sort()
    create_names = set()
    updates = set()
    for tag in tags:
        if not tag in tag_cache:
            create_names.add(tag)
        else:
            dbTag = tags_to_update[tag] if tag in tags_to_update else tag_cache[tag]
            if dbTag.last_used < tag_date:
                dbTag.last_used = tag_date
                tags_to_update[tag] = dbTag
                updates.add(dbTag)
    taggings = create_tags(tag_date, create_names)
    for new_tag in taggings:
        tag_cache[new_tag.name] = new_tag
    taggings.extend(list(updates))
    create_taggings(nb_person, tag_date, taggings)
    return len(taggings) > 0
