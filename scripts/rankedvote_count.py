from primaries.models import RankedVote, Candidate, Primary
import re

incorrect_vote_id = 0
multiple_candidates_match_id = 0


def create_candidate_list(primary_id):

    candidates = Candidate.objects.all().filter(primary=primary_id)

    result = []
    blt_id = 1

    for candidate in candidates:

        candidate_id = candidate.pk
        candidate_name = candidate.first_name + candidate.last_name
        candidate_name = re.sub(r'[^\w]', '', candidate_name).upper()[0:19]

        candidate = {
            "blt_id": blt_id,
            "candidate_id": candidate_id,
            "candidate_name": candidate_name
        }

        result.append(candidate)
        blt_id += 1

    return result


def make_blt_line(ballot):
    ballot = tuple(sorted(ballot, key=lambda vote: vote['rank']))
    raw_line = str(tuple(map(lambda vote: vote['id'], ballot)))

    charsToRemove = ["(", ",", ")"]
    for char in charsToRemove:
        raw_line = raw_line.replace(char, "")

    return "1 " + raw_line


def print_ballots(candidate_list, primary_id):
    global incorrect_vote_id
    global multiple_candidates_match_id
    raw_ballots = list(RankedVote.objects.all().filter(
        primary=primary_id).values_list('ballot'))

    candidate_ids = list(map(lambda c: c['candidate_id'], candidate_list))

    for ballot in raw_ballots:

        ballot = ballot[0]

        for vote in ballot:

            if vote['id'] not in candidate_ids:
                print(vote)
                incorrect_vote_id += 1
                continue

            candidate_id = vote['id']

            filtered_candidates_by_id = list(filter(
                lambda candidate: candidate['candidate_id'] == candidate_id,
                candidate_list
            ))

            if len(filtered_candidates_by_id) != 1:
                multiple_candidates_match_id += 1
                continue

            candidate = filtered_candidates_by_id[0]

            vote['id'] = candidate['blt_id']

        blt_line = make_blt_line(ballot)
        print(blt_line)
    print(0)


def print_candidates(candidate_list, primary_id):

    primary = Primary.objects.filter(pk=primary_id).first()
    primary_name = f'"{primary.name}"'

    candidate_tuple = tuple(
        sorted(candidate_list, key=lambda cand: cand['blt_id']))

    for candidate in candidate_tuple:
        name = candidate['candidate_name']
        name = f'"{name}"'
        print(name)

    print(primary_name)


def run(*args):
    global incorrect_vote_id
    global multiple_candidates_match_id

    if len(args) < 1:
        print('Must specify primary_id and amount of places with --script-args')
        return
    elif len(args) == 1:
        print("Must specify amount of places")
        return

    primary_id = args[0]
    places = args[1]

    candidate_list = create_candidate_list(primary_id)
    candidate_amt = len(candidate_list)

    print(candidate_amt, places)
    print_ballots(candidate_list, primary_id)
    print_candidates(candidate_list, primary_id)

    if (multiple_candidates_match_id + incorrect_vote_id) > 0:
        print("spoilt ballots:")
        print("multiple_candidates_match_id:", multiple_candidates_match_id)
        print("incorrect_vote_id:", incorrect_vote_id)
