from territories.models import *
from datetime import date
import csv
import logging
import os


downloads = f'{os.environ.get("HOME")}/Downloads'


def run(*args):
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(message)s', datefmt='%I:%M:%S')

    with open(f'{downloads}/regionsAddedCty.csv') as t:

        updates = []
        creates = []

        reader = csv.DictReader(t)
        for row in reader:

            csvName = row['name']
            csvLookup = row['lookup_field']
            regionTerritory = Territory.objects.get(
                name=row['region'], lookup_field="european_electoral_region")

            isInDb = Territory.objects.filter(
                name=csvName, lookup_field=csvLookup).exists()

            if isInDb:
                existingTerritory = Territory.objects.get(
                    name=csvName, lookup_field=csvLookup)
                existingTerritory.region = regionTerritory
                updates.append(existingTerritory)

            else:
                creates.append(
                    Territory(name=csvName, lookup_field=csvLookup, region=regionTerritory))

    Territory.objects.bulk_update(updates, ['region'], batch_size=100)
    Territory.objects.bulk_create(creates)
