# returns the default template context
from django.conf import settings

def default(request):
    return { 
        'site_name_short': settings.SITE_NAME_SHORT,
        'site_name_long': settings.SITE_NAME_LONG,
        'join_url': settings.JOIN_URL,
        'profiles_visible_to_non_staff': settings.PROFILES_VISIBLE_TO_NON_STAFF
    }
