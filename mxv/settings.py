"""
Django settings for mxv project on Heroku. For more info, see:
https://github.com/heroku/heroku-django-template

For more information on this file, see
https://docs.djangoproject.com/en/3.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.1/ref/settings/
"""

import os
import dj_database_url
from django.contrib import messages
from datetime import date
import cloudinary
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from dotenv import load_dotenv
import json

def bool_env(name):
    return True if os.environ.get(name, 'False').upper() == 'TRUE' else False

load_dotenv()

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('MXV_SECRET_KEY', '&0+9q8c$q46+bslj=g#!i9@u#j@3#p=#k12je47wj%fj24q%=*')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True if os.environ.get('MXV_DEBUG', 'True') == 'True' else False

# Application definition

INSTALLED_APPS = [
    # apps
    'mxv.apps.MxvConfig',
    'members.apps.MembersConfig',
    'review.apps.ReviewConfig',
    'voting_intentions.apps.VotingIntentionsConfig',    
    'consultations.apps.ConsultationsConfig',
    'tasks.apps.TasksConfig',
    'groups.apps.GroupsConfig',
    'primaries.apps.PrimariesConfig',
    'questions.apps.QuestionsConfig',
    'territories.apps.TerritoriesConfig',
    'lobby.apps.LobbyConfig',
    'petitions.apps.PetitionsConfig',
    'livereload',
    # admin, Django and third party
    'nested_admin',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'rest_framework',
    'corsheaders',
    # Disable Django's own staticfiles handling in favour of WhiteNoise, for
    # greater consistency between gunicorn and `./manage.py runserver`. See:
    # http://whitenoise.evans.io/en/stable/django.html#using-whitenoise-in-development
    'whitenoise.runserver_nostatic',
    'django.contrib.staticfiles',
    'solo.apps.SoloAppConfig',
    'tinymce',
    'widget_tweaks',
    'django_rq',
    'polymorphic',
    'cloudinary',
    'django_countries',
    'rangefilter',
    'django_extensions',
    'martor', # markdown editor
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'csp.middleware.CSPMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'corsheaders.middleware.CorsPostCsrfMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

if DEBUG:
    MIDDLEWARE.append('livereload.middleware.LiveReloadScript')

ROOT_URLCONF = 'mxv.urls'
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            BASE_DIR + '/mxv/templates/',
            BASE_DIR + '/client/',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'mxv.context_processors.default',
            ],
            'debug': DEBUG,
        },
    },
]

WSGI_APPLICATION = 'mxv.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': os.environ.get('MXV_DATABASE_HOST', ''),
        'PORT': os.environ.get('MXV_DATABASE_PORT', ''),
        'NAME': os.environ.get('MXV_DATABASE_NAME', 'mxv'),
        'USER': os.environ.get('MXV_DATABASE_USER', 'mxv'),
        'PASSWORD': os.environ.get('MXV_DATABASE_PASSWORD', 'mxv')
    },
    'live': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': os.environ.get('MXV_LIVE_DATABASE_HOST', ''),
        'PORT': os.environ.get('MXV_LIVE_DATABASE_PORT', ''),
        'NAME': os.environ.get('MXV_LIVE_DATABASE_NAME', 'mxv'),
        'USER': os.environ.get('MXV_LIVE_DATABASE_USER', 'mxv'),
        'PASSWORD': os.environ.get('MXV_LIVE_DATABASE_PASSWORD', 'mxv')
    },
}
# Change database configurations with $DATABASE_URL
DATABASES['default'].update(dj_database_url.config(conn_max_age=500))

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.SessionAuthentication',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ]
}

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Europe/London'
USE_I18N = True
USE_L10N = False
USE_TZ = True

# Settings for SecurityMiddleware
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_HSTS_PRELOAD = not DEBUG
SECURE_HSTS_SECONDS = 15768000
# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_SSL_REDIRECT = not DEBUG

# CSRF cookie settings
CSRF_COOKIE_AGE=None
CSRF_COOKIE_SAMESITE='Strict'
CSRF_COOKIE_SECURE=True

# Content-Security-Policy header, set via django-csp middleware
# Note that self has to be single-quoted inside double-quotes
CSP_DEFAULT_SRC=('none',)
CSP_FRAME_SRC=("'self'", "https:",)
CSP_IMG_SRC=("'self'","https:","data:",'www.googletagmanager.com')
CSP_FONT_SRC=("'self'","https:",)
CSP_MANIFEST_SRC=("'self'",)
CSP_SCRIPT_SRC=["'self'","'unsafe-inline'", "https:",] # TODO get rid of unsafe-inline
CSP_STYLE_SRC=("'self'","'unsafe-inline'", "https:",)
CSP_FORM_ACTION=("'self'", "https://*.gocardless.com")
CSP_CONNECT_SRC=["'self'", "https://plausible.io/api/"]
if DEBUG:
    CSP_SCRIPT_SRC += ["127.0.0.1:35729"]
    CSP_CONNECT_SRC += ["ws://127.0.0.1:35729"]

# Allow all host headers
ALLOWED_HOSTS = ['*']

# should be an array of http addresses of Momentum sites that can post requests
CORS_ALLOWED_ORIGINS = json.loads(os.environ.get("CORS_ALLOWED_ORIGINS"))
CSRF_TRUSTED_ORIGINS = [
    site.split('/')[-1]
    for site in CORS_ALLOWED_ORIGINS
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_ROOT = os.path.join(PROJECT_ROOT, 'staticfiles')
STATIC_URL = '/static/'

# Extra places for collectstatic to find static files.
STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, 'static'),
    ('client', os.path.join(BASE_DIR, 'client/build')),
]

# Simplified static file serving.
# https://warehouse.python.org/project/whitenoise/
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

# extend the base user model
AUTH_USER_MODEL = 'members.Member'

# get the secret used by the join page to create inactive members
CREATE_INACTIVE_MEMBER_SECRET = os.environ.get('MXV_CREATE_INACTIVE_MEMBER_SECRET', 'mxv')

# get the secret used by the GDPR tool to delete members
GDPR_TOOL_SECRET = os.environ.get('MXV_GDPR_TOOL_SECRET', 'mxv')

# set up HTML editor
TINYMCE_DEFAULT_CONFIG = {
    'theme': "silver",
    'width' : 600,
    'height' : 300,
    'convert_urls' : False,
    'relative_urls' : False,
    'plugins': 'code link',
    'toolbar': 'link unlink styleselect bold italic bullist numlist outdent indent code',
}

# Postmark
EMAIL_BACKEND = 'postmarker.django.EmailBackend'
POSTMARK = {
    'TOKEN': os.environ.get('POSTMARK_SERVER_TOKEN', "mxv"),
    'TEST_MODE': False,
    'VERBOSITY': 0,
}
DEFAULT_FROM_EMAIL = "Team Momentum <mymomentum@peoplesmomentum.com>"
CONFERENCE_MOTIONS_EMAIL = os.environ.get('CONFERENCE_MOTIONS_EMAIL')
CONFERENCE_RULE_CHANGES_EMAIL = os.environ.get('CONFERENCE_RULE_CHANGES_EMAIL')
MAILGUN_API_KEY = os.environ.get('MAILGUN_API_KEY')
MAILGUN_DOMAIN = os.environ.get('MAILGUN_DOMAIN')

# join page
JOIN_URL = "https://join.peoplesmomentum.com"

# site name
SITE_NAME_SHORT = "My Momentum"
SITE_NAME_LONG = "My Momentum"

# send members to the index page after they login
LOGIN_URL = '/members/login'
LOGIN_REDIRECT_URL = '/'

# send members back to the login page after they logout

LOGOUT_REDIRECT_URL = '/members/login'

# date/time formats
DATE_FORMAT = 'd/m/Y'
DATETIME_FORMAT = 'd/m/Y H:i:s'

# close the session when the browser closes
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

# whether to allow requests to the error URL (for testing error handling)
ALLOW_ERROR_URL = True if os.environ.get('MXV_ALLOW_ERROR_URL', 'False') == 'True' else False

# change error into danger for bootstrap
MESSAGE_TAGS = {
    messages.ERROR: 'danger'
}

# whether to show track voting-specific changes to just staff or anyone
TRACK3_VOTING_VISIBLE_TO_NON_STAFF = True if os.environ.get('MXV_TRACK3_VOTING_VISIBLE_TO_NON_STAFF', 'False') == 'True' else False

# when the site was launched to the members
LAUNCH_DATE = date(2018, 2, 2)

# token for accessing NationBuilder
NATIONBUILDER_API_TOKEN = os.environ.get('MXV_NATIONBUILDER_API_TOKEN', '')
# NationBuilder prefix
NATIONBUILDER_API_PREFIX = os.environ.get('MXV_NATIONBUILDER_API_PREFIX', 'momentum')

# default redirect page URL
DEFAULT_REDIRECT_PAGE_URL = 'https://peoplesmomentum.com'
TRUSTED_REDIRECTS = json.loads(os.environ.get('TRUSTED_REDIRECTS'))

# whether to show consultations to just staff or anyone
CONSULTATIONS_VISIBLE_TO_NON_STAFF = True if os.environ.get('MXV_CONSULTATIONS_VISIBLE_TO_NON_STAFF', 'False') == 'True' else False

# whether to show membership cards on the index or not

MEMBERSHIP_CARD_VISIBLE_TO_NON_STAFF = True if os.environ.get('MXV_MEMBERSHIP_CARD_VISIBLE_TO_NON_STAFF', 'False') == 'True' else False

# task queueing
RQ_SHOW_ADMIN_LINK = True
RQ_QUEUES = {
    'default': {
        'URL': os.getenv('REDISTOGO_URL', 'redis://localhost:6379/0'),
    }
}

# configure sentry
sentry_sdk.init(
    dsn=os.getenv('SENTRY_DSN', ''),
    integrations=[DjangoIntegration()]
)

# whether to show profile pages to just staff or anyone
PROFILES_VISIBLE_TO_NON_STAFF = True if os.environ.get('MXV_PROFILES_VISIBLE_TO_NON_STAFF', 'False') == 'True' else False

UPDATE_SUBS_VISIBLE_TO_NON_STAFF = True if os.environ.get('MXV_UPDATE_SUBS_VISIBLE_TO_NON_STAFF', 'False') == 'True' else False

# URL for drop-in update subs page replacement
UPDATE_SUBS_URL = os.environ.get('MXV_UPDATE_SUBS_URL', '')

# URL for MoApp member lookup endpoint
MEMBER_LOOKUP_URL = os.environ.get('MXV_MEMBER_LOOKUP_URL', '')

# get the secret used by the NationBuilder web hooks
WEB_HOOK_SECRET = os.environ.get('MXV_WEB_HOOK_SECRET', 'mxv')

# NCG voting
QUESTIONS_VISIBLE_TO_NON_STAFF = bool_env('MXV_QUESTIONS_VISIBLE_TO_NON_STAFF')
NCG_VOTING_URL = os.environ.get('MXV_NCG_VOTING_URL', '')

# Show primaries 
PRIMARIES_VISIBLE = bool_env('MXV_PRIMARIES_VISIBLE')

POLICY_PRIMARY_SUBMISSIONS_VISIBLE = bool_env('MXV_POLICY_PRIMARY_SUBMISSIONS_VISIBLE')

# django-countries settings 
COUNTRIES_FIRST=['UK']

cloudinary.config( 
  cloud_name = os.environ.get("CLOUDINARY_NAME"), 
  api_key = os.environ.get("CLOUDINARY_API_KEY"), 
  api_secret = os.environ.get("CLOUDINARY_API_SECRET")
)

# elobby addresses
ELOBBY_EMAIL_TO = json.loads(os.environ.get('ELOBBY_EMAIL_TO'))
ELOBBY_EMAIL_CC = json.loads(os.environ.get('ELOBBY_EMAIL_CC'))
ELOBBY_EMAIL_BCC = json.loads(os.environ.get('ELOBBY_EMAIL_BCC'))
ELOBBY_EMAIL_SUBJECT = os.environ.get('ELOBBY_EMAIL_SUBJECT')
ELOBBY_EMAIL_TAG = os.environ.get('ELOBBY_EMAIL_TAG')
ELOBBY_SEND_MP_EMAIL = bool_env('ELOBBY_SEND_MP_EMAIL')

# new member emails
NEW_MEMBER_EMAIL = bool_env('NEW_MEMBER_EMAIL')

# show member inboxes
INBOX_VISIBLE = bool_env('MXV_INBOX_VISIBLE')

# Choices are: "semantic", "bootstrap"
MARTOR_THEME = 'bootstrap'

# Global martor settings
# Input: string boolean, `true/false`
MARTOR_ENABLE_CONFIGS = {
    'emoji': 'true',        # to enable/disable emoji icons.
    'imgur': 'false',        # to enable/disable imgur/custom uploader.
    'mention': 'false',     # to enable/disable mention
    # to include/revoke jquery (require for admin default django)
    'jquery': 'true',
    'living': 'false',      # to enable/disable live updates in preview
    'spellcheck': 'false',  # to enable/disable spellcheck in form textareas
    'hljs': 'true',         # to enable/disable hljs highlighting in preview
}

# To show the toolbar buttons
MARTOR_TOOLBAR_BUTTONS = [
    'bold', 'italic', 'horizontal', 'heading', 'pre-code',
    'blockquote', 'unordered-list', 'ordered-list',
    'link', 'image-link', 'emoji',
    'direct-mention', 'toggle-maximize', 'help'
]

# To setup the martor editor with title label or not (default is False)
MARTOR_ENABLE_LABEL = False

# Markdownify
MARTOR_MARKDOWNIFY_FUNCTION = 'martor.utils.markdownify'  # default
MARTOR_MARKDOWNIFY_URL = '/martor/markdownify/'  # default

# Markdown extensions (default)
MARTOR_MARKDOWN_EXTENSIONS = [
    'markdown.extensions.extra',
    'markdown.extensions.nl2br',
    'markdown.extensions.smarty',
    'markdown.extensions.fenced_code',

    # Custom markdown extensions.
    'martor.extensions.urlize',
    'martor.extensions.del_ins',      # ~~strikethrough~~ and ++underscores++
    'martor.extensions.mention',      # to parse markdown mention
    'martor.extensions.emoji',        # to parse markdown emoji
    'martor.extensions.mdx_video',    # to parse embed/iframe video
    'martor.extensions.escape_html',  # to handle the XSS vulnerabilities
]

# Markdown Extensions Configs
MARTOR_MARKDOWN_EXTENSION_CONFIGS = {}

# Markdown urls
MARTOR_UPLOAD_URL = '/martor/uploader/'  # default
MARTOR_SEARCH_USERS_URL = '/martor/search-user/'  # default

# Markdown Extensions
# MARTOR_MARKDOWN_BASE_EMOJI_URL = 'https://www.webfx.com/tools/emoji-cheat-sheet/graphics/emojis/'     # from webfx
# default from github
MARTOR_MARKDOWN_BASE_EMOJI_URL = 'https://github.githubassets.com/images/icons/emoji/'
# please change this to your domain
MARTOR_MARKDOWN_BASE_MENTION_URL = 'https://my.peoplesmomentum.com'

# If you need to use your own themed "bootstrap" or "semantic ui" dependency
# replace the values with the file in your static files dir
# MARTOR_ALTERNATIVE_JS_FILE_THEME = "semantic-themed/semantic.min.js"   # default None
# MARTOR_ALTERNATIVE_CSS_FILE_THEME = "semantic-themed/semantic.min.css"  # default None
# MARTOR_ALTERNATIVE_JQUERY_JS_FILE = "jquery/dist/jquery.min.js"        # default None
