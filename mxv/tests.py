from django import test
from mxv import views, test_common, utils
from groups.models import Group, GroupStatusType
from members.models import Member
from unittest.mock import patch
import json

class TestMxv(test.TestCase):
    def setUp(self):
        factory = test.RequestFactory()
        self.request = factory.get('/')
        self.request.user = test_common.create_test_member('leo@panitch.com', 'Leo', 'Panitch')

    def tearDown(self):
        Group.objects.all().delete()
        Member.objects.all().delete()

    def test_member_dpa(self):
        group = Group.objects.create(dpa=self.request.user, name='Scotland the Brave', status=GroupStatusType.ScotlandVerified)
        self.assertTrue(views.member_is_dpa(self.request))

    def test_member_nondpa(self):
        self.assertFalse(views.member_is_dpa(self.request))

    def test_member_dpa_nonverified(self):
        group = Group.objects.create(dpa=self.request.user, name='Scotland the Brave', status=GroupStatusType.Legacy)
        self.assertFalse(views.member_is_dpa(self.request))

    def test_member_dpa(self):
        group = Group.objects.create(dpa=self.request.user, name='Wales the Brave', status=GroupStatusType.WalesVerified)
        self.assertTrue(views.member_is_dpa(self.request))

class TestMailGun(test.TestCase):
    @patch('mxv.utils.requests.post')
    def test_send_email(self, mock_post):
        mock_post.return_value.status_code = 200
        utils.send_mailgun_email(
            'ha@ha.com',
            'Mike',
            {'Keir Hardie': 'keir.hardie@hotmail.com',
             'Keir Starmer': 'keir.starmer@hotmail.com',
            },
            {'Ed Broadbent': 'ed.broadbent@canada.com',
             'Audrey McLaughlin': 'audrey.mclaughlin@canada.com'
            },
            'sender@peoplesmomentum.com',
            'replyTo@peoplesmomentum.com',
            'This is my subject',
            'Dear %recipient.salutation%, I rock!',
            '<p>Dear %recipient.salutation%<br/>, I rock</p>'
        )
        mock_post.assert_called_once()
        self.assertEqual(len(mock_post.call_args[0]), 1)
        self.assertIsNotNone(mock_post.call_args[1]["auth"])
        maildata = mock_post.call_args[1]["data"]
        self.assertEqual(maildata["from"], "sender@peoplesmomentum.com")
        self.assertEqual(maildata["h:Reply-To"], "replyTo@peoplesmomentum.com")
        self.assertEqual(maildata["to"], "ha@ha.com")
        self.assertEqual(maildata["cc"], ["keir.hardie@hotmail.com", "keir.starmer@hotmail.com"])
        self.assertEqual(maildata["bcc"], ["ed.broadbent@canada.com", "audrey.mclaughlin@canada.com"])
        self.assertEqual(maildata["subject"], "This is my subject")
        self.assertEqual(maildata["body"], "Dear %recipient.salutation%, I rock!")
        self.assertEqual(maildata["html"], "<p>Dear %recipient.salutation%<br/>, I rock</p>")
        variables = json.loads(maildata["recipient-variables"])
        self.assertEqual(variables["ha@ha.com"]["salutation"], "Mike")
        self.assertEqual(variables["keir.hardie@hotmail.com"]["salutation"], "Keir")
        self.assertEqual(variables["keir.starmer@hotmail.com"]["salutation"], "Keir")
        self.assertEqual(variables["ed.broadbent@canada.com"]["salutation"], "Ed")
        self.assertEqual(variables["audrey.mclaughlin@canada.com"]["salutation"], "Audrey")
