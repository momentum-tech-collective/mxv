from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.views import PasswordResetView
from django.http import Http404
from django.conf import settings
from review.models import TrackVoting
from django.shortcuts import render, redirect
from mxv.models import Reconsent
from mxv.forms import ReconsentForm
from django.contrib import messages
from django.http.response import HttpResponseRedirect, HttpResponseServerError
from django.urls.base import reverse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import ensure_csrf_cookie
from mxv.nation_builder import NationBuilder
from members.models import ensure_nationbuilder_person
from groups.models import Group, GroupStatusType
from members.models import *
import time
import logging
import requests

logger = logging.getLogger()
logger.level = logging.DEBUG

def member_is_dpa(request):
    if not Group.objects.filter(dpa=request.user).exists():
        return False
    # TODO: will crash if single DPA has more than one group, shouldn't happen in prod
    group = Group.objects.get(dpa=request.user)
    return group.status in {GroupStatusType.Verified, GroupStatusType.ScotlandVerified, GroupStatusType.WalesVerified}

# landing page
@login_required
def index(request):
    template = loader.get_template('mxv/index.html')
    
    context = {
        'show_track3_voting': settings.TRACK3_VOTING_VISIBLE_TO_NON_STAFF or request.user.is_staff,
        'show_consultations': settings.CONSULTATIONS_VISIBLE_TO_NON_STAFF or request.user.is_staff,
        'show_members_card': settings.MEMBERSHIP_CARD_VISIBLE_TO_NON_STAFF or request.user.is_staff,
        'show_questions': settings.QUESTIONS_VISIBLE_TO_NON_STAFF or request.user.is_staff,
        'show_primaries': settings.PRIMARIES_VISIBLE,
        'show_inbox': settings.INBOX_VISIBLE,
        'show_update_subs': request.user.is_superuser or settings.UPDATE_SUBS_VISIBLE_TO_NON_STAFF,
        'track3_voting': TrackVoting.objects.filter(pk=3).first(),
        'member_logged_in': request.user.is_authenticated,
        'member_is_dpa': member_is_dpa(request),
        'show_policy_primary_submissions': settings.POLICY_PRIMARY_SUBMISSIONS_VISIBLE
    }

    # return NB person
    person = ""
    if settings.MEMBERSHIP_CARD_VISIBLE_TO_NON_STAFF == True:
        nb = NationBuilder()
        member = request.user
        ensure_nationbuilder_person(nb, member)
        nb_person = NationBuilderPerson.objects.get(id=member.nationbuilderperson_id)
        nb_context = {
            'nb_membershipno': nb_person.membership_number,
            'nb_email': nb_person.email,
            'nb_address1': nb_person.primary_address1,
            'nb_address2': nb_person.primary_address2,
            'nb_address3': nb_person.primary_address3,
            'nb_address_city': nb_person.primary_city,
            'nb_address_postcode': nb_person.primary_postcode
        }

        if nb_person.first_name and nb_person.last_name:
            full_name = f'{nb_person.first_name} {nb_person.last_name}'
            nb_context.setdefault('nb_full_name', full_name)

        context.update(nb_context)

    return HttpResponse(template.render(context, request))

# pass the site names to the password reset context
class ExtraContextPasswordResetView(PasswordResetView):
    extra_email_context = {
         'site_name_short': settings.SITE_NAME_SHORT,
         'site_name_long': settings.SITE_NAME_LONG,
         }

# raises an error if allowed
def error(request):
    if settings.ALLOW_ERROR_URL:
        raise Exception('error test')
    else:
        raise Http404('not found')
    
# returns the IP address from a request
def ip_from_request(request):
    
    # use the forwarded address if behind a router
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        return(x_forwarded_for.split(',')[-1].strip())
    else:
        return(request.META.get('REMOTE_ADDR'))
        
# re-consent page
def reconsent(request):
    
    # if post...
    if request.method == 'POST':
        form = ReconsentForm(request.POST)
        
        # just complete if the re-consent already exists
        exists = Reconsent.objects.filter(email = form.data['email'])
        if exists:
            return redirect('reconsent_complete')
        
        # if valid post for new email...
        if form.is_valid():
            
            # save the email and IP
            reconsent = form.save(commit = False)
            reconsent.ip_address = ip_from_request(request)
            reconsent.save()
            
            # complete
            return redirect('reconsent_complete')
        
        else:
            #show errors
            messages.error(request, 'Please correct the errors below.')
            
    else:
        # if there is an email parameter...
        email = request.GET.get('email', None)
        if email:
            
            # if the email does not already exist...
            exists = Reconsent.objects.filter(email = email)
            if not exists:
                
                # save the email and IP
                reconsent = Reconsent()
                reconsent.email = email
                reconsent.ip_address = ip_from_request(request)
                reconsent.save()
            
            # complete
            return redirect('reconsent_complete')
        else:
            # no parameter so allow user to enter an email
            form = ReconsentForm()
    
    return render(request, 'mxv/new_reconsent.html', { 
        'title' : 'Re-consent',
        'form' : form})
    
# re-consent complete page
def reconsent_complete(request):
    return render(request, 'mxv/reconsent_complete.html', { 
        'title' : 'Re-consent complete'})
    
# /u/[unique_token] redirects to /members/update_details/1?unique_token=[unique_token]
def redirect_to_update_details(request, unique_token):
    return HttpResponseRedirect('%s?unique_token=%s' % (reverse('members:update_details', kwargs = {'page': 1}), unique_token))

@ensure_csrf_cookie
def react(request):
    return render(request, 'build/index.html', {})

# Pass the imprint text from MoApp. More or less just copied over from yl,
def imprint(request):
    try:
        response = requests.get('https://api.peoplesmomentum.com/imprint', timeout=10)
        if response.status_code == 200:
            return HttpResponse(response.text, content_type='text/plain; charset=UTF-8')
        else:
            logging.error(response.text)
            return HttpResponseServerError()
    except Exception as e:
        logging.error(e)
        return HttpResponseServerError()
