from django.conf import settings
from rest_framework.response import Response
from rest_framework import status
import json
import logging
import pprint
import requests

pp = pprint.PrettyPrinter()

# field sizes
url_text_length = 32
short_text_length = 255
long_text_length = 2000

def htmlify(text):
    body = text.replace("\n", "<p/>")
    return f'<html><body>{body}</body></html>'

def send_mailgun_email(to_email, to_salutation, cc_dict, bcc_dict, fromAddr, replyTo, subject, text, html):
    cc = list(cc_dict.values())
    bcc = list(bcc_dict.values())

    variables = {}
    variables[to_email] = { 'salutation': to_salutation }
    all_emails = { **cc_dict, **bcc_dict }
    for name, addr in all_emails.items():
        variables[addr] = { 'salutation': name.split(' ')[0] }
    maildata = {
        "from": fromAddr,
        "h:Reply-To": replyTo,
        "to": to_email,
        "cc": cc,
        "bcc": bcc,
        "subject": subject,
        "body": text,
        "html": html,
        "recipient-variables": json.dumps(variables),
    }

    mgResp = requests.post(
        f'https://api.mailgun.net/v3/{settings.MAILGUN_DOMAIN}/messages',
        auth=("api", settings.MAILGUN_API_KEY),
        data=maildata,
    )
    if (mgResp.status_code != 200):
        logging.error(f'Unable to send MailGun email: {mgResp.status_code} {mgResp.text}')
    if (mgResp.status_code == 200):
        logging.info(f'{fromAddr} sent an email')

def postcode_geo(postcode: str, scotland=False):
    extra = 'scotland/' if scotland else ''
    response = requests.get(f'https://api.postcodes.io/{extra}postcodes/{postcode}')
    data = response.json()
    if data['status'] != 200 or not "result" in data:
        raise Exception(f'Failed to geocode postcode: {postcode}.')
    return data['result']


def success():
    return Response({"message": "success"})

def fail(prod, dbg=None):
    if settings.DEBUG and dbg:
        message = dbg
    else:
        message = prod
    return Response({"message": message}, status=status.HTTP_400_BAD_REQUEST)

