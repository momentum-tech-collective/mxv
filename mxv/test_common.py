import logging
import pprint
import sys
from members.models import Member, NationBuilderPerson

pp = pprint.PrettyPrinter()


def create_test_member(email, first_name, last_name):
    nbperson = NationBuilderPerson.objects.create(email=email, first_name=first_name, last_name=last_name)
    member = Member.objects.create_member(email, first_name, last_name, 'password')
    member.is_active = True
    member.save()
    return member
