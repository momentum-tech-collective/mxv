from . import views
from . import forms
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.http import HttpResponse, HttpResponseRedirect

urlpatterns = [
    # landing page
    url(r'^$', views.index, name='index'),
    
    # admin
    url(r'^admin/', include('members.adminurls')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^nested_admin/', include('nested_admin.urls')),

    # redirect to react app    
    url(r'^app/', views.react),
    url(r'^api-auth/', include('rest_framework.urls')),

    # error testing
    url(r'^error/$', views.error, name='error'),
    
    # for various app urls.py

    # members
    url(r'^members/', include('members.urls')),
    url(r'^members/password_reset/$', views.ExtraContextPasswordResetView.as_view(
        form_class=forms.ActivationEmailPasswordResetForm), name='password_reset'),
    url(r'^members/login/$', auth_views.LoginView.as_view(
        form_class=forms.ActivationEmailAuthenticationForm), name='login'),
    url(r'^members/', include('django.contrib.auth.urls')),
    # q&a
    url(r'^questions/', include('questions.urls')),
    # lobbying 
    url(r'^lobby/', include('lobby.urls')),
    # groups
    url(r'^groups/', include('groups.urls')),
    # publications/petitions/cms
    url(r'^publications/', include('petitions.urls')),
    # democracy review
    url(r'^review/', include('review.urls')), 
    # GDPR
    url(r'^reconsent/$', views.reconsent, name='reconsent'), 
    url(r'^reconsent_complete/$', views.reconsent_complete, name='reconsent_complete'),
    # voting intentions
    url(r'^voting_intentions/', include('voting_intentions.urls')),
    # consultations
    url(r'^consultations/', include('consultations.urls')),
    # short URL redirects so that the links can be used in SMS
    url(r'^u/(?P<unique_token>[a-zA-Z0-9]+)/$',
        views.redirect_to_update_details, name='redirect_to_update_details'),

    # for martor, the markdown editor
    url(r'^martor/', include('martor.urls')),
    # task queueing
    url(r'^django-rq/', include('django_rq.urls')),

    # robots.txt
    url(r'^robots.txt', lambda x: HttpResponse(
        "User-Agent: *\nDisallow:", content_type="text/plain"), name="robots_file"),
    
    # humans.txt
    url(r'^humans.txt', lambda x: HttpResponseRedirect("https://gitlab.com/momentum-tech-collective/mxv")),

    # Imprint
    url(r'^imprint/', views.imprint)
]

if settings.PRIMARIES_VISIBLE:
    urlpatterns += [
        url(r'^primaries/', include('primaries.urls')),
    ]

admin.autodiscover()
admin.site.enable_nav_sidebar = False
