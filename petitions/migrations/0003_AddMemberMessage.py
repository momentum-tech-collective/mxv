# Generated by Django 3.1.4 on 2021-02-16 12:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('petitions', '0002_petition_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='MemberMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('body_text', models.TextField()),
                ('published', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
