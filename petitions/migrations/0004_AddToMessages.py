# Generated by Django 3.1.4 on 2021-02-17 09:26

from django.db import migrations, models
import django.utils.timezone
import martor.models


class Migration(migrations.Migration):

    dependencies = [
        ('petitions', '0003_AddMemberMessage'),
    ]

    operations = [
        migrations.AddField(
            model_name='membermessage',
            name='published_at',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='membermessage',
            name='standfirst',
            field=models.CharField(default='Sf', max_length=80),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='membermessage',
            name='body_text',
            field=martor.models.MartorField(),
        ),
    ]
