from django.contrib import admin
from .models import Petition, MemberMessage

@admin.register(Petition)
class PetitionAdmin(admin.ModelAdmin):
    list_display = ('admin_name', 'slug', 'published', 'redirect',
                    'nationbuilder_tag', 'created_at', 'updated_at')
    ordering = ('updated_at',)
    list_filter = ('published',)


@admin.register(MemberMessage)
class MemberMessageAdmin(admin.ModelAdmin):
    list_display = ('title', 'published', 'created_at', 'published_at','updated_at')
    list_filter = ('published',)
