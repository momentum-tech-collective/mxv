from django.db import models
from django.core.exceptions import ValidationError
from cloudinary.models import CloudinaryField
from martor.models import MartorField
from django.utils import timezone
from mxv.utils import url_text_length, short_text_length
from django.core.validators import validate_slug, URLValidator

class Petition(models.Model):
    admin_name = models.CharField(
        max_length=short_text_length, unique=True, help_text="This is for internal use only -- the reference name of this petition")
    slug = models.CharField(max_length=url_text_length, unique=True, help_text="the url that the petition should use!", validators=[validate_slug])
    title = models.CharField(max_length=short_text_length, help_text="The title the user will see. 'Stop Boris eating kebabs!' for e.g.")
    header_image = CloudinaryField('Header Image', blank=True, null=True, help_text="The image that will appear on the petition itself")
    body_text = models.TextField(help_text="The blurb of the petition")
    published = models.BooleanField(default=False, help_text="Tick this to publish the poll")
    meta_image = CloudinaryField('Meta Image', blank=True, null=True, help_text="The meta image used for the petition")
    redirect = models.URLField(max_length=short_text_length, blank=True, null=True, help_text="Enter a URL to redirect users. If not entered, will direct to a generic thanks page.", validators=[URLValidator])
    nationbuilder_tag = models.CharField(max_length=url_text_length, help_text="What should we tag the signups with?")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.admin_name

    def clean(self):
        if self.published and (not self.header_image or not self.meta_image):
            errors = {}
            if not self.header_image:
                errors['header_image'] = 'You must have a header image to publish'
            if not self.meta_image:
                errors['meta_image'] = 'You must have a meta image to publish'
            raise ValidationError(errors)

class MemberMessage(models.Model):
    title = models.CharField(max_length=255)
    standfirst = models.CharField(max_length=80)
    body_text = MartorField()
    published_at = models.DateTimeField(default=timezone.now)
    published = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
