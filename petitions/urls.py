from django.conf.urls import url
from petitions import views

urlpatterns = [
    url(r'^memberMessages/', views.AllMessages.as_view()),
]
