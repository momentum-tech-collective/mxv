from petitions.models import *
from rest_framework import serializers

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = MemberMessage
        exclude = ['published', 'created_at', 'updated_at']
