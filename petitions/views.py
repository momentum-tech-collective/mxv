from django.conf import settings

from django.contrib.auth.decorators import login_required

from django.http import HttpResponse

from petitions.models import MemberMessage
from petitions.serializers import MessageSerializer

from rest_framework.generics import *
from rest_framework.response import Response
from rest_framework.views import APIView

import logging

@login_required
def index(request):
    template = loader.get_template('petitions/index.html')
    return HttpResponse(template.render(request))


class AllMessages(ListAPIView):
    queryset = MemberMessage.objects.all()

    def list(self, request, *args, **kwargs):
        messages = self.get_queryset().filter(published=True).all()
        serializer = MessageSerializer(messages, many=True)
        if settings.INBOX_VISIBLE:
            return Response(serializer.data)
