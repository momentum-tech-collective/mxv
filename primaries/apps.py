from django.apps import AppConfig


class PrimariesConfig(AppConfig):
    name = 'primaries'
