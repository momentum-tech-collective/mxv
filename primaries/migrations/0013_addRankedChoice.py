# Generated by Django 3.1.4 on 2021-02-25 15:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('primaries', '0012_territories_app2'),
    ]

    operations = [
        migrations.AddField(
            model_name='primary',
            name='ranked_choice',
            field=models.BooleanField(default=False),
        ),
        migrations.CreateModel(
            name='RankedVote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ballot', models.JSONField()),
                ('primary', models.ForeignKey(editable=False, on_delete=django.db.models.deletion.CASCADE, related_name='votes', to='primaries.primary')),
            ],
        ),
    ]
