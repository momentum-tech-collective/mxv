from django.conf.urls import url
from primaries import views

urlpatterns = [
    url(r'^primaries/', views.CurrentPrimariesView.as_view()),
    url(r'^completed/', views.CompletedPrimariesView.as_view()),
    url(r'^cast/', views.CastPrimariesView.as_view()),
    url(r'^vote/', views.VoteView.as_view()),
    url(r'^stand/', views.StandView.as_view()),
    url(r'^nominate/', views.NominateView.as_view()),
]
