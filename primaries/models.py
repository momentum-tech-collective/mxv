from dateutil.relativedelta import relativedelta
from dateutil.parser import *
from datetime import date
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.query_utils import Q
from cloudinary.models import CloudinaryField
from mxv.models import BaseVote, STATUS_CHOICES
from mxv.utils import short_text_length, long_text_length
from django.contrib.postgres.fields import ArrayField

from territories.models import Territory
import datetime
import requests
import logging

NOON = datetime.time(hour=12)

RESTRICTION_TYPES=(
    ('age', 'Age'),
    ('field', 'NationBuilder standard or custom field'),
    ('tag', 'NationBuilder tag'),
)

class Restriction(models.Model):
    name = models.CharField(max_length=short_text_length)
    restriction_type = models.CharField(choices=RESTRICTION_TYPES,max_length=short_text_length)
    max_age = models.PositiveSmallIntegerField(null=True,blank=True,
        help_text='For Age restrictions, the maximum age of individuals allowed to vote in this primary')
    cutoff_date = models.DateField(null=True,blank=True,
        help_text='For Age restrictions, the date we evaluate eligibility for')
    field_slug = models.CharField(blank=True,max_length=short_text_length,
        help_text='For Field restrictions, NationBuilder standard or custom field slug to query. Nested fields not yet supported.')
    field_value = models.CharField(blank=True,max_length=short_text_length,
        help_text='For Field restrictions, required value for the NationBuilder field')
    tag = models.CharField(blank=True,max_length=short_text_length,
        help_text='For Tag restrictions, name of the required NationBuilder tag')

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

    def clean(self):
        super().clean()
        if self.restriction_type == 'age' and not self.max_age:
            raise ValidationError('Max age must be specified for age restrictions')
        if self.restriction_type in ['std_field','custom_field'] and \
            (not self.field_slug or not self.field_value):
            raise ValidationError('NationBuilder field name and value required')
        if self.restriction_type == 'tag' and not self.tag:
            raise ValidationError('NationBuilder tag name required')


class Primary(BaseVote):
    max_votes = models.PositiveSmallIntegerField(default=1,
        help_text='Number of votes each member can cast')
    territories = models.ManyToManyField(Territory,blank=True, related_name='primaries_including',
        help_text='Territories to include')
    exclude_territories = models.ManyToManyField(Territory, blank=True, related_name='primaries_excluding',
        help_text='Territories to exclude')
    restrictions = models.ManyToManyField(Restriction, blank=True)
    show_results = models.BooleanField(default=False)
    min_nominations = models.IntegerField(default=0)
    registrations_start = models.DateField(null=True, blank=True)
    nominations_start = models.DateField(null=True, blank=True, help_text="Leave blank if nominations are not required")
    nominations_end = models.DateField(null=True, blank=True, help_text="Leave blank if nominations are not required")
    ranked_choice = models.BooleanField(default=False)

    def voting_closed(self):
        deadline = datetime.datetime.combine(self.voting_end, NOON)
        return datetime.datetime.now() >= deadline

    class Meta:
        verbose_name_plural = 'primaries'
        constraints = [
            models.CheckConstraint(check=Q(max_votes__gte=1),name='min_one_vote'),
        ]
        ordering = ('voting_end', 'name',)

class Candidate(models.Model):
    primary = models.ForeignKey(Primary,related_name='candidates',on_delete=models.CASCADE)
    first_name = models.CharField(max_length=short_text_length)
    last_name = models.CharField(max_length=short_text_length)
    statement = models.TextField(blank=True, max_length=4000)
    photo = CloudinaryField('photo', null=True, blank=True)
    video_url = models.URLField('video_url', max_length=short_text_length, null=True, blank=True)
    woman = models.BooleanField(default=True, help_text="Leave this box checked only if the candidate qualifies for a Labour all-women's shortlist")
    bame = models.BooleanField(default=False, help_text="Check this box if the candidate self-identifies as BAME")
    disabled = models.BooleanField(default=False, help_text="Check this box if the candidate self-identifies as disabled") 
    lgbtq = models.BooleanField(default=False, help_text="Check this box if the candidate self-identifies as LGBTQ+")
    member = models.OneToOneField(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)
    checked_labour = models.BooleanField(default=False, help_text="The candidate's Labour membership has been verified")
    status = models.CharField(default='pending', choices=STATUS_CHOICES, max_length=16)
    reject_reason = models.TextField(max_length=long_text_length, null=True, blank=True)
    over_18 = models.BooleanField(default=True)
    twitter = models.CharField(max_length=short_text_length, null=True, blank=True)
    facebook = models.CharField(max_length=short_text_length, null=True, blank=True)
    website = models.CharField(max_length=short_text_length, null=True, blank=True)
    data_controller_name = models.CharField(max_length=short_text_length, null=True)
    data_controller_email = models.CharField(max_length=short_text_length, null=True)
    
    def __str__(self):
        return f'{self.first_name} {self.last_name}'
    class Meta:
        ordering = ('primary', 'last_name',)

class Nomination(models.Model):
    candidate = models.ForeignKey(Candidate, related_name='nominations', on_delete=models.CASCADE)
    member = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE, editable=False)
    status = models.CharField(default='pending', choices=STATUS_CHOICES, max_length=16)

class Vote(models.Model):
    # to preserve secret ballot we don't link to the member
    # TODO: see if we can add a field to ensure the integrity of this table, see what Helios does
    candidate = models.ForeignKey(Candidate,related_name='votes',on_delete=models.CASCADE, editable=False)

class RankedVote(models.Model):
    ballot = models.JSONField()
    primary = models.ForeignKey(Primary, related_name='votes', on_delete=models.CASCADE, editable=False)
    # TODO: see if we can add a field to ensure the integrity of this table, see what Helios does

class HasVoted(models.Model):
    primary = models.ForeignKey(Primary,on_delete=models.CASCADE, editable=False)
    member = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE, editable=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['primary', 'member'], name='vote_once'),
        ]
