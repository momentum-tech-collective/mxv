from django.contrib import admin
from django.db.models import Count
from django.utils.safestring import mark_safe
from primaries.models import *

def bool_to_yn(bool_value):
    return "Yes" if bool_value else "No"

@admin.register(Candidate)
class CandidateAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'primary', 'checked_labour', 'status')
    list_filter = ('primary', 'status', 'checked_labour')
    autocomplete_fields = ('member',)


@admin.register(Nomination)
class NominationAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'candidate')
    list_filter = ('candidate',)


@admin.register(Primary)
class PrimaryAdmin(admin.ModelAdmin):
    list_display = ('name', 'visible_to_non_staff', 'voting_start', 'voting_end', 'max_votes')
    readonly_fields = ('raw_results_table',)
    autocomplete_fields = ('territories', 'exclude_territories')
    # TODO candidates should not be deletable after a certain date

    def get_fields(self, request, primary):
        fields = ['name']
        if primary and primary.voting_closed():
            fields += ['raw_results_table'] 
            # TODO show_results is not in this list, so there's no way to show results in the client.
            # Reason: because of AWSes and BAME quotas in the YL primaries, we decided not to deal with
            # that complexity but will create a PDF to show results in the short term
        # TODO prevent accidental editing after voting closes
        fields += ['description', 'visible_to_non_staff', 'registrations_start', 'min_nominations', 'nominations_start', 'nominations_end','voting_start', 'voting_end', 'max_votes', 'territories', 'exclude_territories', 'restrictions', 'ranked_choice']
        return fields

    def raw_results_table(self, primary):
        if not primary or not primary.voting_closed():
            return 'Voting has not yet closed'
        results = primary.candidates \
            .annotate(num_votes=Count('votes')) \
            .order_by('-num_votes', 'last_name') \
            .all()
        output = '<table><tr><th>Candidate</th><th>Woman</th><th>BAME</th><th>Votes</th></tr>'
        for result in results:
            output += f'<tr><td>{result.first_name}&nbsp;{result.last_name}</td>'
            output += f'<td>{bool_to_yn(result.woman)}</td><td>{bool_to_yn(result.bame)}</td>'
            output += f'<td>{result.num_votes}</td></tr>'
        output += '</table>'
        return mark_safe(output)

# TODO consider improving the restrictions UI
admin.site.register(Restriction)
