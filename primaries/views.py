from django.conf import settings
from django.db import transaction
from django.db.models import Exists, OuterRef
from django.utils import timezone
from rest_framework.views import APIView
from rest_framework.generics import *
from rest_framework.response import Response
from members.models import *
from mxv.nation_builder import NationBuilder
from mxv.utils import postcode_geo, fail, success
from primaries.models import *
from primaries.serializers import *
import datetime

import logging
import pprint
pp = pprint.PrettyPrinter(indent=4)

def lookup_nb(nb, member):
    try:
        ensure_nationbuilder_person(nb, member)
        nb_record = nb.LookupPerson(member.nationbuilderperson.nation_builder_id)
        return nb_record
    except Exception as e:
        logging.error(e)
        return None

def territory_eligible(nb_record, territory):
    if not "primary_address" in nb_record or not "zip" in nb_record["primary_address"]:
        return False
    try:
        scotland = territory.lookup_field == 'scottish_parliamentary_constituency'
        geo = postcode_geo(nb_record["primary_address"]["zip"], scotland)
    except Exception as e:
        # TODO: distinguish between postcodes being down and invalid postcode
        logging.error(e)
        return False
    actual = geo[territory.lookup_field]
    return actual and actual.casefold() == territory.name.casefold()

def age_eligible(nb_record, restriction):
    if 'birthdate' not in nb_record or not nb_record['birthdate']:
        return False
    min_birthdate = restriction.cutoff_date + relativedelta(years=-restriction.max_age-1)
    birthdate = parse(nb_record['birthdate']).date()
    return birthdate > min_birthdate

def field_eligible(nb_record, restriction):
    if not restriction.field_slug in nb_record:
        return False
    isstring = False
    if restriction.field_value.casefold() == 'true':
        lookup_value = True
    elif restriction.field_value.casefold() == 'false':
        lookup_value = False
    elif restriction.field_value.isdigit():
        # negative numbers and floats not yet supported
        lookup_value = int(restriction.field_value)
    else:
        return restriction.field_value.casefold() == nb_record[restriction.field_slug].casefold()
    return lookup_value == nb_record[restriction.field_slug]

def tag_eligible(nb_record, restriction):
    return restriction.tag.casefold() in [tag.casefold() for tag in nb_record.tags]

def restriction_eligible(nb_record, restriction):
    if restriction.restriction_type == 'age':
        return age_eligible(nb_record, restriction)
    elif restriction.restriction_type == 'field':
        return field_eligible(nb_record, restriction)
    elif restriction.restriction_type == 'tag':
        return tag_eligible(nb_record, restriction)
    else:
        raise Exception(f'Unsupported restriction type {restriction.restriction_type}')

def eligible(nb_record, primary):
    for restriction in primary.restrictions.all():
        if not restriction_eligible(nb_record, restriction):
            return False
    if primary.territories.count() > 0:
        matches = [
            territory
            for territory in primary.territories.all()
            if territory_eligible(nb_record, territory)
        ]
        if len(matches) == 0:
            return False
    for territory in primary.exclude_territories.all():
        if territory_eligible(nb_record, territory):
            return False
    return True

MUST_HAVE_ONE_OF_TAGS = ['momentum-member','cfs-2020-0527', 'wlg-2020-0527']
CANNOT_HAVE_TAGS = ['momentum-cancelled', 'cfs-former', 'wlg-former', 'Deceased']

def has_intersection(list1, list2):
    return not set(list1).isdisjoint(list2)

def voter_eligible(nb_record):
    if 'membership_payment_status' not in nb_record:
        return False
    if 'tags' not in nb_record:
        return False
    status = nb_record['membership_payment_status']
    tags = nb_record['tags']
    return \
        (status == 'active' or status == 'affiliate') and \
        has_intersection(tags, MUST_HAVE_ONE_OF_TAGS) and \
        not has_intersection(tags, CANNOT_HAVE_TAGS)

# If called before noon, return today, else tomorrow
def last_day_voting_open():
    noon = datetime.time(hour=12, tzinfo=timezone.get_default_timezone())
    now = timezone.localtime().time()
    if now < noon:
        return datetime.date.today()
    else:
        return datetime.date.today() + datetime.timedelta(days=1)

def get_eligible(nb, member, queryset=Primary.objects.all()):
    nb_record = lookup_nb(nb, member)
    if not nb_record:
        return []
    eligible_primaries = []
    if voter_eligible(nb_record):
        future_primaries = queryset.filter(visible_to_non_staff=True, voting_end__gte=last_day_voting_open()) \
            .exclude(models.Exists(HasVoted.objects.filter(primary__id=OuterRef('pk'), member=member))) \
            .all()
        eligible_primaries = [
            primary
            for primary in future_primaries
            if eligible(nb_record, primary)
        ]
    return eligible_primaries

def get_cast(nb, member, queryset=Primary.objects.all()):
    nb_record = lookup_nb(nb, member)
    if not nb_record:
        return []
    cast_primaries = []
    if voter_eligible(nb_record):
        cast_primaries = queryset.filter(visible_to_non_staff=True, voting_end__gte=last_day_voting_open()) \
            .filter(models.Exists(HasVoted.objects.filter(primary__id=OuterRef('pk'), member=member))) \
            .all()
    return cast_primaries

# TODO: having separate API calls for current and cast means
# we make the same call to NB twice in a row, doubling the risk of timeout.
# These two should really be combined into one, 
# it should be the client's job to separate them

class CurrentPrimariesView(ListAPIView):
    queryset = Primary.objects.all()
    def list(self, request, *args, **kwargs):
        member = self.request.user
        nb = NationBuilder()
        eligible_primaries = get_eligible(nb, member, self.queryset)
        serializer = PrimarySerializer(eligible_primaries, many=True)
        return Response(serializer.data)


class CastPrimariesView(ListAPIView):
    queryset = Primary.objects.all()
    def list(self, request, *args, **kwargs):
        member = self.request.user
        nb = NationBuilder()
        eligible_primaries = get_cast(nb, member, self.queryset)
        serializer = PrimarySerializer(eligible_primaries, many=True)
        return Response(serializer.data)

class CompletedPrimariesView(ListAPIView):
    queryset = Primary.objects.all()
    def list(self, request, *args, **kwargs):
        past_primaries = self.get_queryset() \
            .filter(voting_end__lt=last_day_voting_open(), show_results=True) \
            .all()
        serializer = PrimarySerializer(past_primaries, many=True)
        return Response(serializer.data)



def primary_eligible(member, primary):
    # TODO: Can this return a bool, and not a string? 
    nb_record = lookup_nb(NationBuilder(), member)
    if not nb_record:
        return "Unable to reach database"
    if not voter_eligible(nb_record):
        return "Not eligible to vote"
    if not eligible(nb_record, primary):
        return "Not eligible for this primary"
    return "success"


class StandView(CreateAPIView):
    def create(self, request, *args, **kwargs):
        pfilter = Primary.objects.filter(pk=request.data['primary'])
        if not pfilter.exists():
            return fail('Invalid candidate', 'Invalid primary')
        primary = pfilter[0]
        today = datetime.date.today()
        if today < primary.registrations_start or \
           today > primary.nominations_end or \
           today > primary.voting_start:
            return fail('Registrations are closed')
        serializer = CandidateSerializer(data=request.data)
        if not serializer.is_valid():
            return fail("Invalid candidate", dbg=serializer.errors)
        member = request.user
        eligible = primary_eligible(member, primary)
        if eligible != "success":
            return fail(eligible)
        candidate = serializer.save()
        candidate.member = member
        candidate.save()
        return success()

class NominateView(CreateAPIView):
    def create(self, request, *args, **kwargs):
        if not Candidate.objects.filter(pk=request.data['candidate']).exists():
            return fail('Invalid candidate' if settings.DEBUG else 'Invalid nomination')
        candidate = Candidate.objects.get(pk=request.data['candidate'])
        member=request.user
        if candidate.member.id == member.id:
            return fail('Invalid nomination', dbg="Can't nominate self")
        primary = candidate.primary
        today = datetime.date.today()
        if today < primary.nominations_start or \
           today > primary.nominations_end or \
           today > primary.voting_start:
            return fail('Nominations are closed')
        if Nomination.objects.filter(candidate=candidate).count() >= primary.min_nominations:
            return fail('Candidate has enough nominations')
        if Nomination.objects.filter(member=member, candidate=candidate).exists():
            return fail('Already nominated this candidate')
        eligible = primary_eligible(member, primary)
        if eligible != "success":
            return fail(eligible)
        nomination = Nomination.objects.create(
            member=request.user, candidate=candidate)
        return success()

def ranked_ballot_is_valid(raw_candidates, primary, fail):
    # TODO: ensure that the ranks are correct, and that the ballot itself isn't spoiled
    # "checksum" the candidates (ensure all candidate ids are indeed valid)

    ranks_set = set(map(lambda cand: cand['rank'], raw_candidates))
    ranks_sorted_list = sorted(list(ranks_set))

    # ensure all ranks are unique
    if len(ranks_set) != len(raw_candidates):
        return fail("Some candidates share a rank on this ballot")

    # ensure the max amount of votes is adhered to
    if len(raw_candidates) > primary.max_votes:
        return fail("Too many candidates on this ballot")

    for i, num in enumerate(ranks_sorted_list):
        # ensure that the first rank is one
        if i == 0 and num != 1:
            return fail("First preference missing from this ballot")
        # ensure the rank is consecutive
        if num != i + 1:
            return fail("This ballot's ranks are not ordered consecutively")
    return True

class VoteView(APIView):
    def fail(self, message):
        return fail("Invalid vote", message)

    def post(self, request, format=None):
        # ensure member exists
        member = request.user
        if not member or not member.id:
            return self.fail("No membership record found")
        
        # ensure the ballot type exists, and then set it
        if 'ranked' in request.data.keys():
            ranked_ballot = request.data['ranked']
        else:
            return self.fail("Ballot type not specified on submission")

        raw_candidates = request.data['candidates']
        
        # ensure votes exist
        if not len(raw_candidates):
            return self.fail("No votes")

        if ranked_ballot:
            if type(raw_candidates[0]) != type({}):
                return self.fail("Wrong type of ballot submitted to ranked choice primary")
            else:
                candidate_strs = list(map(lambda c: c['id'], raw_candidates))
        else: 
            if type(raw_candidates[0]) != type(""):
                return self.fail("Wrong type of ballot submitted to first-past-the-post primary")
            else:
                candidate_strs = raw_candidates

        candidate_ids = set([
            int(candidate_id)
            for candidate_id in candidate_strs
        ])

        candidates = Candidate.objects.filter(id__in=candidate_ids).all()
        primary_ids = set([candidate.primary.id for candidate in candidates])

        # ensure only one primary exists
        if len(primary_ids) != 1:
            return self.fail("Candidates from multiple primaries")

        # ensure primary is visible
        primary = Primary.objects.get(id=primary_ids.pop())
        if not primary.visible_to_non_staff:
            return self.fail("Primary not open")

        # ensure that the ballot is of the right type
        if ranked_ballot != primary.ranked_choice:
            return self.fail("Incorrect ballot type")

        # ensure there are not more candidates than the primary maximum
        if len(candidates) > primary.max_votes:
            return self.fail("Too many votes")
        
        # ensure the member has not voted
        has_voted = HasVoted.objects.filter(primary=primary, member=member)
        if has_voted.exists():
            return self.fail("Already voted")

        # ensure the primary is open 
        today = datetime.date.today()
        if today < primary.voting_start or today > primary.voting_end:
            return self.fail("Not in voting period")
        
        # ensure the member is eligible to vote
        eligible = primary_eligible(member, primary)
        if eligible != "success":
            return self.fail(eligible)

        # validate ranked ballot
        if ranked_ballot:
            checked_ranked_ballot = ranked_ballot_is_valid(raw_candidates, primary, self.fail)
            if checked_ranked_ballot != True:
                return checked_ranked_ballot

        # cast the vote
        with transaction.atomic():
            has_voted = HasVoted(primary=primary, member=member)
            has_voted.save()
            if ranked_ballot:
                ballot = RankedVote(
                    ballot=raw_candidates,
                    primary=primary)
                ballot.save()
            else:
                for candidate in candidates:
                    vote = Vote(candidate=candidate)
                    vote.save()
            
        return success()
