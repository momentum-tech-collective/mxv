from django import test
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase
from primaries.models import *
from primaries.views import lookup_nb, ranked_ballot_is_valid, VoteView
from members.models import Member
from unittest import skip
from unittest.mock import patch
import datetime
from mxv import test_common

# Note: this doesn't work, because the variable is used in urls.py, which is evaluated before 
# the test case runs. The .env file must set MXV_PRIMARIES_VISIBLE to True
@test.override_settings(PRIMARIES_VISIBLE=True)
class TestPrimaries(APITestCase):
    def setUp(self):
        patcher_nb = patch('primaries.views.lookup_nb')
        patcher_geo = patch('primaries.views.postcode_geo')
        self.mock_lookup_nb = patcher_nb.start()
        self.mock_postcode_geo = patcher_geo.start()
        self.addCleanup(patcher_nb.stop)
        self.addCleanup(patcher_geo.stop)
        self.tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        self.yesterday = datetime.date.today() + datetime.timedelta(days=-1)
        self.nb_record = { "birthdate": "1994-05-24", "id": 107876, "student": True, 
           "primary_address": {"zip": "CR0 7DJ"}, "membership_payment_status": "active",
           "tags": ["momentum-member"]}
        self.mock_lookup_nb.return_value = self.nb_record
        self.voter = test_common.create_test_member('voter@voter.com', 'My', 'Voter')
        self.candidate_member = test_common.create_test_member('candidate@candidate.com', 'My', 'Candidate')
        self.client.force_authenticate(user=self.voter)

    def tearDown(self):
        Vote.objects.all().delete()
        HasVoted.objects.all().delete()
        Candidate.objects.all().delete()
        Primary.objects.all().delete()
        Restriction.objects.all().delete()
        Territory.objects.all().delete()

    def _make_four_candidates(self, primary):
        self.cand1 = Candidate.objects.create(
            primary=primary,first_name='Jeremy',last_name='Corbyn',statement='Oh, Jeremy Corbyn')
        self.cand2 = Candidate.objects.create(
            primary=primary,first_name='Ed',last_name='Miliband',statement='One Nation Labour')
        self.cand3 = Candidate.objects.create(
            primary=primary, first_name='Gordon',last_name='Brown',statement='Not Flash, just Gordon')
        self.cand4 = Candidate.objects.create(
            primary=primary,first_name='Tony',last_name='Blair',statement='New Labour. New Britain.')

    def test_not_visible(self):
        primary = Primary.objects.create(name='primary1', description='Desc',
            voting_start=datetime.date.today(), voting_end=self.tomorrow)
        self._make_four_candidates(primary)
        
        response = self.client.get('/primaries/primaries/', format='json')
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

    def _yl_test(self):
        london = Territory.objects.create(name='London',lookup_field='european_electoral_region')
        students = Restriction.objects.create(name='Student',restriction_type='field',field_slug='student',field_value='true')
        under_27 = Restriction.objects.create(name='Under27',restriction_type='age',max_age=26,cutoff_date=self.tomorrow)
        self.primary1 = Primary.objects.create(name='primary1', description='Desc',
            voting_start=datetime.date.today(), voting_end=self.tomorrow, max_votes=3,
            visible_to_non_staff=True)
        self.primary1.territories.add(london)
        self.primary1.restrictions.add(students)
        self.primary1.restrictions.add(under_27)
        self._make_four_candidates(self.primary1)
        self.mock_lookup_nb.return_value = self.nb_record
        self.mock_postcode_geo.return_value = {"european_electoral_region": "London"}
    
    def _ranked_test(self):
        london = Territory.objects.create(
            name='London', 
            lookup_field='european_electoral_region')
        
        students = Restriction.objects.create(
            name='Student',
            restriction_type='field',
            field_slug='student',
            field_value='true')
        
        under_27 = Restriction.objects.create(
            name='Under27',
            restriction_type='age',
            max_age=26,
            cutoff_date=self.tomorrow)
        
        self.primary1 = Primary.objects.create(
            name='primary1', 
            description='Desc',
            voting_start=datetime.date.today(), 
            voting_end=self.tomorrow, 
            max_votes=3,
            visible_to_non_staff=True, 
            ranked_choice=True)

        self.primary1.territories.add(london)
        self.primary1.restrictions.add(students)
        self.primary1.restrictions.add(under_27)
        self._make_four_candidates(self.primary1)
        
        self.mock_lookup_nb.return_value = self.nb_record
        self.mock_postcode_geo.return_value = {"european_electoral_region": "London"}

    def _candidate_data(self, primary):
        return {
            'primary': primary.id,
            'first_name': 'Jacinda',
            'last_name': 'Ardern',
            'statement': 'I wish I lived in Wales',
            # TODO photo
            'video_url': 'https://www.youtube.com',
            'woman': True,
            'bame': False,
            'disabled': False,
            'lgbtq': False,
            'over_18': True,
            'twitter': '@jacinda',
            'facebook': 'https://m.facebook.com/jacinda',
            'website': 'https://nzlabour.co.nz',
            'data_controller_name': 'Tyrone Nicholas',
            'data_controller_email': 'tyrone.nicholas@gmail.co.nz',
        }

    @skip("Found broken for unknown reason during Digital Team handover 2021")
    def test_basics(self):
        self._yl_test()
        
        response = self.client.get('/primaries/primaries/', format='json')
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(len(response.data[0]["candidates"]), 4)

    @skip("Found broken for unknown reason during Digital Team handover 2021")
    def test_vote(self):
        self._yl_test()
        candidates = [
            str(self.cand1.id), str(self.cand2.id), str(self.cand4.id)]
        
        response = self.client.post(
            '/primaries/vote/', 
            {'ranked': False, 'candidates': candidates}, 
            format='json')
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, HasVoted.objects.filter(member=self.voter, primary=self.primary1).count())
        self.assertEqual(3, Vote.objects.filter(candidate__primary=self.primary1).count())

    def test_multi_territory(self):
        northeast = Territory.objects.create(name='North East',lookup_field='european_electoral_region')
        cumbria = Territory.objects.create(name='Cumbria',lookup_field='admin_county')
        self.primary1 = Primary.objects.create(name='primary1', description='Desc',
            voting_start=datetime.date.today(), voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        self.primary1.territories.add(northeast)
        self.primary1.territories.add(cumbria)
        self.mock_lookup_nb.return_value = self.nb_record
        self.mock_postcode_geo.return_value = {"european_electoral_region": "North West", "admin_county": "Cumbria"}
        
        response = self.client.get('/primaries/primaries/', format='json')
        
        self.assertEqual(len(response.data), 1)

    def test_scottish_constituency(self):
        glasgow_kelvin = Territory.objects.create(name='Glasgow Kelvin', lookup_field='scottish_parliamentary_constituency')
        self.primary1 = Primary.objects.create(name='primary1', description='Desc',
            voting_start=datetime.date.today(), voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        self.primary1.territories.add(glasgow_kelvin)
        self.mock_lookup_nb.return_value = self.nb_record
        self.mock_postcode_geo.return_value = {"scottish_parliamentary_constituency": "Glasgow Kelvin"}
        
        response = self.client.get('/primaries/primaries/', format='json')
        
        self.assertEqual(len(response.data), 1)

    def test_exclude_territory(self):
        northwest = Territory.objects.create(name='North West',lookup_field='european_electoral_region')
        cumbria = Territory.objects.create(name='Cumbria',lookup_field='admin_county')
        self.primary1 = Primary.objects.create(name='primary1', description='Desc',
            voting_start=datetime.date.today(), voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        self.primary1.territories.add(northwest)
        self.primary1.exclude_territories.add(cumbria)
        self.mock_lookup_nb.return_value = self.nb_record
        self.mock_postcode_geo.return_value = {"european_electoral_region": "North West", "admin_county": "Cumbria"}
        
        response = self.client.get('/primaries/primaries/', format='json')
        
        self.assertEqual(len(response.data), 0)

    def test_exclude_correct_territory(self):
        northwest = Territory.objects.create(name='North West',lookup_field='european_electoral_region')
        cumbria = Territory.objects.create(name='Cumbria',lookup_field='admin_county')
        self.primary1 = Primary.objects.create(name='primary1', description='Desc',
            voting_start=datetime.date.today(), voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        self.primary1.territories.add(northwest)
        self.primary1.exclude_territories.add(cumbria)
        self.mock_lookup_nb.return_value = self.nb_record
        self.mock_postcode_geo.return_value = {"european_electoral_region": "North West", "admin_county": "Not Cumbria"}
        
        response = self.client.get('/primaries/primaries/', format='json')
        
        self.assertEqual(len(response.data), 1)

    def test_not_eligible_tags(self):
        self.nb_record = { "birthdate": "1994-05-24", "id": 107876, "student": True, 
           "primary_address": {"zip": "CR0 7DJ"}, "membership_payment_status": "active",
           "tags": ["momentum-cancelled"]} 
        primary = Primary.objects.create(name='Generic', visible_to_non_staff=True,
            voting_start=datetime.date.today(), voting_end=self.tomorrow)
        self.mock_lookup_nb.return_value = self.nb_record
        self.mock_postcode_geo.return_value = {"european_electoral_region": "North West", "admin_county": "Cumbria"}
        
        response = self.client.get('/primaries/primaries/', format='json')
        
        self.assertEqual(len(response.data), 0)

    def test_not_eligible_status(self):
        self.nb_record = { "birthdate": "1994-05-24", "id": 107876, "student": True, 
           "primary_address": {"zip": "CR0 7DJ"}, "membership_payment_status": "cancelled",
           "tags": ["momentum-member"]} 
        primary = Primary.objects.create(name='Generic', visible_to_non_staff=True,
            voting_start=datetime.date.today(), voting_end=self.tomorrow)
        self.mock_lookup_nb.return_value = self.nb_record
        self.mock_postcode_geo.return_value = {"european_electoral_region": "North West", "admin_county": "Cumbria"}
        
        response = self.client.get('/primaries/primaries/', format='json')
        
        self.assertEqual(len(response.data), 0)

    def test_missing_birthdate(self):
        self._yl_test()
        self.mock_lookup_nb.return_value = { "birthdate": None, "id": 107876, "student": True, 
           "primary_address": {"zip": "CR0 7DJ"}, "membership_payment_status": "cancelled",
           "tags": ["momentum-member"]} 
        
        response = self.client.get('/primaries/primaries/', format='json')
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

    @patch('primaries.views.timezone.localtime')
    def test_voting_open_before_noon(self, mock_localtime):
        primary = Primary.objects.create(name='Running', visible_to_non_staff=True,
            voting_start=datetime.date.today(), voting_end=datetime.date.today())
        eleven = datetime.datetime.combine(datetime.date.today(), datetime.time(hour=11))
        mock_localtime.return_value = eleven
        
        response = self.client.get('/primaries/primaries/', format='json')
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    @patch('primaries.views.timezone.localtime')
    def test_voting_closes_after_noon(self, mock_localtime):
        primary = Primary.objects.create(name='Finished', visible_to_non_staff=True,
            voting_start=datetime.date.today(), voting_end=datetime.date.today())
        twelve_01 = datetime.datetime.combine(datetime.date.today(), datetime.time(hour=12, minute=1))
        mock_localtime.return_value = twelve_01
        
        response = self.client.get('/primaries/primaries/', format='json')
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

    def test_results(self):
        past = Primary.objects.create(name='Over',voting_start=self.yesterday, voting_end=self.yesterday, show_results=True)
        self._make_four_candidates(past)
        for i in range(0, 9):
            Vote.objects.create(candidate=self.cand1)
        for i in range(0, 7):
            Vote.objects.create(candidate=self.cand2)
        for i in range(0, 6):
            Vote.objects.create(candidate=self.cand3)
        for i in range(0, 3):
            Vote.objects.create(candidate=self.cand4)
        
        response = self.client.get('/primaries/completed/', format='json')
        
        self.assertEqual(len(response.data), 1)

        candidates = response.data[0]["candidates"]
        
        self.assertEqual(len(candidates), 4)
        self.assertEqual(candidates[0]["votes"], 9)
        self.assertEqual(candidates[1]["votes"], 7)
        self.assertEqual(candidates[2]["votes"], 6)
        self.assertEqual(candidates[3]["votes"], 3)

    def _fptp_vote_should_not_accept(self, candidates):
        
        response = self.client.post(
            '/primaries/vote/', 
            {'ranked': False, 'candidates': candidates}, 
            format='json')
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(0, HasVoted.objects.all().count())
        self.assertEqual(0, Vote.objects.all().count())

    def _no_fptp_vote_should_be_available_for(self, primary):
        self._make_four_candidates(primary)
        
        candidates = [str(self.cand1.id)]
        
        self._fptp_vote_should_not_accept(candidates)

    def test_cant_vote_not_visible(self):
        primary = Primary.objects.create(name='Hidden', 
            voting_start = self.yesterday, voting_end=self.tomorrow)
        
        self._no_fptp_vote_should_be_available_for(primary)

    def test_cant_vote_too_early(self):
        primary = Primary.objects.create(name='Starts tomorrow', visible_to_non_staff=True,
            voting_start = self.tomorrow, voting_end=self.tomorrow)
        
        self._no_fptp_vote_should_be_available_for(primary)

    def test_cant_vote_too_late(self):
        primary = Primary.objects.create(name='Ends yesterday', visible_to_non_staff=True,
            voting_start = self.yesterday, voting_end=self.yesterday)
        
        self._no_fptp_vote_should_be_available_for(primary)

    def test_no_votes(self):
        primary = Primary.objects.create(name='Ongoing', visible_to_non_staff=True,
            voting_start=self.yesterday, voting_end=self.tomorrow)
        self._make_four_candidates(primary)

        self._fptp_vote_should_not_accept([])

    def test_too_many_votes(self):
        primary = Primary.objects.create(name='Ongoing', visible_to_non_staff=True,
            voting_start=self.yesterday, voting_end=self.tomorrow)
        self._make_four_candidates(primary)
        candidates = [str(self.cand1.id), str(self.cand2.id)]
        
        self._fptp_vote_should_not_accept(candidates)

    def test_mix_primaries(self):
        primary1 = Primary.objects.create(name='First',  visible_to_non_staff=True,
            voting_start=self.yesterday, voting_end=self.tomorrow, max_votes=2)
        primary2 = Primary.objects.create(name='Second', visible_to_non_staff=True,
            voting_start=self.yesterday, voting_end=self.tomorrow, max_votes=2)
        cand1 = Candidate.objects.create(
            primary=primary1,first_name='Jeremy',last_name='Corbyn',statement='Oh, Jeremy Corbyn')
        cand2 = Candidate.objects.create(
            primary=primary2,first_name='Ed',last_name='Miliband',statement='One Nation Labour')
        cand3 = Candidate.objects.create(
            primary=primary1, first_name='Gordon',last_name='Brown',statement='Not Flash, just Gordon')
        cand4 = Candidate.objects.create(
            primary=primary2,first_name='Tony',last_name='Blair',statement='New Labour. New Britain.')
        candidates = [ str(cand.id) for cand in [cand1, cand2] ]
        
        self._fptp_vote_should_not_accept(candidates)

    def test_invalid_candidate(self):
        primary = Primary.objects.create(name='Ongoing', visible_to_non_staff=True,
            voting_start=self.yesterday, voting_end=self.tomorrow)
        self._make_four_candidates(primary)
        
        self._fptp_vote_should_not_accept(['999'])

    def test_already_voted(self):
        primary = Primary.objects.create(name='Ongoing', visible_to_non_staff=True,
            voting_start=self.yesterday, voting_end=self.tomorrow)
        self._make_four_candidates(primary)
        candidates = [  str(self.cand1.id) ]
        
        response = self.client.post(
            '/primaries/vote/', 
            {'ranked': False, 'candidates': candidates}, 
            format='json')
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, HasVoted.objects.filter(member=self.voter, primary=primary).count())
        self.assertEqual(1, Vote.objects.filter(candidate__primary=primary).count())

        response = self.client.post(
            '/primaries/vote/', 
            {'ranked': False, 'candidates': candidates}, 
            format='json')
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(1, HasVoted.objects.filter(member=self.voter, primary=primary).count())
        self.assertEqual(1, Vote.objects.filter(candidate__primary=primary).count())

    def test_voting_once_doesnt_stop_another(self):
        primary1 = Primary.objects.create(name='First', visible_to_non_staff=True,
            voting_start=self.yesterday, voting_end=self.tomorrow, max_votes=2)
        primary2 = Primary.objects.create(name='Second', visible_to_non_staff=True,
            voting_start=self.yesterday, voting_end=self.tomorrow, max_votes=2)
        cand1 = Candidate.objects.create(
            primary=primary1,first_name='Jeremy',last_name='Corbyn',statement='Oh, Jeremy Corbyn')
        cand2 = Candidate.objects.create(
            primary=primary2,first_name='Ed',last_name='Miliband',statement='One Nation Labour')
        cand3 = Candidate.objects.create(
            primary=primary1, first_name='Gordon',last_name='Brown',statement='Not Flash, just Gordon')
        cand4 = Candidate.objects.create(
            primary=primary2,first_name='Tony',last_name='Blair',statement='New Labour. New Britain.')

        response = self.client.post(
            '/primaries/vote/', {'ranked': False, 'candidates': [str(cand1.id)]}, format='json')
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
        response = self.client.post(
            '/primaries/vote/', {'ranked': False, 'candidates': [str(cand2.id)]}, format='json')
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(2, HasVoted.objects.filter(member=self.voter).count())
        self.assertEqual(1, Vote.objects.filter(candidate__primary=primary1).count())
        self.assertEqual(1, Vote.objects.filter(candidate__primary=primary2).count())

    def test_cant_vote_unless_logged_in(self):
        self.client.force_authenticate(user=None)
        primary = Primary.objects.create(name='Ongoing', visible_to_non_staff=True,
            voting_start=self.yesterday, voting_end=self.tomorrow)
        self._make_four_candidates(primary)
        candidates = [str(self.cand1.id)]
        
        response = self.client.post(
            '/primaries/vote/', 
            {'ranked': False, 'candidates': candidates}, 
            format='json')
        
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(0, HasVoted.objects.all().count())
        self.assertEqual(0, Vote.objects.all().count())

    def test_cant_vote_birthday_is_cutoff_date(self):
        under_27 = Restriction.objects.create(name='Under27',restriction_type='age',max_age=26,
            cutoff_date=datetime.date(year=2020,month=11,day=13))
        primary = Primary.objects.create(name='primary1', description='Desc', visible_to_non_staff=True,
            voting_start=datetime.date.today(), voting_end=self.tomorrow, max_votes=1)
        primary.restrictions.add(under_27)
        self._make_four_candidates(primary)
        self.mock_lookup_nb.return_value = self.nb_record
        self.mock_postcode_geo.return_value = {"european_electoral_region": "London"}
        self.mock_lookup_nb.return_value = { "birthdate": '1993-11-13', "id": 107876, "student": True, 
           "primary_address": {"zip": "CR0 7DJ"}, "membership_payment_status": "active",
           "tags": ["momentum-member"]}

        self._no_fptp_vote_should_be_available_for(primary)

    def test_can_vote_primaries_birthday_day_after_cutoff(self):
        under_27 = Restriction.objects.create(name='Under27',restriction_type='age',max_age=26,
            cutoff_date=datetime.date(year=2020,month=11,day=13))
        primary = Primary.objects.create(name='primary1', description='Desc', visible_to_non_staff=True,
            voting_start=datetime.date.today(), voting_end=self.tomorrow, max_votes=1)
        primary.restrictions.add(under_27)
        self._make_four_candidates(primary)
        self.mock_lookup_nb.return_value = self.nb_record
        self.mock_postcode_geo.return_value = {"european_electoral_region": "London"}
        self.mock_lookup_nb.return_value = { "birthdate": '1993-11-14', "id": 107876, "student": True, 
           "primary_address": {"zip": "CR0 7DJ"}, "membership_payment_status": "active",
           "tags": ["momentum-member"]}
        
        response = self.client.post(
            '/primaries/vote/', 
            {'ranked': False, 'candidates': [str(self.cand1.id)]},
            format='json')
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, HasVoted.objects.filter(member=self.voter).count())
        self.assertEqual(1, Vote.objects.filter(candidate__primary=primary).count())

# test standing

    def test_stand_candidate(self):
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=20, registrations_start=datetime.date.today(), nominations_start=datetime.date.today(),
            nominations_end=self.tomorrow,
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)

        response = self.client.post('/primaries/stand/', self._candidate_data(primary), format='json')
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, Candidate.objects.filter(member=self.voter, primary=primary).count())

    def test_cant_stand_unless_logged_in(self):
        self.client.force_authenticate(user=None)
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=20, registrations_start=datetime.date.today(), nominations_start=datetime.date.today(),
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)

        response = self.client.post('/primaries/stand/', self._candidate_data(primary), format='json')
        
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(0, Candidate.objects.count())

    def _just_cant_stand(self, primary):
        response = self.client.post('/primaries/stand/', self._candidate_data(primary), format='json')
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(0, Candidate.objects.count())

    def test_cant_stand_before_registrations_start(self):
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=20, registrations_start=self.tomorrow,
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        self._just_cant_stand(primary)

    def test_cant_stand_after_nominations_close(self):
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=20, registrations_start=self.yesterday, 
            nominations_start=self.yesterday, nominations_end=self.yesterday,
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        
        self._just_cant_stand(primary)

    def test_cant_stand_if_wrong_region(self):
        northwest = Territory.objects.create(name='North West',lookup_field='european_electoral_region')
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=20, registrations_start=datetime.date.today(),
            nominations_start=self.tomorrow, nominations_end=self.tomorrow,
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        primary.territories.add(northwest)
        self.mock_lookup_nb.return_value = self.nb_record
        self.mock_postcode_geo.return_value = {"european_electoral_region": "London"}
        
        response = self.client.get('/primaries/primaries/', format='json')
        
        self._just_cant_stand(primary)

    def test_cant_stand_if_not_member(self):
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=20, registrations_start=datetime.date.today(),
            nominations_start=self.tomorrow, nominations_end=self.tomorrow,
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        nb_record = { "birthdate": "1994-05-24", "id": 107876, "student": True, 
           "primary_address": {"zip": "CR0 7DJ"}, "membership_payment_status": "cancelled",
           "tags": ["momentum-member"]}
        self.mock_lookup_nb.return_value = nb_record
        
        self._just_cant_stand(primary)

# test nominations

    def test_nominate(self):
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=20,registrations_start=datetime.date.today(), nominations_start=datetime.date.today(),
            nominations_end=self.tomorrow,
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        candidate = Candidate.objects.create(primary=primary, member=self.candidate_member)
        
        response = self.client.post('/primaries/nominate/', {'candidate': candidate.id}, format='json')
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, Nomination.objects.filter(member=self.voter, candidate=candidate).count())

    def test_cant_nominate_before_nominations_start(self):
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=20, registrations_start=self.tomorrow, nominations_start=self.tomorrow,
            nominations_end=self.tomorrow,
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        
        self._cant_nominate(primary)
    
    def _cant_nominate(self, primary):
        candidate = Candidate.objects.create(primary=primary, member=self.candidate_member)
        
        response = self.client.post('/primaries/nominate/', {'candidate': candidate.id}, format='json')
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(0, Nomination.objects.filter(member=self.voter, candidate=candidate).count())

    def test_cant_nominate_after_nominations_close(self):
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=20, registrations_start=self.yesterday, nominations_start=self.yesterday,
            nominations_end=self.yesterday,
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        
        self._cant_nominate(primary)

    def test_cant_nominate_unless_logged_in(self):
        self.client.force_authenticate(user=None)
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=20,registrations_start=datetime.date.today(), nominations_start=datetime.date.today(),
            nominations_end=self.tomorrow,
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        candidate = Candidate.objects.create(primary=primary)
        
        response = self.client.post('/primaries/nominate/', {'candidate': candidate.id}, format='json')
        
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(0, Nomination.objects.filter(member=self.voter, candidate=candidate).count())

    def test_cant_nominate_if_wrong_region(self):
        northwest = Territory.objects.create(name='North West',lookup_field='european_electoral_region')
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=20, registrations_start=datetime.date.today(),
            nominations_start=datetime.date.today(), nominations_end=self.tomorrow,
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        primary.territories.add(northwest)
        self.mock_lookup_nb.return_value = self.nb_record
        self.mock_postcode_geo.return_value = {"european_electoral_region": "London"}
        
        self._cant_nominate(primary)

    def test_cant_nominate_if_not_member(self):
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=20,registrations_start=datetime.date.today(), nominations_start=datetime.date.today(),
            nominations_end=self.tomorrow,
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        nb_record = { "birthdate": "1994-05-24", "id": 107876, "student": True, 
           "primary_address": {"zip": "CR0 7DJ"}, "membership_payment_status": "cancelled",
           "tags": ["momentum-member"]}
        self.mock_lookup_nb.return_value = nb_record
        
        self._cant_nominate(primary)

    def test_cant_nominate_if_over_limit(self):
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=0,registrations_start=datetime.date.today(), nominations_start=datetime.date.today(),
            nominations_end=self.tomorrow,
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        
        self._cant_nominate(primary)

    def test_cant_nominate_self(self):
        primary = Primary.objects.create(name='primary', description='Desc',
            min_nominations=20,registrations_start=datetime.date.today(), nominations_start=datetime.date.today(),
            nominations_end=self.tomorrow,
            voting_start=self.tomorrow, voting_end=self.tomorrow, max_votes=1,
            visible_to_non_staff=True)
        candidate = Candidate.objects.create(primary=primary, member=self.voter)
        
        response = self.client.post('/primaries/nominate/', {'candidate': candidate.id}, format='json')
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(0, Nomination.objects.filter(member=self.voter, candidate=candidate).count())

# test ranked voting

    def _ranked_vote_should_not_accept(self, candidates):
        response = self.client.post(
            '/primaries/vote/',
            {'ranked': False, 'candidates': candidates},
            format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(0, HasVoted.objects.all().count())
        self.assertEqual(0, RankedVote.objects.all().count())

    def _ranked_vote_should_accept(self, candidates):
        response = self.client.post(
            '/primaries/vote/',
            {'ranked': True, 'candidates': candidates},
            format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, HasVoted.objects.filter(
            member=self.voter,
            primary=self.primary1)
            .count())
        self.assertEqual(1, RankedVote.objects.filter(
            primary=self.primary1)
            .count())

    def _ranked_candidates_are_not_valid(self, candidates, primary):
        def fail(message):
            return message
        
        result = ranked_ballot_is_valid(
            raw_candidates=candidates,
            primary=primary,
            fail=fail
        )

        self.assertNotEqual(result, True)

    def _ranked_candidates_are_valid(self, candidates, primary):
        def fail(message):
            return message
        
        result = ranked_ballot_is_valid(
            raw_candidates=candidates,
            primary=primary,
            fail=fail
        )

        self.assertTrue(result)

    def test_cannot_submit_without_ranked_on_post(self):
        self._ranked_test()
        good_ranked_candidates = [
            {'id': str(self.cand1.id), 'rank': 1},
            {'id': str(self.cand2.id), 'rank': 2},
            {'id': str(self.cand4.id), 'rank': 3}]

        response = self.client.post(
            '/primaries/vote/',
            {'candidates': good_ranked_candidates},
            format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(0, HasVoted.objects.all().count())
        self.assertEqual(0, RankedVote.objects.all().count())
        
    def test_cannot_submit_without_ranked_as_bool(self):
        self._ranked_test()
        good_ranked_candidates = [
            {'id': str(self.cand1.id), 'rank': 1},
            {'id': str(self.cand2.id), 'rank': 2},
            {'id': str(self.cand4.id), 'rank': 3}]

        response = self.client.post(
            '/primaries/vote/',
            {'ranked': 'foobarbaz', 'candidates': good_ranked_candidates},
            format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(0, HasVoted.objects.all().count())
        self.assertEqual(0, RankedVote.objects.all().count())

    def test_ranked_votes_are_unique(self):
        self._ranked_test()
        bad_ranked_candidates = [
            {'id': str(self.cand1.id), 'rank': 1},
            {'id': str(self.cand2.id), 'rank': 1},
            {'id': str(self.cand4.id), 'rank': 1}]
        primary = self.primary1

        self._ranked_candidates_are_not_valid(bad_ranked_candidates, primary)

    def test_ranked_votes_are_consecutive(self):
        self._ranked_test()
        bad_ranked_candidates = [
            {'id': str(self.cand1.id), 'rank': 1},
            {'id': str(self.cand2.id), 'rank': 3},
            {'id': str(self.cand4.id), 'rank': 4}]
        primary = self.primary1

        self._ranked_candidates_are_not_valid(bad_ranked_candidates, primary)

    def test_ranked_votes_start_with_one(self):
        self._ranked_test()
        bad_ranked_candidates = [
            {'id': str(self.cand1.id), 'rank': 2},
            {'id': str(self.cand2.id), 'rank': 3},
            {'id': str(self.cand4.id), 'rank': 4}]
        primary = self.primary1

        self._ranked_candidates_are_not_valid(bad_ranked_candidates, primary)

    def test_ranked_votes_are_no_more_than_max(self):
        self._ranked_test()
        bad_ranked_candidates = [
            {'id': str(self.cand1.id), 'rank': 1},
            {'id': str(self.cand2.id), 'rank': 2},
            {'id': str(self.cand3.id), 'rank': 3},
            {'id': str(self.cand4.id), 'rank': 4}]

        primary = self.primary1

        self._ranked_candidates_are_not_valid(bad_ranked_candidates, primary)

    def test_cant_submit_fptp_vote_to_ranked(self):
        self._ranked_test()
        good_fptp_candidates = [
            str(self.cand1.id), 
            str(self.cand2.id), 
            str(self.cand4.id)]

        self._ranked_vote_should_not_accept(good_fptp_candidates)

    def test_cant_submit_ranked_vote_to_fptp(self):
        self._yl_test()
        good_ranked_candidates = [
            {'id': str(self.cand1.id), 'rank': 1},
            {'id': str(self.cand2.id), 'rank': 2},
            {'id': str(self.cand4.id), 'rank': 3}]

        self._fptp_vote_should_not_accept(good_ranked_candidates)

    @skip("Found broken for unknown reason during Digital Team handover 2021")
    def test_ranked_vote_submits(self):
        self._ranked_test()
        good_ranked_candidates = [
            {'id': str(self.cand1.id), 'rank': 1},
            {'id': str(self.cand2.id), 'rank': 2},
            {'id': str(self.cand4.id), 'rank': 3}]
        primary = self.primary1

        self._ranked_candidates_are_valid(good_ranked_candidates, primary)
        self._ranked_vote_should_accept(good_ranked_candidates)
