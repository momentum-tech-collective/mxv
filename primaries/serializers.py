from primaries.models import *
from rest_framework import serializers

def include_votes(primary):
    return primary.voting_closed() and primary.show_results

class CandidateSerializer(serializers.ModelSerializer):
    photo = serializers.SerializerMethodField()
    nominations = serializers.SerializerMethodField()
    votes = serializers.SerializerMethodField()
    class Meta:
        model = Candidate
        exclude = ['member', 'data_controller_name', 'data_controller_email', 'reject_reason', 'over_18', 'checked_labour']

    def get_photo(self, candidate):
        if candidate.photo:
            return candidate.photo.build_url(secure=True, width=530, height=398, crop='limit')
        else:
            return None
    
    def get_nominations(self, candidate):
        return Nomination.objects.filter(candidate=candidate).count()

    def get_votes(self, candidate):
        primary = candidate.primary
        if include_votes(primary):
            return Vote.objects.filter(candidate=candidate).count()
        else:
            return None


class PrimarySerializer(serializers.ModelSerializer):
    candidates = CandidateSerializer(many=True, read_only=True)
    class Meta:
        model = Primary
        fields = (
            'id', 
            'candidates', 
            'name', 
            'description', 
            'registrations_start', 
            'nominations_start', 
            'nominations_end', 
            'min_nominations', 
            'voting_start', 
            'voting_end', 
            'max_votes', 
            'ranked_choice')
    
    def to_representation(self, primary):
        response = super().to_representation(primary)
        if include_votes(primary):
            response["candidates"] = sorted(response["candidates"], key=lambda x: x["votes"], reverse=True)
        return response

