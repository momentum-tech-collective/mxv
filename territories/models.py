from django.db import models

POSTCODES_IO_LOOKUPS=(
    ('admin_district', 'Council'),
    ('admin_county', 'County'),
    ('parish', 'Parish or Town'),
    ('scottish_parliamentary_constituency', 'Scottish Constituency'),
    ('parliamentary_constituency', 'UK Constituency'),
    ('european_electoral_region', 'UK Region'),
    ('admin_ward', 'Ward'),
    # Welsh/Scottish regions not supported by postcodes.io, must use constituency or hand-code them
)

class Territory(models.Model):
    name = models.CharField(max_length=255)
    lookup_field = models.CharField(choices=POSTCODES_IO_LOOKUPS,max_length=255,
        help_text="'Council' can be a district council or London or metropolitan borough council. 'County' refers to an upper-tier county council. Spelling must match that of https://postcodes.io")
    region = models.ForeignKey("self", limit_choices_to=models.Q(lookup_field='european_electoral_region'), on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name_plural = 'territories'
        constraints = [
            models.UniqueConstraint(fields=['lookup_field', 'name'], name='duplicate_territory'),
        ]
        ordering = ('lookup_field', 'name',)

    def __str__(self):
        return f'{self.name} ({self.get_lookup_field_display()})'        

