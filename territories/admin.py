from django.contrib import admin
from territories.models import *
from groups.admin import GroupInline, BooleanFilterFactory
from django.db.models import Q

RegionFilter = ('region', admin.RelatedOnlyFieldListFilter)

HasDpaFilter = BooleanFilterFactory(
    'Group assigned',
    Q(groups_including=None)
)

@admin.register(Territory)
class TerritoryAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    ordering = ('name',)
    list_filter = ('lookup_field', RegionFilter, HasDpaFilter)
    list_display = ('name', 'lookup_field', 'region')
    inlines = (GroupInline,)
