from django.contrib import admin
from lobby.models import *

@admin.register(MP)
class MPAdmin(admin.ModelAdmin):
    list_display = ('name', 'constituency', 'email')
    search_fields = ('name', 'constituency')