from django.conf.urls import url
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from lobby import views

app_name = 'lobby'
urlpatterns = [
    url(r'^send', views.SendEmailView.as_view()),
]