from django.db import models
from django.contrib.postgres.fields.citext import CIEmailField

class MP(models.Model):
    name = models.CharField(max_length=32)
    email = CIEmailField(max_length=255)
    constituency = models.CharField(max_length=255)

    @property
    def salutation(self):
        namewords = self.name.split(' ')
        if self.name.startswith('Sir'):
            return f'Sir {namewords[1]}'
        elif namewords[0] in ['Mr', 'Mrs', 'Ms', 'Dr']:
            return f'{namewords[0]} {namewords[2:]}'
        else:
            return namewords[0]
