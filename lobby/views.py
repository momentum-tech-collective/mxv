from django.conf import settings
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from email_validator import *
from lobby.models import MP
from mxv.nation_builder import NationBuilder
from mxv.utils import postcode_geo, send_mailgun_email, htmlify
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
import json
import requests

import logging
import pprint
import sys
pp = pprint.PrettyPrinter()

def full_email(data):
    fulltext = f'Dear %recipient.salutation%,\n\n{data["text"]}'
    if data['firstName'] and data['lastName']:
        fulltext += f'\nThanks,\n\n{data["firstName"]} {data["lastName"]}\n'
    return fulltext

def check_email(raw):
    try:
        return validate_email(raw).email
    except EmailNotValidError as e:
        logging.error(f'{raw} is not valid: {e}')
        return None 

class SendEmailView(APIView):
    # TODO: is this safe enough? Can only be hit with CORS permissions
    permission_classes = [AllowAny]

    @method_decorator(ensure_csrf_cookie)
    def post(self, request, format=None):
        source = check_email(request.data['email'])
        if not source:
            return Response({"Message": "Error validating email"}, status=status.HTTP_400_BAD_REQUEST)
        prefix = source.split('@')[0]
        text = full_email(request.data)
        html = htmlify(text)
        replyTo = f'{request.data["firstName"]} {request.data["lastName"]} <{source}>'

        try:
            geo = postcode_geo(request.data['postcode'])
        except Exception as e:
            return Response({"Message": "Geocoding error"},status=status.HTTP_400_BAD_REQUEST)
        constituency = geo['parliamentary_constituency']
        
        if not MP.objects.filter(constituency=constituency).exists():
            logging.error(f'Unrecognized constituency {constituency}')
            return Response({"Message": "Error in constituency"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        mp = MP.objects.get(constituency=constituency)

        # Parliamentary bot uses the From address, so we have to risk sending using it
        if settings.ELOBBY_SEND_MP_EMAIL:
            to_email = mp.email
            to_salutation = mp.salutation
        else:
            to_email, to_name = settings.ELOBBY_EMAIL_CC.popitem()
            to_salutation = to_name.split(' ')[0]

        send_mailgun_email(to_email, to_salutation, settings.ELOBBY_EMAIL_CC, settings.ELOBBY_EMAIL_BCC, 
                           replyTo, replyTo, settings.ELOBBY_EMAIL_SUBJECT, text, html)
            
        nb = NationBuilder()
        person =  { 'email': source, 'tags': [settings.ELOBBY_EMAIL_TAG]} 
        if request.data['firstName']:
            person['first_name'] = request.data['firstName']
        if request.data['lastName']:
            person['last_name'] = request.data['lastName']
        if request.data['mobile']:
            # TODO: use phonenumbers to determine if land or mobile, and valid
            person['mobile'] = request.data['mobile']
        if request.data['postcode']:
            postcode = request.data['postcode']
            if len(postcode) <= 10: # NB won't accept postcodes longer than 10 chrs
                person['home_address'] = { 'zip' : postcode }
        if request.data['optIn']:
            nb.CreateOrUpdate(person)
            # TODO: check NB worked

        return Response(status=status.HTTP_200_OK)

